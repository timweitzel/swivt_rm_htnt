
function [PV_t] = ImportSHdata_v2(delta_T,Tag,Monat,Jahr,duration,stretch)
% this function imports price data from data base and transfers it to an
% array with time stepsize delta_T. Delta_T is measured in minutes.

%clear all

file=strcat(pwd,'/90 Input/PVData/SH.mat');
load(file)

name=strcat('PV');

for d=1:duration
    t=datetime(Jahr,Monat,Tag)+(d-1);
    date=year(t)*10000+month(t)*100+day(t);
    name=strcat('PV');
    matrix=table2array(eval(name));

    [v,index]=max(matrix(:,4)==date);

    PV_1=matrix(index,5:end);

    N=60/delta_T;

    for i=1:24
        for n=1:N
            if i<24
                PV_t((d-1)*24*N +(i-1)*N+n)=(PV_1(i)*(N-(n-1))/N+PV_1(i+1)*(n-1)/N)*stretch;
            else
                PV_t((d-1)*24*N +(i-1)*N+n)=(PV_1(i)*(N-(n-1))/N+0*(n-1)/N)*stretch;
            end
        end
    end
end
end