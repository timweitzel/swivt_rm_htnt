clear all
handles=findall(0,'type','figure');
handles.delete;

year=2015;
day =1;
days=[31,28,31,30,31,30,31,31,30,31,30,31];
month=[1:1:12];
delta_t=5;
stretch=1;

PV=zeros(12,24*60/delta_t);
PV2=zeros(1,24*60/delta_t);
X=[0:delta_t/60:24];

for i=1:12
    PV2=0;
    for d=1:days(i)
        PV2=PV2+ImportPVdata(delta_t,d,month(i),year,stretch);
    end
    PV(i,:)=PV2./days(i);
end

figure(1)
hold on
grid on
title(strcat('AVG. PV Production in kW'));
xlabel('hour');
ylabel('kW');
set(gca,'xtick',[0:1:24]);

name={};
for i=1:12
    plot(X(1:end-1), PV(i,:));
    name=[name {strcat('Month=', num2str(i))}];
end
legend(name);