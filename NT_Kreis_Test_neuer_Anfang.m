%% Voreinstellungen

x_var.subdim={};
x_var.type=[];
x_var.dim.vect=[];
x_var.dim.vect_start=[];
x_var.dim.vect_end=[];
x_var.dim.total=0;
x_var.names=[];
x_var.ub=[];
x_var.lb=[];
big_M=1000000;

par.delta_T     =  60; %min
par.T_O_Master  =  2; %Optimierungszeitraum in Tagen
par.T_O         =  par.T_O_Master;
par.T           =  24/(par.delta_T/60)*par.T_O_Master; %Optimierungszeitraum in Zeiteinheiten delta_T
par.T_1         =  par.T + 1;
par.T_C         =  24/(par.delta_T/60);
par.T_total     =  4; %kompletter Betrachtungszeitraum in TAgen


C_E = 0.02; %Preis elektrisch
C_G = 0.04; %Preis Gas

% COP = 4;

T_HT_delta_WT_in = 10;
eta_WT = 0.95;

m = 2.05; %kg/s
c = 4.182; %(kW*s)/(kg*K)

I.QDemand_NT_t = 180* rand (1,48); % in kW 180 * ones(1,48);
I.Qd_SH_prod = 120 * rand(1,48);%120 * rand (1,48);
I.Qd_TESS_V_prod = 30 * rand(1,48);

par.NT_TESS.T_min = 5;%15; % Eigenschaften Speicher m�ssen in Namen noch nach NT und HT differenziert werden, sind im Code zurzeit noch �berall dieselben
par.NT_TESS.T_max = 90;%35;  %Bezeichnung mit par.NT.usw. einheitlich machen nach Umbennenung NT und HT und nicht mehr par.T_NT_usw.
par.NT_TESS.sigma = 0.0062;
par.NT_TESS.Volume = 100 ;% in l

Q_max_NT_TESS = 400;
Q_min_NT_TESS = 0;
% gamma_NT_TESS = (Q_max_NT_TESS - Q_min_NT_TESS) / (par.NT_TESS.T_max - par.NT_TESS.T_min);

I.start.Q_NT_TESS_T0 = Q_min_NT_TESS; %Speicher am Anfang leer
I.end.Q_NT_TESS_T0 = (Q_max_NT_TESS - Q_min_NT_TESS)/2 + Q_min_NT_TESS;      % Speicher am Ende halb voll


x_var   =   createVariable(x_var, 'P_THP',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);

x_var   =   createVariable(x_var, 'Q_NT_TESS',       par.T_1,      {'1' 'T_1' '' '' ''},      'C',    Q_min_NT_TESS,  Q_max_NT_TESS);

x_var   =   createVariable(x_var, 'Qd_NT_WT_in',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M); %hei�t im richtigen Programm glaube ich etwas anders
x_var   =   createVariable(x_var, 'Qd_THP',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M); %in Qd_THP_out umbenennen
x_var   =   createVariable(x_var, 'Qd_THP_zu',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'Qd_THP_in',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'Qd_NT_WT_out',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'Qd_NT_TESS_in',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'Qd_NT_TESS_out',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'Qd_NT_Demand_Zusatz',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);

x_var   =   createVariable(x_var, 'Qd_NT_SH_Curt',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'Qd_NT_TESS_V_Curt',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);

x_var   =   createVariable(x_var, 'Qd_SH_s',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'Qd_NT_TESS_V',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);

x_var   =   createVariable(x_var, 'T_THP_in',         par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);
% x_var   =   createVariable(x_var, 'T_NT_TESS_out',         par.T,      {'1' 'T' '' '' ''},      'C',    par.NT_TESS.T_min,  par.NT_TESS.T_max);
% x_var   =   createVariable(x_var, 'T_NT_TESS_in',         par.T,      {'1' 'T' '' '' ''},      'C',    par.NT_TESS.T_min,  par.NT_TESS.T_max);
x_var   =   createVariable(x_var, 'T_NT_delta_WT_out',         par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);

x_var   =   createVariable(x_var, 'S_T_THP_1',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_T_THP_2',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_T_THP_3',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_T_THP_4',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_T_THP_5',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);

x_var   =   createVariable(x_var, 'S_COP_1',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_COP_2',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_COP_3',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_COP_4',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_COP_5',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_COP_6',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_COP_7',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_COP_8',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_COP_9',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_COP_10',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);

x_var   =   createVariable(x_var, 'beta_WT',         par.T,      {'1' 'T' '' '' ''},      'C',    -big_M,  big_M);

%% Zielfunktion

[f_sub]=createMatrix( 1 , x_var);
for t=1:par.T
    f_sub.P_THP(1, t)   = C_E; 
    f_sub.Qd_NT_WT_in(1, t)   = C_G;
end

f = concatenateMatrix(x_var,f_sub);

%% A001 THP intern

[A_eq_A001_sub, b_eq_A001]=createMatrix(3*par.T, x_var); 
i=1;

%(3) mit zus�tzlicher Nachfrage, damit es bei zu
%kleiner Nachfrage trotzdem l�uft
for t=1:par.T
    A_eq_A001_sub.Qd_THP(i, t)         = 1;
%     A_eq_A001_sub.Qd_NT_Demand_Zusatz(i, t)         = -1;
    b_eq_A001(i,1) = I.QDemand_NT_t(t);
    i=i+1;
end

%(4)
for t=1:par.T
    A_eq_A001_sub.Qd_THP_in(i, t)         = 1;
    A_eq_A001_sub.Qd_THP(i, t)         = -1;
    A_eq_A001_sub.P_THP(i, t)         = 1;
    i=i+1;
end

A_eq_A001=concatenateMatrix(x_var,A_eq_A001_sub);

%% A004 Speicherinhalt

[A_eq_A004_sub, b_eq_A004]=createMatrix(1*par.T + 2, x_var);
i=1;

%(18) 
for t=1:par.T
    A_eq_A004_sub.Q_NT_TESS( i, t+1)          = +1;
    A_eq_A004_sub.Q_NT_TESS( i, t)           = -(1-par.NT_TESS.sigma);
    A_eq_A004_sub.Qd_NT_TESS_out( i, t)          = - (-par.delta_T/60);
    %A_eq_A004_sub.Qd_NT_TESS_Curt( i, t)          = - (-par.delta_T/60);
    A_eq_A004_sub.Qd_NT_TESS_in( i, t)          = - (par.delta_T/60);
    A_eq_A004_sub.Qd_SH_s( i, t)          = - (par.delta_T/60);
    A_eq_A004_sub.Qd_NT_TESS_V( i, t)          = - (par.delta_T/60);
    %b_eq_A004(i, 1) = I.Qd_SH_und_V(t);
    i=i+1;
end

A_eq_A004_sub.Q_NT_TESS(i, 1)        = 1; %Anfangsbedingung
b_eq_A004 (i,1)                       = I.start.Q_NT_TESS_T0;
i=i+1;

A_eq_A004_sub.Q_NT_TESS(i,par.T + 1) = 1; %Endedingung
b_eq_A004 (i,1)                       = I.end.Q_NT_TESS_T0;
i=i+1;

A_eq_A004=concatenateMatrix(x_var,A_eq_A004_sub);

%% A007 SH Curtailment und Speicherkopplung Curtailment (die k�nnen so nicht in anderen Code �bernommen werden, da wird vorher curtailt

[A_eq_A007_sub, b_eq_A007]=createMatrix(2*par.T, x_var);
i=1;

for t=1:par.T
    A_eq_A007_sub.Qd_NT_TESS_V_Curt(i, t)         = 1;
    A_eq_A007_sub.Qd_NT_TESS_V(i, t)         = 1;
    b_eq_A007 (i,1) = I.Qd_TESS_V_prod(t);
    i=i+1;
end

for t=1:par.T
    A_eq_A007_sub.Qd_NT_SH_Curt(i, t)         = 1;
    A_eq_A007_sub.Qd_SH_s(i, t)         = 1;
    b_eq_A007 (i,1) = I.Qd_SH_prod(t);
    i=i+1;
end

A_eq_A007=concatenateMatrix(x_var,A_eq_A007_sub);

%% A003 W�rmeflussgleichungen

[A_eq_A003_sub, b_eq_A003]=createMatrix(2*par.T, x_var);
i=1;

%(12)
for t=1:par.T
    A_eq_A003_sub.Qd_THP_in(i, t)         = 1;
    A_eq_A003_sub.Qd_NT_WT_out(i, t)         = -1;
    A_eq_A003_sub.Qd_NT_TESS_in(i, t)         = 1;
    i=i+1;
end

%(13)
for t=1:par.T
    A_eq_A003_sub.Qd_NT_WT_out(i, t)         = 1;
    A_eq_A003_sub.Qd_NT_TESS_out(i, t)         = -1;
    A_eq_A003_sub.Qd_NT_WT_in(i, t)         = -1;
    i=i+1;
end

% %(14)
% for t=1:par.T
%     A_eq_A003_sub.Qd_NT_WT_out(i, t)         = 1;
%     A_eq_A003_sub.T_THP_in(i, t)         = -1 * m * c;
%     i=i+1;
% end
% 
% %(15)
% for t=1:par.T
%     A_eq_A003_sub.Qd_NT_TESS_out(i, t)         = 1;
%     A_eq_A003_sub.T_NT_TESS_out(i, t)         = -1 * m * c;
%     i=i+1;
% end
% 
% %(16)
% for t=1:par.T
%     A_eq_A003_sub.Qd_NT_TESS_in(i, t)         = 1;
%     A_eq_A003_sub.T_NT_TESS_in(i, t)         = -1 * m * c;
%     i=i+1;
% end
% 
% %(17)
% for t=1:par.T
%     A_eq_A003_sub.Qd_THP_in(i, t)         = 1;
%     A_eq_A003_sub.T_THP_in(i, t)         = -1 * m * c;
%     A_eq_A003_sub.T_NT_TESS_in(i, t)         = 1 * m * c;
%     i=i+1;
% end

A_eq_A003=concatenateMatrix(x_var,A_eq_A003_sub);

%% Zusammenf�hren Matrizen

A_eq_sys=[A_eq_A001; A_eq_A003; A_eq_A004; A_eq_A007];
b_eq_sys=[b_eq_A001; b_eq_A003; b_eq_A004; b_eq_A007];
A_ineq_sys=[]; %A_ineq_A0011];
b_ineq_sys=[]; %b_ineq_A0011];  


%% L�sung

RMESS.f=f;
RMESS.ub=x_var.ub;
RMESS.lb=x_var.lb;
RMESS.Aineq=A_ineq_sys;
RMESS.bineq=b_ineq_sys;
RMESS.Aeq=A_eq_sys;
RMESS.beq=b_eq_sys;
RMESS.ctype=x_var.type;

cplex = Cplex(RMESS);

%cplex.Param.mip.tolerances.mipgap.Cur = 1;
%cplex.Param.timelimit.Cur=20;
%cplex.Param.simplex.tolerances.Cur = 0.1;
 
cplex.solve;

x           =cplex.Solution.x;
[res ,start] =divide_xvar_RM(x, x_var.dim.vect, x_var.names, x_var.subdim ,  par);
