handles=findall(0,'type','figure');
handles.delete;
clear all

folderold=pwd;
cd(folderold);
folder1=strcat(pwd,'/');
cd(folder1);
foldername=strcat('Szenario3a_', datestr(now,'yy-mm-dd'));
mkdir(foldername);
folder=strcat(folder1,foldername);
cd(folderold);

%% Parameterdefinitionen (Basis)
%CHP Plant sollte mindestens 5000 h im Jahr laufen und nicht k?rzer als 1h
%am St?ck laufen
par.CHP.c_fuel = 0.04; %4 ct / kWh HO
par.CHP.c_startup = 0; % [EUR] -- (1/3) einer Periode kann nur die Halbe Leistung verwendet werden (approximiert)
% ==> c_fuel * P_CHP * (1/3) * 2
par.CHP.alpha = [9999; 2.720; 2.107; 1.975];%[999999; 999999; 2.720; 2.107; 1.975]; % bei der Erzeugung von 1kW elektrische Leistung werden alpha kW thermische Leistung durch Abw??rme erzeugt
par.CHPmodes=[0; 0.50; 0.75; 1.0];%[0; 0.499999; 0.50; 0.75; 1.0];%2.720; 2.107; 1.975]; %Percent of rated electrical power
par.CHP.eta = [0.00001; 0.699; 0.613; 0.613];%;[0.0002; 0.0002; 0.699; 0.611; 0.613]; % W?rmewirkungsgrad
par.n_CHPmodes=length(par.CHPmodes);
% Laut Viessmann-Website sind typische Werte alpha = 1.5 ... 2
% entspricht REZIPROKE STROMKENNZAHL: 0.5
par.CHP.P = 20;
par.CHP.P_max = par.CHP.P;
par.CHP.P_min = par.CHP.P*0.5;% [kW] Betriebspunkt 50/81 als andere Variante

%par.CHP.etaP = 0.322; % elektrischer Wirkungsgradwirkungsgrad
%par.CHP.etaG= par.CHP.etaP+par.CHP.eta;
par.CHP.t_min = 1 + 3;%mindestlaufzeit an perioden. Mindestens 1 + x
%par.CHP.Curt_max=par.CHP.alpha(numel(par.CHP.alpha))*par.CHP.P*0.7;
%Variable entfernt


%Energy Storage
%Battery is a LiFePo4 battery
par.ESS.cell.C_batt=2.3; %2.3 Ah per cell
par.ESS.cell.U_n=3.3; %3.3V nominal voltage
par.ESS.cell.E=par.ESS.cell.C_batt * par.ESS.cell.U_n; %7.59W per Cell
par.ESS.cell.n=14000; % in Summe ca 53,13kWh bei 7000 Zellen

par.ESS.sigma=83.333e-6; % entspricht 0.2\% pro Tag, mittlerer Wert in Chen et. al (2009)
%sigma_bat = 0.000027778; %sigma_SD = 0.00067; % pro Stunde; entspricht 2% self-discharge / Monat
%sigma_bat = 0.000125 % entspricht 0.3\% pro Tag, max. Wert in  Chen et. al (2009)
par.ESS.RTE=0.95;
par.ESS.eta_c=sqrt(par.ESS.RTE);
par.ESS.eta_d=sqrt(par.ESS.RTE);

par.ESS.E=100;%par.ESS.cell.E*par.ESS.cell.n/1000;
par.ESS.E_max=par.ESS.E;
par.ESS.E_min=0;
par.ESS.P_c_max=par.ESS.E*3;
par.ESS.P_d_max=par.ESS.E*3;
par.ESS.P_c_min=1; %minimum damit neuer Zyklus beginnt
par.ESS.P_d_min=1; %minimum damit neuer Zyklus beginnt
par.ESS.P_min=0;
par.ESS.price           = 200;
par.ESS.CAC.temp        = 25;
par.ESS.CAC.C_verl      = 0.2;
par.ESS.CAC.max_year    = 20;
par.ESS.CYC.n_max       = 10000;
par.ESS.E_inv=par.ESS.price * par.ESS.E;

% Heater
par.HTR.c_fuel=0.055; % Preis ist leicht h?her, da in KWK Betrieb teilweise Kosten wieder zur?ckgeholt werden k?nnen
par.HTR.Q_dot_max_burner = 3000; %stand vorher auf 300, hochgesetzt, damit W�rmegleichgewicht HT l�sbar wird
par.HTR.eta= 0.92*0.89; % 0.84Brennwert- (und nicht Heizwert-) -basierter Wirkungsgrad

% Thermal Heat pump
%hier muss noch was gemacht werden nehme ich mal an, habe erstmal das
%opt.THP zeug rausgeschmissen

%opt.INCL_THP   	 =0; %on/off switchfor THP
par.THP.P_max=1500; %3}50kw Pumpen max elektrische kW auf 1500 erh�ht, weil sonst Programm keine L�sung findet
par.THP.COP= 0.99; %COP
%opt.THP_COP_var =0; % 1-THP varoable COP/ 0-electric Heater or THP with constant COP
par.THP.eta=1;

% Thermischer Energiespeichers
par.TESS.sigma= 0.0062; % 0.62% pro Stunde Verlust -- W???rme soll zwei Tage vorhalten
par.TESS.eta_c= 1;
par.TESS.eta_d= 1;
par.TESS.Q_max= par.CHP.P*par.CHP.alpha(numel(par.CHP.alpha)) * 50 * 1.163 * 30/1000;%50l/kw thermisch installiert * 1,163 Wh/kgK * Delta T=30K
par.TESS.Qdot_max= 100000000000;

%Speicherkopplung
par.TESS.eta_kopplung = 0.8;

%W�rmetauscher
par.WT.eta = 0.95;

% Netzgrenzen
par.Grid.P_d_max=1000;
par.Grid.P_s_max=1000;

% Preisinformationen

par.Price.pmult=1; %multiplier for given market prices

% par.Price.customer=     250; %EUR/MWh
% par.Price.Grid_AP=      55.00; %EUR/MWh
% par.Price.Grid_GP=      2.78; %EUR/MWh
% par.Price.sonstige=     16.36; %EUR/MWh
% par.Price.Stromsteuer=  29.12; %EUR/MWh
% par.Price.Konzession=   15.90; %EUR/MWh

par.Price.customer_heat=     65; %EUR/MWh
par.Price.EEG_umlage=   63.54; %EUR/MWh
par.Price.Stromsteuer=  29.12; %EUR/MWh
par.Price.Konzession=   15.90; %EUR/MWh
par.Price.Grid_Umlagen = 8.2;%EUR/MWh KWKW und �19 Umlage
par.Price.Grid_Entgelt = 74.8;%EUR/MWh
par.Price.Grid_ges = par.Price.Konzession + par.Price.Grid_Entgelt + par.Price.Grid_Umlagen;

%par.Price.Kunde_Grid = 110; gibt es nicht mehr, weil wir zu B�rsenpreis
%kaufen und zu B�rsenpreis an Miterstrombetreiber weitergeben, sonst kauft
%der selbst an B�rse
par.Price.Kunde_PV = 90; %Preis von S�nke als �bergabe an Mieterstrombetreiber
par.Price.Kunde_CHP = 80; %Preis von S�nke als �bergabe an Mieterstrombetreiber
par.Price.Grid_PV = 110; %PV EEG Preis, muss noch genau geschaut werden wie gro� Anlage ist
par.Price.Grid_CHP = 110; %KWKG Preis, muss noch genau geschaut werden welcher gilt



% par.Price.Grid_Sell=   110; %EUR/MWh
% 
% par.Price.total_addon_micro= par.Price.EEG_umlage + par.Price.sonstige + par.Price.Grid_GP;
% par.Price.total_addon_grid = par.Price.total_addon_micro + par.Price.Grid_AP + par.Price.Konzession + par.Price.Stromsteuer;
% par.Price.NC_delta=par.Price.total_addon_grid-par.Price.total_addon_micro;

%LCC calculations
par.DODdelta=0.1;
par.DODvector=[0; 0.05; 0.15; 0.25; 0.35; 0.45; 0.55; 0.65; 0.75; 0.85; 0.95; 1.00];%[0:par.DODdelta:1];%
par.AhDODdelta=0.1;
par.AhDODvector=[0; 10];%
par.SOCdelta=0.1;
par.SOCvector=[0; 0.15; 0.30; 0.45; 0.60; 0.75; 0.90; 1.00];%0.00 ; 0.05; 0.10; 0.20; 0.30; 0.40; 0.45; 0.55; 0.60; 0.70; 0.80; 0.90; 0.95; 1.00];

par.DODApprox=length(par.DODvector)-1;%number of linearizations for DOD and SOC
par.SOCApprox=length(par.SOCvector);%number of linearizations for DOD and SOC
par.AhDODApprox=length(par.AhDODvector);



%% Options for optimization
opt.INCL_LCCinf             =1; %=1 falls LCC in Zielfunktion aktiv geschaltet werden sollen
opt.INCL_LCC                =1;
opt.INCL_CACinf             =1; %=1 falls CAC in Zielfunktion aufgenommen werden sollen



opt.cplex.DisplayIter       =0;% 1-Iteration werden angezeigt, 0- werden nicht angezeigt %
opt.cplex.TimeCap           =2*60;%5*60min TimeCap
opt.cplex.SolTolerance      =0.005;%0.005;
opt.INCL_epsconstraint      =0;
opt.Multiple_Periods        =0; %Multiple period analysis

% Parameter Variation - only select one option!
opt.INCL_parvar_ESSprice    =0;
opt.INCL_parvar_LCConoff    =0;
opt.INCL_parvar_CAConoff    =0;
opt.INCL_parvar_LCCCAConoff =0;
opt.INCL_parvar_Scenarios   =0;

opt.n_workers               =1; %parfor activated for n_workers>1

opt.save_conc_res           =1; %save sub results
opt.save_cplex              =0; %1: Cplex objekte werden gespeichert


%% Kontrollparameter NB

%Epsilon constraint Bedingungen
par.eps_const.delta     =0.1;
par.eps_const.vector    =[0:0.1:1];%[-0.3:par.eps_const.delta:1];
par.eps_const.num       =numel(par.eps_const.vector);

if opt.INCL_epsconstraint==1
    eps_max=par.eps_const.num-1;
else
    eps_max=1;
    par.eps_const.num=2;
end


% Anzahl der perioden
par.delta_T     =  60; %min
par.T_O_Master  =  2; %Optimierungszeitraum in Tagen
par.T_O         =  par.T_O_Master;
par.T           =  24/(par.delta_T/60)*par.T_O_Master; %Optimierungszeitraum in Zeiteinheiten delta_T
par.T_1         =  par.T + 1;
par.T_C         =  24/(par.delta_T/60);
par.T_total     =  50; %kompletter Betrachtungszeitraum in TAgen

if opt.Multiple_Periods==0
    par.periods=1;
    par.sdate.datetime(1) = datetime(2015, 1, 1); %17.7.
else
    par.sdate.datetime(1) = datetime(2015, 1, 12); %jeweils Montag als Anfangsdatum
    par.sdate.datetime(2) = datetime(2015, 4, 13);
    par.sdate.datetime(3) = datetime(2015, 7, 13);
    par.sdate.datetime(4) = datetime(2015, 10, 12);
    par.periods=numel(par.sdate.datetime);
end


%Parametervarition
%initiaize
for i=1:6
    par.parvar(i).vect=[];
    par.parvar(i).name='';
    par.parvar(i).ID=i;
    par.parvar(i).n_max=1;
end


if opt.INCL_parvar_ESSprice==1
    par.parvar(1).ID=1;
    par.parvar(1).name='ESSprice';
    par.parvar(1).vect=[200, 250, 300, 350, 400, 450, 500];%,150,200,250,300,400,500,600,1000]; %Unterschiedliche Preise f?r Speicher
    par.parvar(1).n_max=numel(par.parvar(1).vect);
    
elseif opt.INCL_parvar_Scenarios==1
    par.parvar(1).ID=1;
    par.parvar(1).name='ESSprice';
    par.parvar(1).vect=[100 200 300 500];
    par.parvar(1).n_max=numel(par.parvar(1).vect);
    
    par.parvar(2).ID=2;
    par.parvar(2).name='ESSsize';
    par.parvar(2).vect=[50 100 150 200];
    par.parvar(2).n_max=numel(par.parvar(2).vect);
    
    par.parvar(3).ID=3;
    par.parvar(3).name='TESSsize_mult';
    par.parvar(3).vect=[1, 2];
    par.parvar(3).n_max=numel(par.parvar(3).vect);
    
    par.parvar(4).ID=4;
    par.parvar(4).name='CHPsize';
    par.parvar(4).vect=[20; 50; 70; 140]; %(variant, Pmax)
    par.parvar(4).n_max=length(par.parvar(4).vect);
    
    par.parvar(4).CHP(1).alpha=[9999; 2.720; 2.107; 1.975];
    par.parvar(4).CHP(1).eta=[0.00001; 0.699; 0.613; 0.613];    
    par.parvar(4).CHP(2).alpha=[9999; 1.840; 1.684; 1.620];
    par.parvar(4).CHP(2).eta=[0.00001; 0.535; 0.542; 0.559];    
    par.parvar(4).CHP(3).alpha=[9999; 1.886; 1.604; 1.643];
    par.parvar(4).CHP(3).eta=[0.00001; 0.541; 0.535; 0.564];
    par.parvar(4).CHP(4).alpha=[9999; 1.857; 1.629; 1.479];
    par.parvar(4).CHP(4).eta=[0.00001; 0.573; 0.55; 0.539];
    
    par.parvar(5).ID=5;
    par.parvar(5).name='Delta_NetworkCharges';
    par.parvar(5).vect=[par.Price.NC_delta, 125, par.Price.total_addon_grid];
    par.parvar(5).n_max=numel(par.parvar(5).vect);
    
    par.parvar(6).ID=6;
    par.parvar(6).name='Increase_GridPrice';
    par.parvar(6).vect=[1];
    par.parvar(6).n_max=numel(par.parvar(6).vect);
    
    
elseif  opt.INCL_parvar_LCConoff==1
    par.parvar(1).n_max=2;
elseif  opt.INCL_parvar_CAConoff==1
    par.parvar(1).n_max=2;
elseif  opt.INCL_parvar_LCCCAConoff==1
    par.parvar(1).n_max=4;
else
    par.parvar(1).n_max=1;
end

%LCC Parameter
par.ESS.LCC.cyc_DOD=zeros(par.DODApprox,1);
for dprox=1:par.DODApprox
    for ahprox=1:par.AhDODApprox
        par.ESS.LCC.cyc_DOD(dprox,ahprox)= BA_cyc_Sarasketa_v2(     par.DODvector(dprox+1),    par.AhDODvector(ahprox)*par.ESS.cell.C_batt,    par.ESS.CAC.C_verl);
    end
end
for sprox=1:par.SOCApprox
    par.ESS.LCC.cal_SOC(sprox)= BA_cal_Sarasketa_v1(     (par.SOCvector(sprox)),    par.ESS.CAC.temp , par.delta_T,par.ESS.CAC.C_verl ,par.ESS.CAC.max_year);
end
clear dprox sprox ahprox

%% Optimierung

tges=datetime('now');
% Initialize Simvariables
Sol.Sim.KPI=[];
Sol.Sim.res=[];
Sol.Sim.I=[];
Sol.Sim.perday=[];
Sol.Sim.par=[];

Sol.par=par;
Sol.opt=opt;

for p1v=1:par.parvar(1).n_max
    for p2v=1:par.parvar(2).n_max
        for p3v=1:par.parvar(3).n_max
            for p4v=1:par.parvar(4).n_max
                for p5v=1:par.parvar(5).n_max
                    for p6v=1:par.parvar(6).n_max
                        for period=1:par.periods
                            for eps=1:eps_max
                                    p6max=par.parvar(6).n_max;
                                    p56max=p6max*par.parvar(5).n_max;
                                    p46max=p56max*par.parvar(4).n_max;
                                    p36max=p46max*par.parvar(3).n_max;
                                    p26max=p36max*par.parvar(2).n_max;
                                    p16max=p26max*par.parvar(1).n_max;
                                    
                                    pv=(p1v-1)*p26max + (p2v-1) * p36max + (p3v-1)*p46max +(p4v-1)*p56max + (p5v-1)*p6max + p6v;
                                    
                                    p=floor(pv/p16max*100);
                                    
                                    
                                p_mult =1; %Multiplikator f?r Preise aus 2015
                                
                                % ?bergabe Optimierungsparameter
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).opt  = opt;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).I=[];
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).pv=[];
                                Sol.pv(pv).ID=pv;
                                
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par  = par;
                                
                                % Parametervariation
                                if opt.INCL_parvar_ESSprice==1
                                    Sol.Sim(eps,period,p1v).par.ESS.price=  par.parvar(1).vect(p1v);
                                    Sol.Sim(eps,period,p1v).par.ESS.E_inv=  par.parvar(1).vect(p1v) * par.ESS.E_max;
                                elseif  opt.INCL_parvar_LCConoff==1
                                    Sol.Sim(eps,period,1).opt.INCL_LCCinf             =1;
                                    Sol.Sim(eps,period,2).opt.INCL_LCCinf             =0;
                                elseif opt.INCL_parvar_CAConoff==1
                                    Sol.Sim(eps,period,1).opt.INCL_CACinf             =1;
                                    Sol.Sim(eps,period,2).opt.INCL_CACinf             =0;
                                elseif opt.INCL_parvar_LCCCAConoff==1
                                    Sol.Sim(eps,period,1).opt.INCL_LCCinf             =1;
                                    Sol.Sim(eps,period,1).opt.INCL_CACinf             =1;
                                    Sol.Sim(eps,period,2).opt.INCL_LCCinf             =1;
                                    Sol.Sim(eps,period,2).opt.INCL_CACinf             =0;
                                    Sol.Sim(eps,period,3).opt.INCL_LCCinf             =0;
                                    Sol.Sim(eps,period,3).opt.INCL_CACinf             =1;
                                    Sol.Sim(eps,period,3).par.CYC=48;
                                    Sol.Sim(eps,period,4).opt.INCL_LCCinf             =0;
                                    Sol.Sim(eps,period,4).opt.INCL_CACinf             =0;
                                    Sol.Sim(eps,period,4).par.CYC=48;
                                    
                                elseif opt.INCL_parvar_Scenarios==1
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.ESS.E           =  par.parvar(2).vect(p2v);
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.ESS.E_max       =  par.parvar(2).vect(p2v);
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.ESS.P_c_max     =  par.parvar(2).vect(p2v)*3;
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.ESS.P_d_max     =  par.parvar(2).vect(p2v)*3;
                                    if par.parvar(2).vect(p2v)==0
                                         Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).opt.INCL_LCCinf             =0;
                                         Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).opt.INCL_CACinf             =0;
                                         Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).opt.INCL_LCC                =0;
                                    end

                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.ESS.price       =  par.parvar(1).vect(p1v);
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.ESS.E_inv       =  par.parvar(1).vect(p1v) * par.parvar(2).vect(p2v);
                                    
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.CHP.P           =  par.parvar(4).vect(p4v,1);
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.CHP.P_max       =  par.parvar(4).vect(p4v,1);
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.CHP.P_min       =  par.parvar(4).vect(p4v,1)*0.5;
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.CHP.alpha       =   par.parvar(4).CHP(p4v).alpha;
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.CHP.eta         =   par.parvar(4).CHP(p4v).eta;
                                    
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.TESS.Q_max      = par.parvar(4).vect(p4v,1)* par.parvar(4).CHP(p4v).alpha(length(par.CHPmodes))*par.parvar(3).vect(p3v) * 50 * 1.163 * 30/1000;
                                    
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.Price.pmult     =  par.parvar(6).vect(p6v);
                                    
                                    Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par.Price.total_addon_micro= par.Price.total_addon_grid - par.parvar(5).vect(p5v);
                                    
                                    Sol.pv(pv).ID=pv;
                                    Sol.pv(pv).ESSprice     =par.parvar(1).vect(p1v);
                                    Sol.pv(pv).ESSpriceID   =p1v;
                                    Sol.pv(pv).ESSsize      =par.parvar(2).vect(p2v);
                                    Sol.pv(pv).ESSsizeID    =p2v;
                                    Sol.pv(pv).TESSsize     =par.parvar(3).vect(p3v);
                                    Sol.pv(pv).TESSsizeID   =p3v;
                                    Sol.pv(pv).CHPsize      =par.parvar(4).vect(p4v);
                                    Sol.pv(pv).CHPsizeID    =p4v;
                                    Sol.pv(pv).NWC          =par.parvar(5).vect(p5v);
                                    Sol.pv(pv).NWCID        =p5v;
                                    Sol.pv(pv).pmult        =par.parvar(6).vect(p6v);
                                    Sol.pv(pv).pmultID      =p6v;

                                end
                            end
                        end
                    end
                end
            end
        end
    end
end

% Optimierung
if opt.n_workers>1
    poolobj = parpool(opt.n_workers);
end

% parvar counter
p6max=par.parvar(6).n_max;
p56max=p6max*par.parvar(5).n_max;
p46max=p56max*par.parvar(4).n_max;
p36max=p46max*par.parvar(3).n_max;
p26max=p36max*par.parvar(2).n_max;
p16max=p26max*par.parvar(1).n_max;

%Save folder


for p1v=1:par.parvar(1).n_max   %parfor kann hier vor die entsprechende Erste Instanz gesetzt werden die parallelisiert werden kann.
    for p2v=1:par.parvar(2).n_max
        for p3v=1:par.parvar(3).n_max
            for p4v=1:par.parvar(4).n_max
                for p5v=1:par.parvar(5).n_max
                    for p6v=1:par.parvar(6).n_max
                        for period=1:par.periods
                            for eps=1:eps_max
                                Sim_local=Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v);
                                pv=(p1v-1)*p26max + (p2v-1) * p36max + (p3v-1)*p46max +(p4v-1)*p56max + (p5v-1)*p6max + p6v;
                                Sim_local.ID=pv;
                                Sim_local.pv=Sol.pv(pv);
                                for d=1:par.T_total
                                    
                                    Sim_local.perday(d).t_sim.start=datetime('now');
                                    if d==1
                                        Sim_local.TIME.t_sim_start=Sim_local.perday(d).t_sim.start;
                                    end
                                    % Parameter?bergabe in jede Tagesoptimierung
                                    Sim_local.perday(d).par = Sim_local.par;
                                     
                                    if par.T_O_Master>par.T_total-d+1 %Anpassung der Optimierungszeitr?ume, so dass am Ender der Laufzeit nur noch ein Tag optimiret wird
                                        Sim_local.perday(d).par.T_O     = par.T_total-d+1;
                                        Sim_local.perday(d).par.T       = 24/(par.delta_T/60)*Sim_local.perday(d).par.T_O; %Optimierungszeitraum in Zeiteinheiten delta_T
                                        Sim_local.perday(d).par.T_1     = Sim_local.perday(d).par.T + 1;
                                    end
                                    
                                    % Inputdefinition f?r jede Tagesoptimierung
                                    %%% Time periods investigated
                                    Sim_local.perday(d).I.datetime    =   Sim_local.perday(d).par.sdate.datetime(period)+(d-1);
                                    l_datetime                        =   Sim_local.perday(d).I.datetime;
                                    Sim_local.perday(d).I.eps_const.n =   eps; %Laufparameter 1:num-1
                                    
% hab ich rausgeschmissen, weil das mit opt.THP auch raus ist                                    
%                                     if opt.THP_COP_var ==1
%                                         Sim_local.perday(d).par.THP.COP= COPperDay(day(l_datetime ),    month(l_datetime ), year(l_datetime ));
%                                     end
                                    
                                    %%% energy prices, heat data, pv Production
                                    Sim_local.perday(d).I.pd_t          = Sim_local.par.Price.pmult* ImportPricedata_v2(   Sim_local.par.delta_T,    day(l_datetime ),    month(l_datetime ),  year(l_datetime ),  Sim_local.perday(d).par.T_O)';
                                    %Sim_local.perday(d).I.ps_t          = Sim_local.par.Price.Grid_Sell * ones(size(Sim_local.perday(d).I.pd_t));%Sim_local.perday(d).I.pd_t;%
                                    Sim_local.perday(d).I.PV_t          = ImportPVdata_v2(      Sim_local.par.delta_T,    day(l_datetime ),    month(l_datetime ),  year(l_datetime ),  Sim_local.perday(d).par.T_O, 1)'; %Stretch is set to 1
                                    Sim_local.perday(d).I.PDemand_t     = ImportPDemanddata_v2( Sim_local.par.delta_T,    day(l_datetime ),    month(l_datetime ),  year(l_datetime ),  Sim_local.perday(d).par.T_O)';
                                    Sim_local.perday(d).I.QDemand_HT_t     = ImportQDemanddata_v2( Sim_local.par.delta_T,    day(l_datetime ),    month(l_datetime ),  year(l_datetime ),  Sim_local.perday(d).par.T_O)';
                                    %NT hat noch selben Input wie HT
                                    Sim_local.perday(d).I.QDemand_NT_t     = ImportQDemanddata_v2( Sim_local.par.delta_T,    day(l_datetime ),    month(l_datetime ),  year(l_datetime ),  Sim_local.perday(d).par.T_O)';
                                    Sim_local.perday(d).I.Pres_t        = Sim_local.perday(d).I.PDemand_t   -   Sim_local.perday(d).I.PV_t;
                                    Sim_local.perday(d).I.Pres_t_pos    = Sim_local.perday(d).I.Pres_t(1:par.T_C);
                                    Sim_local.perday(d).I.Pres_t_pos    = Sim_local.perday(d).I.Pres_t_pos(Sim_local.perday(d).I.Pres_t_pos>0);  
                                    clearvars l_datetime
                                    
                                    %Anfangs- und Endbedingungen
                                    if d==1
                                        Sim_local.perday(d).I.start.Q_HT_TESS_T0      = 0.0 *  Sim_local.par.TESS.Q_max;
                                        %n�chste Zeile noch selbe Qmax
                                        %werte wie HT
                                        Sim_local.perday(d).I.start.Q_NT_TESS_T0      = 0.0 *  Sim_local.par.TESS.Q_max;
                                        Sim_local.perday(d).I.start.E_ESS_T0       = 0.0 *  Sim_local.par.ESS.E_max; %Anfangswert und gleichzeitig Endwert einer Betrachtung
                                        Sim_local.perday(d).I.start.S_run_CHP_T0   =0;
                                        Sim_local.perday(d).I.start.S_run_CHP_T0_num=0; %Anzahl Perioden die das BHKW schon gelaufen ist
                                        
                                        Sim_local.perday(d).I.end.Q_HT_TESS_T0        = 0.0 *  Sim_local.par.TESS.Q_max;
                                        %n�chste Zeile NT noch selbe Qmax
                                        %werte wie HT
                                        Sim_local.perday(d).I.end.Q_NT_TESS_T0        = 0.0 *  Sim_local.par.TESS.Q_max;
                                        Sim_local.perday(d).I.end.E_ESS_T0         = 0.0 *  Sim_local.par.ESS.E_max;
                                        
                                    else
                                        Sim_local.perday(d).I.start.Q_HT_TESS_T0      = Sim_local.perday(d-1).end.Q_HT_TESS_T_1;
                                        Sim_local.perday(d).I.start.Q_NT_TESS_T0      = Sim_local.perday(d-1).end.Q_NT_TESS_T_1;
                                        Sim_local.perday(d).I.start.E_ESS_T0       = Sim_local.perday(d-1).end.E_ESSstore_T_1;
                                        Sim_local.perday(d).I.start.S_run_CHP_T0   = Sim_local.perday(d-1).end.CHP_run;
                                        if Sim_local.perday(d-1).end.CHP_run==1 % indieser Abfrage wird ermittelt wie lange das BHKW in der Vorperiode schon gelaufen ist.
                                            Sim_local.perday(d).I.start.S_run_CHP_T0_num   = 0;
                                            l=length(Sim_local.perday(d-1).res.S_run_CHP_T);
                                            l_check=Sim_local.perday(d-1).res.S_run_CHP_T(l-1);
                                            while  l_check==1 && l>1
                                                Sim_local.perday(d).I.start.S_run_CHP_T0_num=Sim_local.perday(d).I.start.S_run_CHP_T0_num+1;
                                                l=l-1;
                                                if l>1
                                                    l_check=Sim_local.perday(d-1).res.S_run_CHP_T(l-1);
                                                else
                                                    l_check=0;
                                                end
                                            end
                                        else
                                            Sim_local.perday(d).I.start.S_run_CHP_T0_num   = 0;
                                        end
                                        
                                        
                                        Sim_local.perday(d).I.end.Q_HT_TESS_T0         = Sim_local.par.TESS.Q_max  *0.0;
                                        Sim_local.perday(d).I.end.E_ESS_T0          = Sim_local.par.ESS.E_max   *0.0;
                                        % n�chste Zeile NT noch selbe Qmax
                                        % Werte wie HT
                                        Sim_local.perday(d).I.end.Q_NT_TESS_T0         = Sim_local.par.TESS.Q_max  *0.0;
                                        
                                    end
                                    
                                    Sim_local.perday(d).I_TC.datetime       =  Sim_local.perday(d).I.datetime;
                                    Sim_local.perday(d).I_TC.eps_const      =  Sim_local.perday(d).I.eps_const;
                                    Sim_local.perday(d).I_TC.pd_t           =  Sim_local.perday(d).I.pd_t(1:par.T_C);
                                    Sim_local.perday(d).I_TC.ps_t           =  Sim_local.perday(d).I.pd_t(1:par.T_C);
                                    Sim_local.perday(d).I_TC.PV_t           =  Sim_local.perday(d).I.PV_t(1:par.T_C);
                                    Sim_local.perday(d).I_TC.PDemand_t      =  Sim_local.perday(d).I.PDemand_t(1:par.T_C);
                                    Sim_local.perday(d).I_TC.QDemand_HT_t      =  Sim_local.perday(d).I.QDemand_HT_t(1:par.T_C);
                                    Sim_local.perday(d).I_TC.QDemand_NT_t      =  Sim_local.perday(d).I.QDemand_NT_t(1:par.T_C);
                                    Sim_local.perday(d).I_TC.Pres_t         =  Sim_local.perday(d).I.Pres_t(1:par.T_C);
                                    
                                    %% Optimierung
                                    
                                    [Sim_local.perday(d).res  Sim_local.perday(d).end    Sim_local.perday(d).x_var    Sim_local.perday(d).cplex  Sim_local.perday(d).opt] = RM_OM_alt (  Sim_local.perday(d).par,      Sim_local.perday(d).I,    Sim_local.opt);
                                    Sim_local.perday(d).end.CHP_run=Sim_local.perday(d).res.S_run_CHP_T(Sim_local.perday(d).par.T_C); %Endstatus des CHP runs wird mit?bernommen
                                    
                                    Sim_local.perday(d).t_sim.end      = datetime('now');
                                    Sim_local.TIME.t_sim_end=             Sim_local.perday(d).t_sim.end;
                                    Sim_local.perday(d).t_sim.status   = Sim_local.perday(d).cplex.Solution.statusstring;
                                    sim_t            = Sim_local.perday(d).t_sim.end-Sim_local.perday(d).t_sim.start;
                                    sim_t2           = Sim_local.perday(d).t_sim.end-tges;
                                    sim_status       = Sim_local.perday(d).t_sim.status;
                                    sim_gap          = Sim_local.perday(d).res.mipgap;
                                    
                                    
                                    %% Zuordnung abspeichern der Ergebnisse
                                    Sim_local.perday(d).TIME.t_date         = Sim_local.perday(d).I_TC.datetime;
                                    Sim_local.perday(d).KPI.E_cons         = sum( Sim_local.perday(d).I_TC.PDemand_t )            *par.delta_T/60;
                                    Sim_local.perday(d).KPI.E_PVprod       = sum( Sim_local.perday(d).I_TC.PV_t)                  *par.delta_T/60;
                                    Sim_local.perday(d).KPI.E_PVcurt       = sum( Sim_local.perday(d).res.P_Curt_PV_T)            *par.delta_T/60;
                                    Sim_local.perday(d).KPI.E_PV2grid       = sum( Sim_local.perday(d).res.P_PV_g_T)            *par.delta_T/60;
                                    Sim_local.perday(d).KPI.E_PV2BESS       = sum( Sim_local.perday(d).res.P_PV_s_T)            *par.delta_T/60;
                                    Sim_local.perday(d).KPI.E_PV2dem       = sum( Sim_local.perday(d).res.P_PV_d_T)            *par.delta_T/60;
                                    Sim_local.perday(d).KPI.E_CHP         = sum( Sim_local.perday(d).res.P_CHP_T)                *par.delta_T/60;
                                    Sim_local.perday(d).KPI.E_CHP2grid         = sum( Sim_local.perday(d).res.P_CHP_g_T)                *par.delta_T/60;
                                    Sim_local.perday(d).KPI.E_CHP2BESS         = sum( Sim_local.perday(d).res.P_CHP_s_T)                *par.delta_T/60;
                                    Sim_local.perday(d).KPI.E_CHP2dem         = sum( Sim_local.perday(d).res.P_CHP_d_T)                *par.delta_T/60;
                                    Sim_local.perday(d).KPI.E_THP         = sum( Sim_local.perday(d).res.P_THP_T)                *par.delta_T/60;
                                    Sim_local.perday(d).KPI.E_Gridpur       = sum( Sim_local.perday(d).res.P_Grid_d_T)             *par.delta_T/60;
                                    Sim_local.perday(d).KPI.E_Gridsold      = sum( Sim_local.perday(d).res.P_Grid_s_T)             *par.delta_T/60;
                                    Sim_local.perday(d).KPI.E_ESScrg      = sum( Sim_local.perday(d).res.P_ESScrg_T)             *par.delta_T/60;
                                    Sim_local.perday(d).KPI.E_ESSdcrg     = sum( Sim_local.perday(d).res.P_ESSdcrg_T)            *par.delta_T/60;
                                    Sim_local.perday(d).KPI.E_ESSlost     = sum( Sim_local.perday(d).res.E_ESSlosses_T)          *par.delta_T/60;
                                    
                                    Sim_local.perday(d).KPI.h_CHP         = sum( Sim_local.perday(d).res.S_run_CHP_T)             *par.delta_T/60;
                                    Sim_local.perday(d).KPI.s_CHP         = sum( Sim_local.perday(d).res.S_sup_CHP_T)           ;
                                    %Sim_local.perday(d).KPI.ESS_SOCmin      = Sim_local.perday(d).res.D_SOCMIN;
                                    %Sim_local.perday(d).KPI.ESS_SOCmax      = Sim_local.perday(d).res.D_SOCMAX;
                                    %Sim_local.perday(d).KPI.ESS_DOD         = Sim_local.perday(d).res.D_DODper;
                                    Sim_local.perday(d).KPI.H_cons_HT          = sum( Sim_local.perday(d).I_TC.QDemand_HT_t )            *par.delta_T/60;
                                    Sim_local.perday(d).KPI.H_cons_NT          = sum( Sim_local.perday(d).I_TC.QDemand_NT_t )            *par.delta_T/60;
                                    Sim_local.perday(d).KPI.H_CHP           = sum( Sim_local.perday(d).res.Qd_HT_CHP_T)                *par.delta_T/60;
                                    Sim_local.perday(d).KPI.H_THP           = sum( Sim_local.perday(d).res.Qd_THP_T)                *par.delta_T/60;
                                    Sim_local.perday(d).KPI.H_HTR           = sum( Sim_local.perday(d).res.Qd_HT_HTR_T)                *par.delta_T/60;
                                    %die folgenden 2 Zeilen sind Auswertung
                                    %f�r Laden und Entladen thermischer HT
                                    %Speicher, das gibt es f�r NT noch
                                    %nicht
                                    %die n�chsten 2 sind die, die original
                                    %drin waren, bevor thermisches
                                    %Speichermodell ge�ndert wurde
                                    %Sim_local.perday(d).KPI.H_TESScrg       = sum( Sim_local.perday(d).res.Qd_HT_TESS_T(Sim_local.perday(d).res.Qd_HT_TESS_T>0))                *par.delta_T/60;
                                    %Sim_local.perday(d).KPI.H_TESSdcrg      = sum( Sim_local.perday(d).res.Qd_HT_TESS_T(Sim_local.perday(d).res.Qd_HT_TESS_T<0))                *par.delta_T/60;
                                    Sim_local.perday(d).KPI.H_TESScrg       = sum( Sim_local.perday(d).res.Qd_HT_TESS_in_T)                *par.delta_T/60;
                                    Sim_local.perday(d).KPI.H_TESSdcrg      = sum( Sim_local.perday(d).res.Qd_HT_TESS_out_T_1)                *par.delta_T/60;
                                    %Sim_local.perday(d).KPI.H_curt          = sum( Sim_local.perday(d).res.Qd_delta_T)                *par.delta_T/60;
                                    
                                    Sim_local.perday(d).KPI.OF_Cost_Grid_d     = sum( Sim_local.perday(d).res.C_Grid_d_T);
                                    Sim_local.perday(d).KPI.OF_Cost_Grid_s_PV     = sum( Sim_local.perday(d).res.C_Grid_s_PV_T);
                                    Sim_local.perday(d).KPI.OF_Cost_Grid_s_CHP     = sum( Sim_local.perday(d).res.C_Grid_s_CHP_T);
                                    Sim_local.perday(d).KPI.OF_Cost_Cust_PV       = sum( Sim_local.perday(d).res.C_Cust_PV_T);
                                    Sim_local.perday(d).KPI.OF_Cost_Cust_CHP       = sum( Sim_local.perday(d).res.C_Cust_CHP_T);
                                    Sim_local.perday(d).KPI.OF_Cost_Custheat_HT   = sum( Sim_local.perday(d).res.C_Custheat_HT_T);
                                    Sim_local.perday(d).KPI.OF_Cost_Custheat_NT   = sum( Sim_local.perday(d).res.C_Custheat_NT_T);
                                    Sim_local.perday(d).KPI.OF_Cost_THP_in       = sum( Sim_local.perday(d).res.C_THP_T);
%                                     Sim_local.perday(d).KPI.OF_Cost_BESS_in       = sum( Sim_local.perday(d).res.C_BESS_in_T);
                                    Sim_local.perday(d).KPI.OF_Cost_CHP_run    = sum( Sim_local.perday(d).res.C_run_CHP_T);
                                    Sim_local.perday(d).KPI.OF_Cost_CHP_sup    = sum( Sim_local.perday(d).res.C_sup_CHP_T);
                                    Sim_local.perday(d).KPI.OF_Cost_HTR        = sum( Sim_local.perday(d).res.C_HTR_T);
                                    Sim_local.perday(d).KPI.OF_Cost_CYC        = sum( Sim_local.perday(d).res.C_CYC);
                                    Sim_local.perday(d).KPI.OF_Cost_CAC        = sum( Sim_local.perday(d).res.C_CAC_T);
                                    Sim_local.perday(d).KPI.OF_Cost_LCC        = Sim_local.perday(d).KPI.OF_Cost_CAC + Sim_local.perday(d).KPI.OF_Cost_CYC;
                                    
                                    Sim_local.perday(d).KPI.OF_Cost            = Sim_local.perday(d).KPI.OF_Cost_Grid_d + Sim_local.perday(d).KPI.OF_Cost_Grid_s_PV + Sim_local.perday(d).KPI.OF_Cost_Grid_s_CHP + Sim_local.perday(d).KPI.OF_Cost_Cust_PV + Sim_local.perday(d).KPI.OF_Cost_Cust_CHP + Sim_local.perday(d).KPI.OF_Cost_Custheat_HT + Sim_local.perday(d).KPI.OF_Cost_Custheat_NT + Sim_local.perday(d).KPI.OF_Cost_THP_in + Sim_local.perday(d).KPI.OF_Cost_CHP_run + Sim_local.perday(d).KPI.OF_Cost_CHP_sup + Sim_local.perday(d).KPI.OF_Cost_HTR + Sim_local.perday(d).KPI.OF_Cost_CYC + Sim_local.perday(d).KPI.OF_Cost_CAC + Sim_local.perday(d).KPI.OF_Cost_LCC;   
                                    
                                    Sim_local.perday(d).KPI.OF_Aut_z          = sum( Sim_local.perday(d).res.P_Grid_d_T)          *par.delta_T/60;
                                    Sim_local.perday(d).KPI.OF_Aut_n          = sum( Sim_local.perday(d).I.Pres_t_pos)            *par.delta_T/60;
                                    Sim_local.perday(d).KPI.OF_Autarchy       = (1-    Sim_local.perday(d).KPI.OF_Aut_z  /   Sim_local.perday(d).KPI.OF_Aut_n )    *   100;
                                    
                                    Sim_local.perday(d).KPI.Tot_CHP2BESS_p100           =   Sim_local.perday(d).KPI.E_CHP2BESS/Sim_local.perday(d).KPI.E_CHP*100;
                                    Sim_local.perday(d).KPI.Tot_CHP2grid_p100           =   Sim_local.perday(d).KPI.E_CHP2grid/Sim_local.perday(d).KPI.E_CHP*100;
                                    Sim_local.perday(d).KPI.Tot_CHP2dem_p100            =   Sim_local.perday(d).KPI.E_CHP2dem/Sim_local.perday(d).KPI.E_CHP*100;
                                    Sim_local.perday(d).KPI.Tot_PV2BESS_p100            =   Sim_local.perday(d).KPI.E_PV2BESS/Sim_local.perday(d).KPI.E_PVprod*100;
                                    Sim_local.perday(d).KPI.Tot_PV2dem_p100            =   Sim_local.perday(d).KPI.E_PV2dem/Sim_local.perday(d).KPI.E_PVprod*100;
                                    Sim_local.perday(d).KPI.Tot_PV2grid_p100            =   Sim_local.perday(d).KPI.E_PV2grid/Sim_local.perday(d).KPI.E_PVprod*100;
                                    
                                    if d==1
                                        Sim_local.KPI  = Sim_local.perday(d).KPI;
                                        Sim_local.TIME.t_start  = Sim_local.perday(d).I_TC.datetime;
                                        Sim_local.res  = Sim_local.perday(d).res;
                                        Sim_local.I    = Sim_local.perday(d).I_TC;
                                    else
                                        Sim_local.TIME.t_end  = Sim_local.perday(d).I_TC.datetime;
                                        if opt.save_conc_res==1
                                            [Sim_local.res Sim_local.KPI Sim_local.I]=concatenateResults_RM02(Sim_local.res, Sim_local.KPI, Sim_local.I, Sim_local.perday(d).res, Sim_local.perday(d).KPI, Sim_local.perday(d).I_TC);
                                        else
                                            [Sim_local.KPI ]=concatenateResults_RM02_reduced( Sim_local.KPI, Sim_local.perday(d).KPI);
                                            Sim_local.perday(d).cplex=[];
                                        end
                                    end
                                    if opt.save_cplex==0
                                        Sim_local.perday(d).cplex=[];
                                    end
                                    
                                   S=sprintf(strcat('ParVar: ', num2str(pv),'/',num2str( p16max),' -- Period: ', num2str(period),'/',num2str(par.periods), ' -- Eps: ', num2str(eps), '/', num2str(eps_max), ' -- Day: ', num2str(d), '/', num2str(par.T_total), ' -- t_iter: ', datestr(sim_t,'HH:MM:SS'), ' -- t_ges: ', datestr(sim_t2,'dd:HH:MM:SS'),' -- gap: ', num2str( ceil(sim_gap*1000)/10 ), ' %%', ' -- status: ', sim_status));
                                    
                                    disp(S);
                                    
                                    if opt.save_conc_res==1
                                        if d==par.T_total
                                            Sim_local.res.E_ESSstore_T_1(par.T_total*24*par.delta_T/60)    =   Sim_local.perday(d).end.E_ESSstore_T_1;
                                            Sim_local.res.Q_HT_TESS_T_1(par.T_total*24*par.delta_T/60)        =   Sim_local.perday(d).end.Q_HT_TESS_T_1;
                                            Sim_local.res.Q_NT_TESS_T_1(par.T_total*24*par.delta_T/60)        =   Sim_local.perday(d).end.Q_NT_TESS_T_1;
                                        end
                                    end
                                    

                                end
                                %Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v)=Sim_local;

                                
                                %Sol.Sim_pd(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).day=Sim_local.perday;
                                %Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).res=Sim_local.res;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).ID=pv;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).par=Sim_local.par;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI=Sim_local.KPI;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).opt=Sim_local.opt;
                                
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_BatteryLifetimePrediction         =   Sim_local.par.ESS.E_inv/Sim_local.KPI.OF_Cost_LCC *Sim_local.par.T_total/365;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_BatteryAvgCycDay        =   Sim_local.KPI.E_ESScrg *Sim_local.par.ESS.eta_c / Sim_local.par.ESS.E /Sim_local.par.T_total;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_BatteryAvgSOC_p100      =    sum( Sim_local.res.E_ESSstore_T_1) / (Sim_local.par.T_total*24*par.delta_T/60) /Sim_local.par.ESS.E*100;    
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_Profit                  =   -Sim_local.KPI.OF_Cost;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_GridPurchase_p100       =    Sim_local.KPI.E_Gridpur/(Sim_local.KPI.E_cons + Sim_local.KPI.E_ESSlost)*100;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_GridSold_p100           =    Sim_local.KPI.E_Gridsold/(Sim_local.KPI.E_CHP + Sim_local.KPI.E_PVprod)*100;
                                %in den folgenden 3 Zeilen ist
                                %KPI.H_cons_HT verwendet, das gibt es auch
                                %f�r NT, nur die folgenden 3 Zeilen gibt es
                                %nicht f�r NT, die m�ssen noch gebaut
                                %werden, ich wei� aber noch nicht, was sie
                                %besagen
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_CHP_heatshare_p100      =    Sim_local.KPI.H_CHP/(Sim_local.KPI.H_cons_HT + Sim_local.KPI.H_TESScrg + Sim_local.KPI.H_TESSdcrg)*100;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_THP_heatshare_p100      =    Sim_local.KPI.H_THP/(Sim_local.KPI.H_cons_HT + Sim_local.KPI.H_TESScrg + Sim_local.KPI.H_TESSdcrg)*100;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_HTR_heatshare_p100      =    Sim_local.KPI.H_HTR/(Sim_local.KPI.H_cons_HT + Sim_local.KPI.H_TESScrg + Sim_local.KPI.H_TESSdcrg)*100;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_TESSAvgCycDay           =    (Sim_local.KPI.H_TESScrg - Sim_local.KPI.H_TESSdcrg) /2 / Sim_local.par.TESS.Q_max /Sim_local.par.T_total;
                                %n�chste Zeile gibt es nur f�r HT und noch
                                %nicht f�r NT, wei� noch nicht, was sie
                                %aussagt
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_TESSAvgSOC_p100         =    sum( Sim_local.res.Q_HT_TESS_T_1) / (Sim_local.par.T_total*24*par.delta_T/60) /Sim_local.par.TESS.Q_max*100;    
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_CHP_AvgP_p100           =    Sim_local.KPI.E_CHP/Sim_local.KPI.h_CHP/Sim_local.par.CHP.P*100;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_CHP_h_to_sup          	=    Sim_local.KPI.h_CHP/Sim_local.KPI.s_CHP;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_CHP_h                   =    Sim_local.KPI.h_CHP;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_CHP2BESS_p100           =   Sim_local.KPI.E_CHP2BESS/Sim_local.KPI.E_CHP*100;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_CHP2grid_p100           =   Sim_local.KPI.E_CHP2grid/Sim_local.KPI.E_CHP*100;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_CHP2dem_p100            =   Sim_local.KPI.E_CHP2dem/Sim_local.KPI.E_CHP*100;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_PV2BESS_p100            =   Sim_local.KPI.E_PV2BESS/Sim_local.KPI.E_PVprod*100;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_PV2dem_p100             =   Sim_local.KPI.E_PV2dem/Sim_local.KPI.E_PVprod*100;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.Tot_PV2grid_p100            =   Sim_local.KPI.E_PV2grid/Sim_local.KPI.E_PVprod*100;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).TIME.t_sim_start                =    Sim_local.TIME.t_sim_start;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).TIME.t_sim_end                  =    Sim_local.TIME.t_sim_end;
                                Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).KPI.t_sim_delta                 =    Sim_local.TIME.t_sim_end-Sim_local.TIME.t_sim_start;
                                %Sol.Sim(eps,period,p1v,p2v,p3v,p4v,p5v,p6v).I=Sim_local.I;
                                
                                cd(folder);
                                name=strcat('Sim','_', num2str(pv), '_', num2str(p16max));
                                eval(sprintf(strcat(name,' =Sim_local;')));
                                save(strcat(name,'.mat'),name);
                                save('Sol.mat','Sol');
                                writexlsx(Sol, folder, pv);
                                cd(folderold);

                            end
                        end
                    end
                end
            end
        end
    end
end

if opt.n_workers>1
    delete(poolobj)
end


% folderold=pwd;
% folder=strcat(folderold,'/02 Residential Microgrid/Output/');
cd(folder);
name='Sol';
save(strcat(name,'.mat'),name);

name2=strcat('Sim_1_',num2str(p16max));
load(name2);
eval(sprintf(strcat('Sim1 = ',name2,';')));

writexlsx(Sol, folder, p16max);

cd(folderold);

clearvars -except Sol Sim1

%Resultplots_single(Sim(1,1,1).res, Sim(1,1,1).KPI, par, Sim(1,1,1).I);
Resultplots_v2(Sim1);



