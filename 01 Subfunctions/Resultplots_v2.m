function resultplots_v2(Sim)
res=Sim.res;
KPI=Sim.KPI;
par=Sim.par;
I=Sim.I;
%handles=findall(0,'type','figure');
%handles.delete;
Nplots=5;

%% Netzbezug und -Einspeisung 
        figure('Position',[20 100 700 700])
    
        h(1)=subplot(Nplots,3,1);
        hold all;
        grid on;
        stairs(I.pd_t);
        ylabel('in EUR/MWh');
        legend('Price in DAM');
        %plot(zeros(size(result_z_G_demand)), 'k'); % schwarze Null-Linie zeichnen
        xlim([0 24*60/par.delta_T*par.T_total]);
        set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
        
        %% Nachfrage
    
        h(2)=subplot(Nplots,3,2);
        hold all;
        grid on;
        stairs(I.PDemand_t);
        stairs(I.QDemand_HT_t);
        stairs(I.QDemand_NT_t);
        ylabel('in kW');
        legend('E Nachfrage','HT Nachfrage','NT Nachfrage' );
        %plot(zeros(size(result_z_G_demand)), 'k'); % schwarze Null-Linie zeichnen
        xlim([0 24*60/par.delta_T*par.T_total]);
        set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
        
       %% Plot f?r NT Speicher - Inhalt und Leistungen
        minA = min(res.Qd_NT_TESS_out_T);
        %maxB = max(res.Qd_NT_TESS_PenaltyAdd_T);
        maxC = max(res.Qd_NT_TESS_in_T);
        maxD = max(res.Qd_NT_SH_s_T);
        maxE = max(res.Qd_NT_TESS_V_T);
        %maxF = max(res.Q_NT_TESS_losses_T);
        maxG = [maxC-minA maxD+maxE];
        hold off;
        h(12)=subplot(Nplots,3,12);
        hold all;
        grid on;
        yyaxis left
        ylabel('in kW');
        ylim([-ceil(max(maxG)) ceil(max(maxG))]);
        stairs(res.Qd_NT_TESS_in_T - res.Qd_NT_TESS_out_T,'b-');
        %stairs(res.Qd_NT_TESS_PenaltyAdd_T);
        %stairs(res.Qd_NT_TESS_in_T);
        stairs(res.Qd_NT_SH_s_T + res.Qd_NT_TESS_V_T,'g-');
        %stairs(res.Qd_NT_TESS_V_T);
        %stairs(-res.Q_NT_TESS_losses_T);
        
        yyaxis right
        ylabel('in kWh');
        ylim([-ceil(max(res.Q_NT_TESS_losses_T)) par.NT.TESS.Q_max]);
        plot(res.Q_NT_TESS_T_1,'r-');
        plot(-res.Q_NT_TESS_losses_T,'m-');
        
        xlim([0 24*60/par.delta_T*par.T_total]);
        set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
      
        

        %legend('E^{bat}', 'P^{(ch)}', 'P^{(dch)}', 'z^{charge}', 'z^{discharge}', 'P_{curtail} (PV)');
        legend('P^{TESS}_{Kreis}','P^{TESS}_{Zu}','E^{TESS}','E^{TESS}_{Verlust}'); 
        
        %% Temperatur�bersicht
        hold off;
        h(6)=subplot(Nplots,3,6);
        hold all;
        grid on;
        stairs(res.T_NT_TESS_out_T_1);
        stairs(res.T_NT_delta_WT_out_T);
        stairs(res.T_THP_in_T);
        stairs(res.T_NT_TESS_in_T);
        legend('T^{TESS Ausgang}', 'deltaT^{WT}', 'T^{WP Eingang}', 'T^{TESS Eingang}');
        ylabel('in Grad Celcius');
        xlim([0 24*60/par.delta_T*par.T_total]);
        set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
        
        %% Energy production
        hold off;
<<<<<<< HEAD
        h(7)=subplot(Nplots,3,7);
=======
        h(4)=subplot(Nplots,3,4);
>>>>>>> a5db43fa2f9eb627606998725125735d876dd0de
        hold all;
        grid on;
        stairs(I.PV_t());
        stairs(res.P_Curt_PV_T);
        stairs(res.P_CHP_T);
        legend('P^{PV}_{elec}', 'P^{PV Curtailment}_{elec}', 'P^{CHP}_{elec}');
        ylabel('in kW');
        xlim([0 24*60/par.delta_T*par.T_total]);
        set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
        
        %% NT production
        hold off;
        h(9)=subplot(Nplots,3,9);
        hold all;
        grid on;
        stairs(res.Qd_NT_SH_s_T + res.Qd_NT_SH_Curt_T,'r-');
        stairs(res.Qd_NT_SH_Curt_T,'r:');
        stairs(res.P_THP_T,'g-');
        stairs(res.Qd_NT_WT_T,'b-');
        legend('H^{SH Ges}', 'H^{SH Curt}', 'P^{THP}', 'H^{WT}');
        ylabel('in kW');
        xlim([0 24*60/par.delta_T*par.T_total]);
        set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
        set(gca,'XTickLabel', [0:12:24*par.T_total])

        %% Plot f?r elektrischen Energiespeicher - Inhalt und Leistungen
        hold off;
        h(10)=subplot(Nplots,3,10);
        hold all;
        grid on;
        yyaxis left
        ylabel('in kW');
        ylim([-ceil(max(res.P_ESSdcrg_T)) ceil(max(res.P_ESScrg_T))]);
        stairs(res.P_ESScrg_T-res.P_ESSdcrg_T, 'b-');
        
        yyaxis right
        ylabel('in kWh');
        ylim([0 par.ESS.E_max]);
        plot(res.E_ESSstore_T_1,'r-');

        xlim([0 24*60/par.delta_T*par.T_total]);
        set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
      
        

        %legend('E^{bat}', 'P^{(ch)}', 'P^{(dch)}', 'z^{charge}', 'z^{discharge}', 'P_{curtail} (PV)');
        legend('P^{ESS}_{elec}', 'E^{ESS}_{elec}');

        


        %% Grid
        hold off;
<<<<<<< HEAD
        h(4)=subplot(Nplots,3,4);
=======
        h(7)=subplot(Nplots,3,7);
>>>>>>> a5db43fa2f9eb627606998725125735d876dd0de
        hold all;
        grid on;
        stairs(res.P_Grid_d_T-res.P_Grid_s_T);
        stairs(I.Pres_t);
        legend('Grid','P^{residual}_{elec}' );
        ylabel('in kW');
        
        xlim([0 24*60/par.delta_T*par.T_total]);
        set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
        
        
        %% Total Production
        hold off;
        h(13)=subplot(Nplots,3,13);
        hold all;
        grid on

        prod=(res.P_Grid_d_T  + res.P_ESSdcrg_T + I.PV_t - res.P_Curt_PV_T + res.P_CHP_T)*par.delta_T/60;
        cons=I.PDemand_t*par.delta_T/60 + (res.P_Grid_s_T + res.P_ESScrg_T + res.P_THP_T + res.P_help_T)*par.delta_T/60;
        y=[prod';cons'];
        bar(y')

        xlim([0 24*60/par.delta_T*par.T_total]);
        set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
        
        legend('Production^{total}_{elec}', 'Consumption^{total}_{elec}');
        xlabel('time');
        ylabel('in kWh')

        
        
        %% W?rme CHP bin
        hold off;
        h(5)=subplot(Nplots,3,5);
        hold all;
        grid on
        stem(res.S_run_CHP_T, '-');  
        stem(2*res.S_sup_CHP_T, '-');
        stem(-1*res.S_off_CHP_T, '-');
        ylim([-1 2]);
        set(gca,'yTick', [-1:1:2])
        set(gca,'YTickLabel',{'off','', 'on','setup'})
        
        
        legend('CHP running', 'CHP setup');
        xlabel('time');
        ylabel('binary')
        
        
        xlim([0 24*60/par.delta_T*par.T_total]);
        set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
        
        %% W?rme CHP and others
        hold off;
        h(8)=subplot(Nplots,3,8);
        hold all;
        grid on
        max11=max(res.Qd_HT_CHP_T);
        max12=max(res.Qd_HT_HTR_T);
        max13=max([max11 max12]);
        
        stairs(res.Qd_HT_CHP_T, '-');  
        stairs(res.Qd_HT_HTR_T, '-');
        %stairs(res.Qd_THP_T, '-');
        %stairs(res.Qd_delta_T, '-');
        
        ylim([0 max13]);
        xlim([0 24*60/par.delta_T*par.T_total]);
        set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
        
        legend('P^{CHP}_{thermal}', 'P^{HTR}_{thermal}');
        xlabel('time');
        ylabel('in kW')
        
        %% W?rme TESS
        hold off;
        h(11)=subplot(Nplots,3,11);
        hold all;
        grid on
        yyaxis left
        stairs(res.Qd_HT_TESS_in_T - res.Qd_HT_TESS_out_T_1, '-');
        xlabel('time');
        ylabel('in kW')
        
        yyaxis right
         plot(res.Q_HT_TESS_T_1, '-');  
        ylabel('in kWh');
        ylim([0 par.HT.TESS.Q_max]);
        legend('P^{TESS}_{thermal}', 'E^{TESS}_{thermal}');
        
        
        xlim([0 24*60/par.delta_T*par.T_total]);
        set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
        
        %% HT W?rme Production
        hold off;
<<<<<<< HEAD
        h(14)=subplot(Nplots,3,14);
=======
        h(^x14)=subplot(Nplots,3,14);
>>>>>>> a5db43fa2f9eb627606998725125735d876dd0de
        hold all;
        grid on
        name={};
        maxY=0;
        prod=res.Qd_HT_CHP_T + res.Qd_HT_TESS_out_T_1+res.Qd_HT_HTR_T;
        cons=1.15 * I.QDemand_HT_t + res.Qd_HT_TESS_in_T + res.Qd_HT_WT_T;
        y=[prod';cons'];
        bar(y')
        xlim([0 length(y)]);
        
        legend('Production^{total}_{HT thermal}', 'Consumption^{total}_{HT thermal}');
        xlabel('time');
        ylabel('in kWh')
        
        xlim([0 24*60/par.delta_T*par.T_total]);
        set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
        
        %set(gcf,'Units','centimeters','PaperPosition',[0 0 32 32],'PaperPositionMode','manual','PaperSize',[32 32])
        %print(gcf,'-r300','-dtiff', '-opengl','/Users/Timm/Dropbox/Promotion/SimulationResultPlot')
        
        linkaxes(h,'x')
        
        %% �bersicht Gleichgewicht elektrischer Kreis
         name_str = '�bersicht Gleichgewicht E Kreis';
        figure('Position',[20 100 700 700],'Name',name_str,'NumberTitle','off')
        hold off;
        hold all;
        grid on
        YCOMP=[ -KPI.E_ESSlost,  KPI.E_ESSdcrg_ges, -KPI.E_ESScrg, -KPI.E_Gridsold,  KPI.E_Gridpur, -KPI.E_help, -KPI.E_THP,  KPI.E_CHP, -KPI.E_PVcurt, KPI.E_PVprod,  -KPI.E_Demand];
        %x=[1,2,3,4,5,6,7,8,9,10,11];
         x=[1,2,3,4,5,6,7,8,9,10,11];
        ax2=barh(YCOMP);
        set(gca,'YTick',0:1:11); 
        %XTick(ax2,[0 1 2 3 5 6 8 9 10]);
        set(gca,'YTickLabel',{'','P_E_S_S^{losses}', 'P_E_S_S^{discharge}','P_E_S_S^{charge}', 'Grid^{supply}', 'Grid^{demand}','Help^{cons}','THP^{cons}', 'CHP^{prod}','PV^{curt}','PV^{prod}', 'Demand'})
        xlabel('in kWh');
        for i1=1:numel(YCOMP)
            if YCOMP(i1)>=0
                text(YCOMP(i1),x(i1),num2str(YCOMP(i1),'%0.2f'),...
                       'HorizontalAlignment','left',...
                       'VerticalAlignment','bottom')
            else
                text(YCOMP(i1),x(i1),num2str(YCOMP(i1),'%0.2f'),...
                       'HorizontalAlignment','right',...
                       'VerticalAlignment','bottom')
            end
        end
        
        %% �bersicht Gleichgewicht HT Kreis
        name_str = '�bersicht Gleichgewicht HT Kreis';
        figure('Position',[20 100 700 700],'Name',name_str,'NumberTitle','off')
        hold off;
        hold all;
        grid on
        YCOMP=[ -KPI.H_HT_TESSlost,  KPI.H_HT_TESSdcrg, -KPI.H_HT_TESScrg, -KPI.H_WT_HT, KPI.H_CHP,  KPI.H_HTR, -KPI.H_HT_Verluste, -KPI.H_Demand_HT];
        %x=[1,2,3,4,5,6,7,8,9,10,11];
         x=[1,2,3,4,5,6,7,8];
        ax2=barh(YCOMP);
        set(gca,'YTick',0:1:8); 
        %XTick(ax2,[0 1 2 3 5 6 8 9 10]);
        set(gca,'YTickLabel',{'','H_T_E_S_S^{losses}', 'H_T_E_S_S^{discharge}','H_T_E_S_S^{charge}','H^{WT}', 'H^{CHP}', 'H^{Heater}','H^{Verluste}','H^{Demand}', 'CHP^{prod}','PV^{curt}','PV^{prod}', 'Demand'})
        xlabel('in kWh');
        for i1=1:numel(YCOMP)
            if YCOMP(i1)>=0
                text(YCOMP(i1),x(i1),num2str(YCOMP(i1),'%0.2f'),...
                       'HorizontalAlignment','left',...
                       'VerticalAlignment','bottom')
            else
                text(YCOMP(i1),x(i1),num2str(YCOMP(i1),'%0.2f'),...
                       'HorizontalAlignment','right',...
                       'VerticalAlignment','bottom')
            end
        end
        
        %% �bersicht Gleichgewicht NT Kreis
        name_str = '�bersicht Gleichgewicht NT Kreis';
        figure('Position',[20 100 700 700],'Name',name_str,'NumberTitle','off')
        hold off;
        hold all;
        grid on
        YCOMP=[ -KPI.H_NT_TESSlost,  KPI.E_THP, KPI.H_TESS_loss_coupling, -KPI.H_SHcurt,  KPI.H_SHprod, KPI.H_WTin, -KPI.H_Demand_NT];
        %x=[1,2,3,4,5,6,7,8,9,10,11];
         x=[1,2,3,4,5,6,7,8];
        ax2=barh(YCOMP);
        set(gca,'YTick',0:1:8); 
        %XTick(ax2,[0 1 2 3 5 6 8 9 10]);
        set(gca,'YTickLabel',{'','H_T_E_S_S^{losses}', 'E^{THP}','H_T_E_S_S^{Coupling}', 'H^{SH Curt}', 'H^{SH}','H^{WT in}','H^{Demand}', 'CHP^{prod}','PV^{curt}','PV^{prod}', 'Demand'})
        xlabel('in kWh');
        for i1=1:numel(YCOMP)
            if YCOMP(i1)>=0
                text(YCOMP(i1),x(i1),num2str(YCOMP(i1),'%0.2f'),...
                       'HorizontalAlignment','left',...
                       'VerticalAlignment','bottom')
            else
                text(YCOMP(i1),x(i1),num2str(YCOMP(i1),'%0.2f'),...
                       'HorizontalAlignment','right',...
                       'VerticalAlignment','bottom')
            end
        end
        
        %% �bersicht Kosten und Erl�se
        name_str = '�bersicht Kosten und Erl�se';
        figure('Position',[20 100 700 700],'Name',name_str,'NumberTitle','off')
        hold off;
        hold all;
        grid on
        YCOMP=[KPI.OF_Cost_Penalty, KPI.OF_Cost_LCC,  KPI.OF_Cost_Grid_d, KPI.OF_Cost_Grid_s_PV, KPI.OF_Cost_Grid_s_CHP, KPI.OF_Cost_Cust_PV,  KPI.OF_Cost_Cust_CHP, KPI.OF_Cost_THP_in, KPI.OF_Cost_Help, KPI.OF_Cost_Custheat_NT, KPI.OF_Cost_Custheat_HT, KPI.OF_Cost_CHP_run, KPI.OF_Cost_HTR, KPI.OF_Cost-KPI.OF_Cost_Penalty];
        %x=[1,2,3,4,5,6,7,8,9,10,11];
         x=[1,2,3,4,5,6,7,8,9,10,11,12,13,14];
        ax2=barh(YCOMP);
        set(gca,'YTick',0:1:14); 
        %XTick(ax2,[0 1 2 3 5 6 8 9 10]);
        set(gca,'YTickLabel',{'','C^{Penalty}','C^{BatterieAlterung}', 'C^{Stromzukauf}','C^{PV Netz}','C^{CHP Netz}', 'C^{PV Kunde}', 'C^{CHP Kunde}','C^{THP lokal}','C^{Hilfsenergie lokal}', 'C^{NT W�rme}','C^{HT W�rme}','C^{CHP}', 'C^{Heater}','C^{Gesamt}'})
        xlabel('in kWh');
        for i1=1:numel(YCOMP)
            if YCOMP(i1)>=0
                text(YCOMP(i1),x(i1),num2str(YCOMP(i1),'%0.2f'),...
                       'HorizontalAlignment','left',...
                       'VerticalAlignment','bottom')
            else
                text(YCOMP(i1),x(i1),num2str(YCOMP(i1),'%0.2f'),...
                       'HorizontalAlignment','right',...
                       'VerticalAlignment','bottom')
            end
        end
        
%         %% NT Speicher nochmal gro�
%         figure('Position',[20 100 700 700])
%         hold off;
%         %h(3)=subplot(Nplots,3,12);
%         hold all;
%         grid on;
%         yyaxis left
%         ylabel('in kW');
%         ylim([-ceil(max(maxG)) ceil(max(maxG))]);
%         stairs(-res.Qd_NT_TESS_out_T);
%         stairs(res.Qd_NT_TESS_PenaltyAdd_T);
%         stairs(res.Qd_NT_TESS_in_T);
%         stairs(res.Qd_NT_SH_s_T);
%         stairs(res.Qd_NT_TESS_V_T);
%         stairs(-res.Q_NT_TESS_losses_T);
%         stairs(res.Qd_NT_TESS_V_T + res.Qd_NT_SH_s_T + res.Qd_NT_TESS_in_T + res.Qd_NT_TESS_PenaltyAdd_T -res.Qd_NT_TESS_out_T-res.Q_NT_TESS_losses_T,'g-');
%         
%         yyaxis right
%         ylabel('in kWh');
%         ylim([par.NT.TESS.Q_min par.NT.TESS.Q_max]);
%         plot(res.Q_NT_TESS_T_1,'r-');
% 
%         xlim([0 24*60/par.delta_T*par.T_total]);
%         set(gca,'XTick', [0:1*60/par.delta_T:24*par.T_total*60/par.delta_T])
%         set(gca,'XTickLabel', [0:1:24*par.T_total])
%       
%         
% 
%         %legend('E^{bat}', 'P^{(ch)}', 'P^{(dch)}', 'z^{charge}', 'z^{discharge}', 'P_{curtail} (PV)');
%         legend('H^{TESS}_{out}','H^{TESS}_{Penalty}','H^{TESS}_{in}','H^{SH}','H^{Kopplung}','H^{TESS}_{losses}','H^{Gesamt}','E^{TESS}'); 
        
%         %% SH Curt Untersuchung
%         figure('Position',[20 100 700 700])
%         hold off;
%         %h(3)=subplot(Nplots,3,12);
%         hold all;
%         grid on;
%         yyaxis left
%         ylabel('in kW');
%         ylim([-ceil(max(maxG)) ceil(max(maxG))]);
%         %stairs(-res.Qd_NT_TESS_out_T);
%         %stairs(res.Qd_NT_TESS_PenaltyAdd_T);
%         %stairs(res.Qd_NT_TESS_in_T);
%         stairs(res.Qd_NT_SH_s_T,'b-');
%         stairs(res.Qd_NT_SH_Curt_T,'g-');
%         %stairs(res.Qd_NT_TESS_V_T);
%         %stairs(-res.Q_NT_TESS_losses_T);
%         %stairs(res.Qd_NT_TESS_V_T + res.Qd_NT_SH_s_T + res.Qd_NT_TESS_in_T + res.Qd_NT_TESS_PenaltyAdd_T -res.Qd_NT_TESS_out_T-res.Q_NT_TESS_losses_T,'g-');
%         
%         yyaxis right
%         ylabel('in kWh');
%         ylim([par.NT.TESS.Q_min par.NT.TESS.Q_max]);
%         plot(res.Q_NT_TESS_T_1,'r-');
% 
%         xlim([0 24*60/par.delta_T*par.T_total]);
%         set(gca,'XTick', [0:1*60/par.delta_T:24*par.T_total*60/par.delta_T])
%         set(gca,'XTickLabel', [0:1:24*par.T_total])
%       
%         
% 
%         %legend('E^{bat}', 'P^{(ch)}', 'P^{(dch)}', 'z^{charge}', 'z^{discharge}', 'P_{curtail} (PV)');
%         legend('H^{SH}','H^{SH Curt}','E^{TESS}'); 
        
        %% HT Bedarf Erf�llung
     name_str = 'HT Bedarf Erf�llung';
        figure('Position',[20 100 700 700],'Name',name_str,'NumberTitle','off')
        hold off;
        %h(3)=subplot(Nplots,3,12);
        hold all;
        grid on;
        yyaxis left
        ylabel('in kW');
        
        prod=res.Qd_HT_CHP_T +res.Qd_HT_HTR_T;
        cons=1.15 * I.QDemand_HT_t + res.Qd_HT_WT_T;
        
        max1 = max(prod);
        max2 = max(cons);
        max3 = [max1 max2];
        
         ylim([-ceil(max(max3)) ceil(max(max3))]);
        %ylim ([-200 200]);
         stairs(res.Qd_HT_CHP_T,'y-');
         stairs(res.Qd_HT_HTR_T,'b-');
         stairs(-res.Qd_HT_WT_T,'r-');
         stairs(-1.15 * I.QDemand_HT_t,'g-');
         
       % stairs(prod - cons,'g-');
       % stairs(prod);
        %stairs(cons,'b-');
        stairs(res.Qd_HT_TESS_in_T,'c-');
        stairs(-res.Qd_HT_TESS_out_T_1,'m-');
        
%         yyaxis right
%         ylabel('in kWh');
%         ylim([0 par.HT.TESS.Q_max]);
%         plot(res.Q_HT_TESS_T_1,'r-');

        xlim([0 24*60/par.delta_T*par.T_total]);
        set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
      
        

        %legend('E^{bat}', 'P^{(ch)}', 'P^{(dch)}', 'z^{charge}', 'z^{discharge}', 'P_{curtail} (PV)');
        legend('prodCHP','prodHTR','consWT','consDemand','Einspeichern','Ausspeichern'); 
        
        
         %% NT Bedarf Erf�llung
        name_str = 'NT Bedarf Erf�llung';
        figure('Position',[20 100 700 700],'Name',name_str,'NumberTitle','off')
        hold off;
        %h(3)=subplot(Nplots,3,12);
        hold all;
        grid on;
        name_str = 'test'; 
        yyaxis left
        ylabel('in kW');
        
        prod=res.Qd_HT_CHP_T +res.Qd_HT_HTR_T;
        cons=1.15 * I.QDemand_HT_t + res.Qd_HT_WT_T;
        
        max1 = max(prod);
        max2 = max(cons);
        max3 = [max1 max2];
        
         ylim([-ceil(max(max3)) ceil(max(max3))]);
        %ylim ([-200 200]);
         stairs(res.Qd_NT_WT_T,'r-');
          stairs(res.Qd_NT_TESS_V_T,'g-');
          stairs(res.Qd_NT_SH_s_T,'c-');
         stairs(-res.P_THP_T,'b-');
         stairs(-I.QDemand_NT_t,'y-');
         stairs(res.Qd_NT_SH_Curt_T,'m-');
         stairs(res.Qd_HT_CHP_wt_T,'r:');
         stairs(res.Qd_HT_HTR_wt_T,'r.');
         stairs(res.Qd_HT_TESS_wt_T,'r-.');
         stairs(res.Qd_NT_PenaltyAdd_T,'r-');
         
       % stairs(prod - cons,'g-');
       % stairs(prod);
        %stairs(cons,'b-');
        %stairs(res.Qd_HT_TESS_in_T,'y-');
        %stairs(-res.Qd_HT_TESS_out_T_1,'b-');
        
        yyaxis right
        ylabel('in �C');
        ylim([0 100]);
        plot(res.T_THP_in_T,'k-');
        plot(res.T_NT_delta_WT_out_T,'k:');
        plot(res.T_NT_PenaltyAdd_T,'k.-');
        
        xlim([0 24*60/par.delta_T*par.T_total]);
        set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
        set(gca,'XTickLabel', [0:12:24*par.T_total])
      
        

        %legend('E^{bat}', 'P^{(ch)}', 'P^{(dch)}', 'z^{charge}', 'z^{discharge}', 'P_{curtail} (PV)');
        legend('prodWT','prodTESSkopplung','prodSH','prodTHPe','consDemand','SHcurt','vonCHP','vonHTR','vonTESS','PenaltyAddWT','T THP in','deltaT WT','deltaT Penalty'); 
        
        
           %% Optimierungsparameter
        name_str = 'Optimierungsparameter';
        figure('Position',[20 100 700 700],'Name',name_str,'NumberTitle','off')
        hold off;
        %h(3)=subplot(Nplots,3,12);
        hold all;
        grid on;
        name_str = 'test'; 
       
        yyaxis left
        ylabel('in s');
         ylim([0 ceil(max(res.time))]);
         plot(res.time,'b-');
         %stairs(res.Qd_NT_TESS_V_T,'r-');
         % stairs(prod - cons,'g-');
       % stairs(prod);
        %stairs(cons,'b-');
        %stairs(res.Qd_HT_TESS_in_T,'y-');
        %stairs(-res.Qd_HT_TESS_out_T_1,'b-');
        
        yyaxis right
        ylabel('in %');
        ylim([0 ceil(max(res.mipgap * 100))]);
        plot(res.mipgap * 100 ,'r-');
        %plot(res.T_NT_delta_WT_out_T,'k:');

        xlim([1 par.T_total]);
        set(gca,'XTick', [1:1:par.T_total])
        set(gca,'XTickLabel', [1:1:par.T_total])
      
        %legend('E^{bat}', 'P^{(ch)}', 'P^{(dch)}', 'z^{charge}', 'z^{discharge}', 'P_{curtail} (PV)');
        legend('Optimierungszeit','relative Abweichung','prodSH','prodTHPe','consDemand','SHcurt','vonCHP','vonHTR','T THP in'); 
%         
%            %% WT Check
%         name_str = 'WT Check';
%         figure('Position',[20 100 700 700],'Name',name_str,'NumberTitle','off')
%         hold off;
%         %h(3)=subplot(Nplots,3,12);
%         hold all;
%         grid on;
%         name_str = 'test'; 
%         yyaxis left
%         ylabel('in kW');
%         
% %         prod=res.Qd_HT_CHP_T +res.Qd_HT_HTR_T;
% %         cons=1.15 * I.QDemand_HT_t + res.Qd_HT_WT_T;
%         
%         max1 = max(prod);
%         max2 = max(cons);
%         max3 = [max1 max2];
%         
%          ylim([-ceil(max(res.Qd_HT_WT_T)) ceil(max(res.Qd_HT_WT_T))]);
%         %ylim ([-200 200]);
%          stairs(-res.Qd_HT_WT_T,'r-');
%          stairs(res.Qd_NT_WT_T,'b-');
%          stairs(res.Qd_HT_HTR_wt_T,'c-');
%          stairs(res.Qd_HT_CHP_wt_T,'g-');
%          stairs(res.Qd_HT_TESS_wt_T,'y-');
%          
% %          stairs(res.Qd_NT_TESS_V_T);
% %          stairs(res.Qd_NT_SH_s_T,'y-');
% %          stairs(-res.P_THP_T,'w-');
% %          stairs(-I.QDemand_NT_t);
% %          stairs(res.Qd_NT_SH_Curt_T,'g-');
% %          stairs(res.Qd_HT_CHP_wt_T,'m-');
% %          stairs(res.Qd_HT_HTR_wt_T,'c-');
% %          stairs(res.Qd_HT_TESS_wt_T,'r-');
%          
%        % stairs(prod - cons,'g-');
%        % stairs(prod);
%         %stairs(cons,'b-');
%         %stairs(res.Qd_HT_TESS_in_T,'y-');
%         %stairs(-res.Qd_HT_TESS_out_T_1,'b-');
%         
% %         yyaxis right
% %         ylabel('in �C');
% %         ylim([0 100]);
% %         plot(res.T_THP_in_T,'k-');
% %         plot(res.T_NT_delta_WT_out_T,'k:');
% 
%         xlim([0 24*60/par.delta_T*par.T_total]);
%         set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
%         set(gca,'XTickLabel', [0:12:24*par.T_total])
%       
%         
% 
%         %legend('E^{bat}', 'P^{(ch)}', 'P^{(dch)}', 'z^{charge}', 'z^{discharge}', 'P_{curtail} (PV)');
%         legend('WT HT','WT NT','vonHTR','vonCHP','vonTESS','SHcurt','vonCHP','vonHTR','vonTESS','T THP in'); 
%         

        
% %% Plot f?r NT Speicher - Inhalt und Leistungen
%         minA = min(res.Qd_NT_TESS_out_T);
%         %maxB = max(res.Qd_NT_TESS_PenaltyAdd_T);
%         maxC = max(res.Qd_NT_TESS_in_T);
%         maxD = max(res.Qd_NT_SH_s_T);
%         maxE = max(res.Qd_NT_TESS_V_T);
%         %maxF = max(res.Q_NT_TESS_losses_T);
%         maxG = [maxC-minA maxD+maxE];
%         hold off;
%         name_str = 'Untersuchung NT Speicherstand';
%         figure('Position',[20 100 700 700],'Name',name_str,'NumberTitle','off')
%         hold all;
%         grid on;
%         yyaxis left
%         ylabel('in kW');
%         ylim([-ceil(max(maxG)) ceil(max(maxG))]);
%         stairs(res.Qd_NT_TESS_in_T - res.Qd_NT_TESS_out_T,'b-');
%         %stairs(res.Qd_NT_TESS_PenaltyAdd_T);
%         %stairs(res.Qd_NT_TESS_in_T);
%         stairs(res.Qd_NT_SH_s_T + res.Qd_NT_TESS_V_T,'g-');
%         %stairs(res.Qd_NT_TESS_V_T);
%         %stairs(-res.Q_NT_TESS_losses_T);
%         
%         yyaxis right
%         ylabel('in kWh');
%         ylim([-ceil(max(res.Q_NT_TESS_losses_T)) par.NT.TESS.Q_max]);
%         plot(res.Q_NT_TESS_T_1,'r-');
%         plot(-res.Q_NT_TESS_losses_T,'m-');
%         
%         xlim([0 24*60/par.delta_T*par.T_total]);
%         set(gca,'XTick', [0:12*60/par.delta_T:24*par.T_total*60/par.delta_T])
%         set(gca,'XTickLabel', [0:12:24*par.T_total])
%       
%         
% 
%         %legend('E^{bat}', 'P^{(ch)}', 'P^{(dch)}', 'z^{charge}', 'z^{discharge}', 'P_{curtail} (PV)');
%         legend('P^{TESS}_{Kreis}','P^{TESS}_{Zu}','E^{TESS}','E^{TESS}_{Verlust}'); 

end