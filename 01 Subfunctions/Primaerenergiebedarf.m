function [Primaerenergiefaktor] = Primaerenergiebedarf(Sim)

res=Sim.res;
KPI=Sim.KPI;
par=Sim.par;
I=Sim.I;

f_PV = 0;
f_CHP_H = 1;
f_CHP_E = 2.4;
f_EGrid = 2.4;
f_HTR = 1.3;
f_Kopplung = 0;

%% Berechnung Endenergieverbrauch

End.E.PV = KPI.E_PVprod - KPI.E_PVcurt;
End.E.CHP = KPI.E_CHP;
End.E.Grid = KPI.E_Gridpur;

End.HT.CHP = KPI.H_CHP;
End.HT.HTR = KPI.H_HTR;

End.NT.SH = KPI.H_SHprod - KPI.H_SHcurt;

End.ges = End.NT.SH + End.HT.HTR + End.HT.CHP + End.E.Grid + End.E.CHP + End.E.PV;
%% Berechnung Primärenergiebedarf

Prim.E.PV = End.E.PV * f_PV;
Prim.E.CHP = End.E.CHP * f_CHP_E;
Prim.E.Grid = End.E.Grid * f_EGrid;

Prim.HT.CHP = End.HT.CHP * f_CHP_H;
Prim.HT.HTR = End.HT.HTR * f_HTR;

Prim.NT.SH = End.NT.SH * f_PV;

Prim.ges = Prim.NT.SH + Prim.HT.HTR + Prim.HT.CHP + Prim.E.Grid + Prim.E.CHP + Prim.E.PV;

Primaerenergiefaktor = Prim.ges / End.ges;

end


% Prim.E.PV = f_PV * KPI.E_PV2dem;
% Prim.E.CHP = f_CHP * KPI.E_CHP2dem;
% Prim.E.BESS_PV = f_PV * KPI.E_ESSdcrg_PV2dem;
% Prim.E.BESS_CHP = f_CHP * KPI.E_ESSdcrg_CHP2dem;
% Prim.E.Grid = f_EGrid * KPI.E_Grid2dem;
% 
% Prim.E.ges = Prim.E.PV + Prim.E.CHP + Prim.E.BESS_PV + Prim.E.BESS_CHP + Prim.E.Grid;
% 
% Prim.HT.CHP = f_CHP * KPI.H_CHP2dem;
% Prim.HT.HTR = f_HTR * KPI.H_HTR2dem;
% Prim.HT.TESS = ((KPI.H_CHP2TESS * f_CHP + KPI.H_HTR2TESS * f_HTR)/(KPI.H_CHP2TESS + KPI.H_HTR2TESS)) * KPI.H_HT_TESSdcrg2dem;
% 
% Prim.HT.ges = Prim.HT.CHP + Prim.HT.HTR + Prim.HT.TESS;
% 
% Prim.NT.SH = f_PV * (KPI.H_SHprod - KPI.H_SHcurt);
% Prim.NT.V = f_Kopplung * (KPI.H_TESS_loss_coupling - KPI.H_TESS_loss_coupling_curt);
