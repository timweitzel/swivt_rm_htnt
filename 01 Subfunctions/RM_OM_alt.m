function [res start x_var cplex opt]=RM_OM(par, I, opt)
% v11b without with power directions
%% Define variables

x_var.subdim={};
x_var.type=[];
x_var.dim.vect=[];
x_var.dim.vect_start=[];
x_var.dim.vect_end=[];
x_var.dim.total=0;
x_var.names=[];
x_var.ub=[];
x_var.lb=[];
big_M=1000000;

x_var   =   createVariable(x_var, 'C_Grid_d',       par.T,      {'1' 'T' '' '' ''},      'C',    - big_M,  big_M);
x_var   =   createVariable(x_var, 'C_Grid_s_PV',       par.T,      {'1' 'T' '' '' ''},      'C',    - big_M,  big_M);
x_var   =   createVariable(x_var, 'C_Grid_s_CHP',       par.T,      {'1' 'T' '' '' ''},      'C',    - big_M,  big_M);
x_var   =   createVariable(x_var, 'C_Cust_PV',         par.T,      {'1' 'T' '' '' ''},      'C',    - big_M,  big_M);
x_var   =   createVariable(x_var, 'C_Cust_CHP',         par.T,      {'1' 'T' '' '' ''},      'C',    - big_M,  big_M);
x_var   =   createVariable(x_var, 'C_Custheat_HT',     par.T,      {'1' 'T' '' '' ''},      'C',    - big_M,  big_M);
x_var   =   createVariable(x_var, 'C_Custheat_NT',     par.T,      {'1' 'T' '' '' ''},      'C',    - big_M,  big_M);
x_var   =   createVariable(x_var, 'C_THP',         par.T,      {'1' 'T' '' '' ''},      'C',    - big_M,  big_M);
% x_var   =   createVariable(x_var, 'C_BESS_in',         par.T,      {'1' 'T' '' '' ''},      'C',    - big_M,  big_M);
x_var   =   createVariable(x_var, 'C_Help',         par.T,      {'1' 'T' '' '' ''},      'C',    - big_M,  big_M);
x_var   =   createVariable(x_var, 'C_run_CHP',      par.T,      {'1' 'T' '' '' ''},      'C',    - big_M,  big_M);
x_var   =   createVariable(x_var, 'C_sup_CHP',      par.T,      {'1' 'T' '' '' ''},      'C',    - big_M,  big_M);
x_var   =   createVariable(x_var, 'C_HTR',          par.T,      {'1' 'T' '' '' ''},      'C',    - big_M,  big_M);
x_var   =   createVariable(x_var, 'C_LCC',          par.T,      {'1' 'T' '' '' ''},      'C',    - big_M,  big_M);
x_var   =   createVariable(x_var, 'C_CYC',          1,          {'0' '' '' '' ''},       'C',    - big_M,  big_M);
x_var   =   createVariable(x_var, 'C_CAC',          par.T,      {'1' 'T' '' '' ''},      'C',    - big_M,  big_M);

x_var   =   createVariable(x_var, 'S_run_CHP',      par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_off_CHP',      par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_sup_CHP',      par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'Qd_HT_CHP',         par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'Qd_HT_CHP_s',         par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'Qd_HT_CHP_wt',         par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'Qd_HT_CHP_d',         par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'Qd_HT_CHP_h',         par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'P_CHP',          par.T,      {'1' 'T' '' '' ''},      'C',    0,  par.CHP.P_max);
x_var   =   createVariable(x_var, 'P_CHP_d',          par.T,      {'1' 'T' '' '' ''},      'C',    0,  par.CHP.P_max);
x_var   =   createVariable(x_var, 'P_CHP_s',          par.T,      {'1' 'T' '' '' ''},      'C',    0,  par.CHP.P_max);
x_var   =   createVariable(x_var, 'P_CHP_t',          par.T,      {'1' 'T' '' '' ''},      'C',    0,  par.CHP.P_max);
x_var   =   createVariable(x_var, 'P_CHP_g',          par.T,      {'1' 'T' '' '' ''},      'C',    0,  par.CHP.P_max);
x_var   =   createVariable(x_var, 'P_CHP_h',          par.T,      {'1' 'T' '' '' ''},      'C',    0,  par.CHP.P_max);

x_var   =   createVariable(x_var, 'P_THP',          par.T,      {'1' 'T' '' '' ''},      'C',    0,  par.THP.P_max);

x_var   =   createVariable(x_var, 'P_help',          par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);

x_var   =   createVariable(x_var, 'Qd_HT_HTR',         par.T,      {'1' 'T' '' '' ''},      'C',    0,  par.HTR.Q_dot_max_burner);
x_var   =   createVariable(x_var, 'Qd_HT_HTR_s',         par.T,      {'1' 'T' '' '' ''},      'C',    0,  par.HTR.Q_dot_max_burner);
x_var   =   createVariable(x_var, 'Qd_HT_HTR_wt',         par.T,      {'1' 'T' '' '' ''},      'C',    0,  par.HTR.Q_dot_max_burner);
x_var   =   createVariable(x_var, 'Qd_HT_HTR_d',         par.T,      {'1' 'T' '' '' ''},      'C',    0,  par.HTR.Q_dot_max_burner);
x_var   =   createVariable(x_var, 'Qd_HT_HTR_h',         par.T,      {'1' 'T' '' '' ''},      'C',    0,  par.HTR.Q_dot_max_burner);
x_var   =   createVariable(x_var, 'Qd_THP',         par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'Qd_THP_in',         par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'Qd_HT_TESS_in',        par.T,      {'1' 'T' '' '' ''},      'C',    0,  par.TESS.Qdot_max);
x_var   =   createVariable(x_var, 'Qd_HT_TESS_out',        par.T_1,      {'1' 'T_1' '' '' ''},      'C',    0,  par.TESS.Qdot_max);
x_var   =   createVariable(x_var, 'Qd_HT_TESS_wt',        par.T,      {'1' 'T' '' '' ''},      'C',    -par.TESS.Qdot_max,  par.TESS.Qdot_max);
x_var   =   createVariable(x_var, 'Qd_HT_TESS_d',        par.T,      {'1' 'T' '' '' ''},      'C',    -par.TESS.Qdot_max,  par.TESS.Qdot_max);
x_var   =   createVariable(x_var, 'Qd_HT_TESS_h',        par.T,      {'1' 'T' '' '' ''},      'C',    -par.TESS.Qdot_max,  par.TESS.Qdot_max);
x_var   =   createVariable(x_var, 'Q_HT_TESS',         par.T_1,    {'1' 'T_1' '' '' ''},   'C',    0,  par.TESS.Q_max);
%x_var   =   createVariable(x_var, 'Qd_delta',       par.T,      {'1' 'T' '' '' ''},      'C',    0, big_M); %Variable wurde entfernt
x_var   =   createVariable(x_var, 'Qd_HT_TESS_V',    par.T_1,      {'1' 'T_1' '' '' ''},         'C',    0,  big_M);

x_var   =   createVariable(x_var, 'Qd_NT_WT',         par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'Qd_HT_WT',         par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);

%NT Speicher hat selbe Grenzen wie HT Speicher
x_var   =   createVariable(x_var, 'Qd_NT_TESS_out',        par.T_1,      {'1' 'T_1' '' '' ''},      'C',    0,  par.TESS.Qdot_max);
x_var   =   createVariable(x_var, 'Qd_NT_TESS_in',        par.T,      {'1' 'T' '' '' ''},      'C',    0,  par.TESS.Qdot_max);
x_var   =   createVariable(x_var, 'Q_NT_TESS',         par.T_1,    {'1' 'T_1' '' '' ''},   'C',    0,  par.TESS.Q_max);
x_var   =   createVariable(x_var, 'T_NT_TESS',       par.T,      {'1' 'T' '' '' ''},      'C',      0,  1); %Temperatur NT Speicher einfach als relativer F�llstand
x_var   =   createVariable(x_var, 'Qd_NT_TESS_V',    par.T,      {'1' 'T' '' '' ''},         'C',    0,  big_M);

x_var   =   createVariable(x_var, 'Qd_SH_s',    par.T,      {'1' 'T' '' '' ''},         'C',    0,  big_M);
x_var   =   createVariable(x_var, 'Qd_Curt_SH',    par.T,      {'1' 'T' '' '' ''},         'C',    0,  big_M);

x_var   =   createVariable(x_var, 'P_ESScrg',       par.T,      {'1' 'T' '' '' ''},        'C',    0,  par.ESS.P_c_max);
x_var   =   createVariable(x_var, 'P_ESSdcrg',      par.T,      {'1' 'T' '' '' ''},        'C',    0,  par.ESS.P_d_max);
x_var   =   createVariable(x_var, 'P_ESSdcrg_PV',      par.T,      {'1' 'T' '' '' ''},        'C',    0,  par.ESS.P_d_max);
x_var   =   createVariable(x_var, 'P_ESSdcrg_CHP',      par.T,      {'1' 'T' '' '' ''},        'C',    0,  par.ESS.P_d_max);
x_var   =   createVariable(x_var, 'P_ESSdcrg_PV_d',      par.T,      {'1' 'T' '' '' ''},        'C',    0,  par.ESS.P_d_max);
x_var   =   createVariable(x_var, 'P_ESSdcrg_PV_h',      par.T,      {'1' 'T' '' '' ''},        'C',    0,  par.ESS.P_d_max);
x_var   =   createVariable(x_var, 'P_ESSdcrg_PV_t',      par.T,      {'1' 'T' '' '' ''},        'C',    0,  par.ESS.P_d_max);
x_var   =   createVariable(x_var, 'P_ESSdcrg_PV_g',      par.T,      {'1' 'T' '' '' ''},        'C',    0,  par.ESS.P_d_max); 
x_var   =   createVariable(x_var, 'P_ESSdcrg_CHP_d',      par.T,      {'1' 'T' '' '' ''},        'C',    0,  par.ESS.P_d_max);
x_var   =   createVariable(x_var, 'P_ESSdcrg_CHP_t',      par.T,      {'1' 'T' '' '' ''},        'C',    0,  par.ESS.P_d_max);
x_var   =   createVariable(x_var, 'P_ESSdcrg_CHP_g',      par.T,      {'1' 'T' '' '' ''},        'C',    0,  par.ESS.P_d_max);
x_var   =   createVariable(x_var, 'P_ESSdcrg_CHP_h',      par.T,      {'1' 'T' '' '' ''},        'C',    0,  par.ESS.P_d_max);
x_var   =   createVariable(x_var, 'P_ESSdcrg_g',      par.T,      {'1' 'T' '' '' ''},        'C',    0,  par.ESS.P_d_max); 
x_var   =   createVariable(x_var, 'S_ESScrg',       par.T,      {'1' 'T' '' '' ''},        'B',    0,  1);
x_var   =   createVariable(x_var, 'S_ESSdcrg',      par.T,      {'1' 'T' '' '' ''},        'B',    0,  1);
x_var   =   createVariable(x_var, 'S_ESSidle',      par.T,      {'1' 'T' '' '' ''},        'B',    0,  1);
x_var   =   createVariable(x_var, 'E_ESSstore',     par.T_1,    {'1' 'T_1' '' '' ''},      'C',    par.ESS.E_min,  par.ESS.E_max);
x_var   =   createVariable(x_var, 'E_ESSstore_PV',     par.T_1,    {'1' 'T_1' '' '' ''},      'C',    par.ESS.E_min,  par.ESS.E_max);
x_var   =   createVariable(x_var, 'E_ESSstore_CHP',     par.T_1,    {'1' 'T_1' '' '' ''},      'C',    par.ESS.E_min,  par.ESS.E_max);
x_var   =   createVariable(x_var, 'E_ESSlosses',    par.T,      {'1' 'T' '' '' ''},        'C',    0,  big_M);

x_var   =   createVariable(x_var, 'D_DOD',          par.T_C,              {'1' 'T_C' '' '' ''},         'C',   -1,  1);
x_var   =   createVariable(x_var, 'D_DODabs',       par.T_C,              {'1' 'T_C' '' '' ''},         'C',    0,  1);

x_var   =   createVariable(x_var, 'S_SOCMIN',       par.T_C,              {'1' 'T_C' '' '' ''},         'B',    0,  1);
x_var   =   createVariable(x_var, 'S_SOCMAX',       par.T_C,              {'1' 'T_C' '' '' ''},         'B',    0,  1);
x_var   =   createVariable(x_var, 'D_SOCMIN',       1,                      {'0' '' '' '' ''},         'C',    0,  1);
x_var   =   createVariable(x_var, 'D_SOCMAX',       1,                      {'0' '' '' '' ''},         'C',    0,  1);
x_var   =   createVariable(x_var, 'D_DODper',       1,                      {'0' '' '' '' ''},         'C',    0,  1);
x_var   =   createVariable(x_var, 'D_DODabssum',    1,                      {'0' '' '' '' ''},         'C',    0,  big_M);

x_var   =   createVariable(x_var, 'S_SOCapprox',    par.T * par.SOCApprox,                       {'2' 'SOCApprox' 'T' '' ''},         'C',    0,  1);
x_var   =   createVariable(x_var, 'S_DODapprox',    par.DODApprox,                               {'1' 'DODApprox' '' '' ''},          'B',    0,  1);
x_var   =   createVariable(x_var, 'S_AhDODapprox',  par.AhDODApprox * par.DODApprox,             {'2' 'AhDODApprox' 'DODApprox' '' ''},       'C',    0,  1);
x_var   =   createVariable(x_var, 'S_CHPP_approx',     par.T * par.n_CHPmodes,                  {'2' 'n_CHPmodes' 'T' '' ''},       'C',    0,  1);

x_var   =   createVariable(x_var, 'P_Grid_d',       par.T,            {'1' 'T' '' '' ''},        'C',    0, par.Grid.P_d_max ); %-big_M * opt.INCL_Pgrid_negVariable darf negativ werden falls R?ckspeisung zul?ssig
x_var   =   createVariable(x_var, 'P_Grid_d_d',       par.T,            {'1' 'T' '' '' ''},        'C',    0, par.Grid.P_d_max ); %-big_M * opt.INCL_Pgrid_negVariable darf negativ werden falls R?ckspeisung zul?ssig
%x_var   =   createVariable(x_var, 'P_Grid_d_s',       par.T,            {'1' 'T' '' '' ''},        'C',    0, par.Grid.P_d_max ); %-big_M * opt.INCL_Pgrid_negVariable darf negativ werden falls R?ckspeisung zul?ssig
x_var   =   createVariable(x_var, 'P_Grid_d_t',       par.T,            {'1' 'T' '' '' ''},        'C',    0, par.Grid.P_d_max );
x_var   =   createVariable(x_var, 'P_Grid_d_h',       par.T,            {'1' 'T' '' '' ''},        'C',    0, par.Grid.P_d_max ); %-big_M * opt.INCL_Pgrid_negVariable darf negativ werden falls R?ckspeisung zul?ssig
x_var   =   createVariable(x_var, 'P_Grid_s',       par.T,            {'1' 'T' '' '' ''},        'C',    0, par.Grid.P_s_max ); %-big_M * opt.INCL_Pgrid_negVariable darf negativ werden falls R?ckspeisung zul?ssig
x_var   =   createVariable(x_var, 'S_Grid',         par.T,            {'1' 'T' '' '' ''},        'B',    0, 1); %Selector variable

x_var   =   createVariable(x_var, 'P_Curt_PV',      par.T,            {'1' 'T' '' '' ''},        'C',    0,  big_M);
x_var   =   createVariable(x_var, 'P_PV_g',      par.T,            {'1' 'T' '' '' ''},        'C',    0,  big_M);
x_var   =   createVariable(x_var, 'P_PV_d',      par.T,            {'1' 'T' '' '' ''},        'C',    0,  big_M);
x_var   =   createVariable(x_var, 'P_PV_s',      par.T,            {'1' 'T' '' '' ''},        'C',    0,  big_M);
x_var   =   createVariable(x_var, 'P_PV_t',      par.T,            {'1' 'T' '' '' ''},        'C',    0,  big_M);
x_var   =   createVariable(x_var, 'P_PV_h',      par.T,            {'1' 'T' '' '' ''},        'C',    0,  big_M);

%% objective Function OF1

[f_sub]=createMatrix( 1 , x_var);
for t=1:par.T
    f_sub.C_Grid_d(1, t)   = 1;
    f_sub.C_Grid_s_PV(1, t)   = 1;
    f_sub.C_Grid_s_CHP(1, t)   = 1;
    f_sub.C_Cust_PV(1, t)     = 1;
    f_sub.C_Cust_CHP(1, t)     = 1;
    f_sub.C_THP(1, t)     = 1;
    f_sub.C_Help(1, t)     = 1;
% %     f_sub.C_BESS_in(1, t)     = 1;
    f_sub.C_Custheat_HT(1, t)     = 1;
    f_sub.C_Custheat_NT(1, t)     = 1;
    %f_sub.C_run_CHP(1, t)  = 1;
    for al=2:par.n_CHPmodes
        f_sub.S_CHPP_approx(1, (al-1) * par.T + t)     = par.CHP.c_fuel * 1 / par.CHP.eta(al) * par.delta_T/60 * par.CHP.alpha(al) * par.CHP.P * par.CHPmodes(al);
    end
    f_sub.C_sup_CHP(1, t)  = 1;
    f_sub.C_HTR(1, t)      = 1;
    
    if opt.INCL_LCCinf==0 && opt.INCL_CACinf==1
        %f_sub.C_CAC(1, t)      = 1;
        for sap=1:par.SOCApprox
            f_sub.S_SOCapprox(1, (sap-1) * par.T + t) = par.ESS.LCC.cal_SOC(sap) * par.ESS.E_inv;
        end
    elseif opt.INCL_LCCinf==1 && opt.INCL_CACinf==1
        %f_sub.C_CAC(1, t)      = 1;
        for sap=1:par.SOCApprox
            f_sub.S_SOCapprox(1, (sap-1) * par.T + t) = par.ESS.LCC.cal_SOC(sap) * par.ESS.E_inv;
        end
    end
    
end
if      opt.INCL_LCCinf==1 && opt.INCL_CACinf==0 %Selector-ansonsten ohne LCC Calculations
    f_sub.C_CYC(1, 1)      = 1;
elseif opt.INCL_LCCinf==1 && opt.INCL_CACinf==1
    f_sub.C_CYC(1, 1)      = 1;
end

f = concatenateMatrix(x_var,f_sub);
clearvars f_sub

%% AOF SubOF1
[A_eq_AOF_sub, b_eq_AOF]=createMatrix(par.T*14 + 1 , x_var);
i=1;

% Netzdemand (3 lila Pfeile)
for t=1:par.T
    A_eq_AOF_sub.P_Grid_d_d(i, t)    = (par.Price.EEG_umlage + par.Price.Stromsteuer + par.Price.Grid_ges) * par.delta_T/60/1000;
    A_eq_AOF_sub.P_Grid_d_t(i, t)    = (I.pd_t(t) + par.Price.EEG_umlage + par.Price.Stromsteuer + par.Price.Grid_ges) * par.delta_T/60/1000;
    A_eq_AOF_sub.P_Grid_d_h(i, t)    = (I.pd_t(t) + par.Price.EEG_umlage + par.Price.Stromsteuer + par.Price.Grid_ges) * par.delta_T/60/1000;
    A_eq_AOF_sub.C_Grid_d(i, t)    = -1; 
    i=i+1;
end    

% PV und PV Speicher an Netz
for t=1:par.T
    A_eq_AOF_sub.P_PV_g(i, t)    = -(par.Price.Grid_PV) * par.delta_T/60/1000;
    A_eq_AOF_sub.P_ESSdcrg_PV_g(i, t)    = -(par.Price.Grid_PV) * par.delta_T/60/1000;
    A_eq_AOF_sub.C_Grid_s_PV(i, t)    = -1; 
    i=i+1;
end    
    
% CHP und CHP Speicher an Netz
for t=1:par.T
    A_eq_AOF_sub.P_CHP_g(i, t)    = -(par.Price.Grid_CHP) * par.delta_T/60/1000;
    A_eq_AOF_sub.P_ESSdcrg_CHP_g(i, t)    = -(par.Price.Grid_CHP) * par.delta_T/60/1000;
    A_eq_AOF_sub.C_Grid_s_CHP(i, t)    = -1; 
    i=i+1;
end    

% PV und PV Speicher an Kunde
for t=1:par.T
    A_eq_AOF_sub.P_PV_d(i, t)    = -(par.Price.Kunde_PV - par.Price.EEG_umlage) * par.delta_T/60/1000;
    A_eq_AOF_sub.P_ESSdcrg_PV_d(i, t)    = -(par.Price.Kunde_PV - par.Price.EEG_umlage) * par.delta_T/60/1000;
    A_eq_AOF_sub.C_Cust_PV(i, t)    = -1; 
    i=i+1;
end    

% CHP und CHP Speicher an Kunde
for t=1:par.T
    A_eq_AOF_sub.P_CHP_d(i, t)    = -(par.Price.Kunde_CHP - par.Price.EEG_umlage) * par.delta_T/60/1000;
    A_eq_AOF_sub.P_ESSdcrg_CHP_d(i, t)    = -(par.Price.Kunde_CHP - par.Price.EEG_umlage) * par.delta_T/60/1000;
    A_eq_AOF_sub.C_Cust_CHP(i, t)    = -1; 
    i=i+1;
end    

% THP aus EE versorgt
for t=1:par.T
    A_eq_AOF_sub.P_PV_t(i, t)    = (0.4 * par.Price.EEG_umlage) * par.delta_T/60/1000;
    A_eq_AOF_sub.P_CHP_t(i, t)    = (0.4 * par.Price.EEG_umlage) * par.delta_T/60/1000;
    A_eq_AOF_sub.P_ESSdcrg_PV_t(i, t)    = (0.4 * par.Price.EEG_umlage) * par.delta_T/60/1000;
    A_eq_AOF_sub.P_ESSdcrg_CHP_t(i, t)    = (0.4 * par.Price.EEG_umlage) * par.delta_T/60/1000;
    A_eq_AOF_sub.C_THP(i, t)    = -1; 
    i=i+1;
end    

% Hilfsenergie aus EE versorgt
for t=1:par.T
    A_eq_AOF_sub.P_PV_h(i, t)    = (0.4 * par.Price.EEG_umlage) * par.delta_T/60/1000;
    A_eq_AOF_sub.P_CHP_h(i, t)    = (0.4 * par.Price.EEG_umlage) * par.delta_T/60/1000;
    A_eq_AOF_sub.P_ESSdcrg_PV_h(i, t)    = (0.4 * par.Price.EEG_umlage) * par.delta_T/60/1000;
    A_eq_AOF_sub.P_ESSdcrg_CHP_h(i, t)    = (0.4 * par.Price.EEG_umlage) * par.delta_T/60/1000;
    A_eq_AOF_sub.C_Help(i, t)    = -1; 
    i=i+1;
end    

% % erstmal keine Kosten beim Einspeichern, muss noch gel�st werden mit EEG
% % f�r mehr als 500 kWh pro kWh Kapazit�t
% for t=1:par.T
%     A_eq_AOF_sub.C_BESS_in(i, t)    = 1; 
%     i=i+1;
% end    

for t=1:par.T
    A_eq_AOF_sub.C_Custheat_HT(i, t)    = 1;
    b_eq_AOF(i, 1)    = -par.Price.customer_heat*I.QDemand_HT_t(t)* par.delta_T/60/1000; 
    i=i+1;
end

%braucht noch eigenen Preis 
for t=1:par.T
    A_eq_AOF_sub.C_Custheat_NT(i, t)    = 1;
    b_eq_AOF(i, 1)    = -par.Price.customer_heat*I.QDemand_NT_t(t)* par.delta_T/60/1000; 
    i=i+1;
end

for t=1:par.T
    %A_eq_AOF_sub.Qd_HT_CHP(i, t)   = par.CHP.c_fuel * 1 / par.CHP.eta * par.delta_T/60;
    for al=2:par.n_CHPmodes
        A_eq_AOF_sub.S_CHPP_approx(i, (al-1) * par.T + t)     = par.CHP.c_fuel * 1 / par.CHP.eta(al) * par.delta_T/60 * par.CHP.alpha(al) * par.CHP.P * par.CHPmodes(al);
    end
    A_eq_AOF_sub.C_run_CHP(i, t)   = -1;
    i=i+1;
end

for t=1:par.T
    A_eq_AOF_sub.S_sup_CHP(i, t)   = par.CHP.c_startup ;
    A_eq_AOF_sub.C_sup_CHP(i, t)   = -1;
    i=i+1;
end

for t=1:par.T
    A_eq_AOF_sub.Qd_HT_HTR(i, t)      = par.HTR.c_fuel * 1 /par.HTR.eta * par.delta_T/60;
    A_eq_AOF_sub.C_HTR(i, t)       = -1;
    i=i+1;
end

for t=1:par.T
    %if opt.INCL_CACinf==1%Selector-ansonsten ohne CAC Calculations
    for sap=1:par.SOCApprox
        A_eq_AOF_sub.S_SOCapprox(i, (sap-1) * par.T + t) = par.ESS.LCC.cal_SOC(sap) * par.ESS.E_inv;
    end
    %end
    A_eq_AOF_sub.C_CAC(i, t)       = -1;
    i=i+1;
end

%if opt.INCL_LCCinf==1 %Selector-ansonsten ohne LCC Calculations
for dap=1:par.DODApprox
    for ahdap=1:par.AhDODApprox
        A_eq_AOF_sub.S_AhDODapprox(i, (ahdap-1)* par.DODApprox + dap) = par.ESS.LCC.cyc_DOD(dap,ahdap) * par.ESS.E_inv;
    end
end
%end
A_eq_AOF_sub.C_CYC(i, 1)       = -1;

A_eq_AOF=concatenateMatrix(x_var,A_eq_AOF_sub);
clearvars A_eq_AOF_sub

%% OF2 eqsilon constraint autarchy

days=par.T_O;
[A_ineq_OF2_sub, b_ineq_OF2]=createMatrix(2*days, x_var);

t_start=1;
t_end=par.T_C;
i=1;
for d=1:days
    for t=t_start:t_end
        A_ineq_OF2_sub.P_Grid_d(i,t)   = -1;
        if I.Pres_t(t)>0
            b_ineq_OF2(i,1)   = b_ineq_OF2(i,1) + (par.eps_const.vector(I.eps_const.n+1)-1) * I.Pres_t(t) ;
        end
    end
    i=i+1;
    
    for t=t_start:t_end
        A_ineq_OF2_sub.P_Grid_d(i,t)   = 1;
        if I.Pres_t(t)>0
            b_ineq_OF2(i,1)   = b_ineq_OF2(i,1) + (1-par.eps_const.vector(I.eps_const.n)) * I.Pres_t(t);
        end
    end
    i=i+1;
    t_start = t_start   +par.T_C;
    t_end   = t_end     +par.T_C;
end

A_ineq_OF2=concatenateMatrix(x_var,A_ineq_OF2_sub);
clearvars A_ineq_OF2_sub

%% A2 Energy balance equation
[A_eq_A2_sub, b_eq_A2]=createMatrix(12*par.T , x_var);
i=1;

for t=1:par.T
    A_eq_A2_sub.P_Grid_s(i,t)   = -1;
    A_eq_A2_sub.P_THP(i,t)  = -1;
    A_eq_A2_sub.P_ESScrg(i,t)   = -1;
    A_eq_A2_sub.P_help(i,t)   = -1;
    A_eq_A2_sub.P_CHP(i,t)   = +1;
    A_eq_A2_sub.P_Curt_PV(i,t)   = -1;
    A_eq_A2_sub.P_ESSdcrg(i,t)   = +1;
    A_eq_A2_sub.P_Grid_d(i,t)   = +1;
    b_eq_A2(i,1)   = I.PDemand_t(t) - I.PV_t(t);
    i=i+1;
end
%     A_eq_A2_sub.P_ESScrg(i,t)   = 1;
%     A_eq_A2_sub.P_ESSdcrg(i,t)  = -1;
%     %A_eq_A2_sub.P_CHP(i,t)  = -1;
%     for al=1:par.n_CHPmodes
%         A_eq_A2_sub.S_CHPP_approx(i, (al-1) * par.T + t)     = -par.CHPmodes(al)*par.CHP.P ;
%     end
%     A_eq_A2_sub.P_THP(i,t)  = 1;
%     A_eq_A2_sub.P_Grid_d(i,t)   = -1;
%     A_eq_A2_sub.P_Grid_s(i,t)   = 1;
%     A_eq_A2_sub.P_Curt_PV(i,t)   = 1;
%     b_eq_A2(i,1)   = -I.Pres_t(t);
%     i=i+1;
% end

for t=1:par.T %CHP output equation
    A_eq_A2_sub.P_CHP(i,t)  = -1;
    A_eq_A2_sub.P_CHP_d(i,t)  = 1;
    A_eq_A2_sub.P_CHP_t(i,t)  = 1; 
    A_eq_A2_sub.P_CHP_s(i,t)  = 1;
    A_eq_A2_sub.P_CHP_g(i,t)  = 1;
    A_eq_A2_sub.P_CHP_h(i,t)  = 1;
    i=i+1;
end

for t=1:par.T %PV output equation
    b_eq_A2(i,1)   = I.PV_t(t);
    A_eq_A2_sub.P_PV_d(i,t)  = 1;
    A_eq_A2_sub.P_PV_t(i,t)  = 1; %to THP
    A_eq_A2_sub.P_PV_s(i,t)  = 1;
    A_eq_A2_sub.P_PV_g(i,t)  = 1;
    A_eq_A2_sub.P_PV_h(i,t)  = 1;
    A_eq_A2_sub.P_Curt_PV(i,t)   = 1;
    i=i+1;
end
%39
for t=1:par.T %SH output equation braucht noch Daten
    b_eq_A2(i,1)   = I.PV_t(t);
    A_eq_A2_sub.Qd_SH_s(i,t)  = 1;
    A_eq_A2_sub.Qd_Curt_SH(i,t)  = 1;
    i=i+1;
end    
    
for t=1:par.T %THP equation
    A_eq_A2_sub.P_CHP_t(i,t)        = 1;
    A_eq_A2_sub.P_PV_t(i,t)         = 1;
    A_eq_A2_sub.P_Grid_d_t(i,t)     = 1;
    A_eq_A2_sub.P_ESSdcrg_PV_t(i,t)    = 1;
    A_eq_A2_sub.P_ESSdcrg_CHP_t(i,t)    = 1;
    A_eq_A2_sub.P_THP(i,t)    = -1;
    i=i+1;
end

for t=1:par.T %BESS input (Speicher darf nicht aus Netz geladen werden)
    A_eq_A2_sub.P_CHP_s(i,t)  = 1;
    A_eq_A2_sub.P_PV_s(i,t)  = 1;
    %A_eq_A2_sub.P_Grid_d_s(i,t)  = 1;
    A_eq_A2_sub.P_ESScrg(i,t)   = -1;
    i=i+1;
end

for t=1:par.T %BESS dischrage PV
    A_eq_A2_sub.P_ESSdcrg_PV(i,t)   = 1;
    A_eq_A2_sub.P_ESSdcrg_PV_d(i,t)   = -1;
    A_eq_A2_sub.P_ESSdcrg_PV_t(i,t)   = -1;
    A_eq_A2_sub.P_ESSdcrg_PV_g(i,t)   = -1;
    A_eq_A2_sub.P_ESSdcrg_PV_h(i,t)   = -1;
    i=i+1;
end

for t=1:par.T %BESS dischrage CHP
    A_eq_A2_sub.P_ESSdcrg_CHP(i,t)   = 1;
    A_eq_A2_sub.P_ESSdcrg_CHP_d(i,t)   = -1;
    A_eq_A2_sub.P_ESSdcrg_CHP_t(i,t)   = -1;
    A_eq_A2_sub.P_ESSdcrg_CHP_g(i,t)   = -1;
    A_eq_A2_sub.P_ESSdcrg_CHP_h(i,t)   = -1;
    i=i+1;
end

for t=1:par.T %BESS dischrage Zerlegung
    A_eq_A2_sub.P_ESSdcrg(i,t)   = 1;
    A_eq_A2_sub.P_ESSdcrg_PV(i,t)   = -1;
    A_eq_A2_sub.P_ESSdcrg_CHP(i,t)   = -1;
    i=i+1;
end

for t=1:par.T %Demand equatioon / BESS output
    b_eq_A2(i,1)   = I.PDemand_t(t);
    A_eq_A2_sub.P_CHP_d(i,t)  = 1;
    A_eq_A2_sub.P_PV_d(i,t)  = 1;
    A_eq_A2_sub.P_Grid_d_d(i,t)  = 1;
    A_eq_A2_sub.P_ESSdcrg_PV_d(i,t)   = 1;
    A_eq_A2_sub.P_ESSdcrg_CHP_d(i,t)   = 1;
    i=i+1;
end

for t=1:par.T %Grid equation
    A_eq_A2_sub.P_CHP_g(i,t)        = 1;
    A_eq_A2_sub.P_PV_g(i,t)         = 1;
    A_eq_A2_sub.P_ESSdcrg_PV_g(i,t)    = 1;
    A_eq_A2_sub.P_ESSdcrg_CHP_g(i,t)    = 1;
    A_eq_A2_sub.P_Grid_s(i,t)       = -1;
    i=i+1;
end

for t=1:par.T %Grid demand (Speicher darf nicht aus Netz geladen werden)
    A_eq_A2_sub.P_Grid_d(i,t)  = 1;
    A_eq_A2_sub.P_Grid_d_t(i,t)  = -1;
    A_eq_A2_sub.P_Grid_d_d(i,t)  = -1;
    A_eq_A2_sub.P_Grid_d_h(i,t)  = -1;
    %A_eq_A2_sub.P_Grid_d_s(i,t)  = -1;
    i=i+1;
end

A_eq_A2=concatenateMatrix(x_var,A_eq_A2_sub);
clearvars A_eq_A2_sub

%% A0001 Speicheraufteilung/getrennte Bilanzierung

[A_eq_A0001_sub, b_eq_A0001]=createMatrix(3*par.T+4, x_var); 
i=1;

for t=1:par.T
    A_eq_A0001_sub.E_ESSstore( i,t)     = -1;
    A_eq_A0001_sub.E_ESSstore_PV( i,t)     = 1;
    A_eq_A0001_sub.E_ESSstore_CHP( i,t)     = 1;
    i=i+1;
end

for t=1:par.T
    A_eq_A0001_sub.E_ESSstore_PV( i, t + 1)     = -1;
    A_eq_A0001_sub.E_ESSstore_PV( i, t)           = (1-par.ESS.sigma);
    A_eq_A0001_sub.P_PV_s( i, t)            = (par.ESS.eta_c) * par.delta_T/60;
    A_eq_A0001_sub.P_ESSdcrg_PV( i, t)         = -(1/par.ESS.eta_d) * par.delta_T/60;    
    i=i+1;
end

for t=1:par.T
    A_eq_A0001_sub.E_ESSstore_CHP( i, t + 1)     = -1;
    A_eq_A0001_sub.E_ESSstore_CHP( i, t)           = (1-par.ESS.sigma);
    A_eq_A0001_sub.P_CHP_s( i, t)            = (par.ESS.eta_c) * par.delta_T/60;
    A_eq_A0001_sub.P_ESSdcrg_CHP( i, t)         = -(1/par.ESS.eta_d) * par.delta_T/60;    
    i=i+1;
end

% Anfang- und Endbedingung 50:50 auf PV und BHKW aufgeteilt

A_eq_A0001_sub.E_ESSstore_PV(i, 1)        = 1; %Anfangsbedingung PV
b_eq_A0001 (i,1)                       = I.start.E_ESS_T0 * 0.5;
i=i+1;

A_eq_A0001_sub.E_ESSstore_PV(i,par.T + 1) = 1; %Endedingung PV
b_eq_A0001 (i,1)                       = I.end.E_ESS_T0 * 0.5;
i=i+1;

A_eq_A0001_sub.E_ESSstore_CHP(i, 1)        = 1; %Anfangsbedingung CHP
b_eq_A0001 (i,1)                       = I.start.E_ESS_T0 * 0.5;
i=i+1;

A_eq_A0001_sub.E_ESSstore_CHP(i,par.T + 1) = 1; %Endedingung CHP
b_eq_A0001 (i,1)                       = I.end.E_ESS_T0 * 0.5;
i=i+1;

A_eq_A0001=concatenateMatrix(x_var,A_eq_A0001_sub);
clearvars A_eq_A0001_sub

%% A3 Energy Storage equation
[A_eq_A3_sub, b_eq_A3]=createMatrix(2*par.T+2, x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
i=1;
for t=1:par.T
    A_eq_A3_sub.E_ESSstore( i, t + 1)     = -1;
    A_eq_A3_sub.E_ESSstore( i, t)           = (1-par.ESS.sigma);
    A_eq_A3_sub.P_ESScrg( i, t)            = (par.ESS.eta_c) * par.delta_T/60;
    A_eq_A3_sub.P_ESSdcrg( i, t)         = -(1/par.ESS.eta_d) * par.delta_T/60;    
    i=i+1;
end

A_eq_A3_sub.E_ESSstore(i, 1)        = 1; %Anfangsbedingung
b_eq_A3 (i,1)                       = I.start.E_ESS_T0;
i=i+1;

A_eq_A3_sub.E_ESSstore(i,par.T + 1) = 1; %Endedingung
b_eq_A3 (i,1)                       = I.end.E_ESS_T0;
i=i+1;

for t=1:par.T
    A_eq_A3_sub.E_ESSlosses( i, t)       = -1;
    A_eq_A3_sub.E_ESSstore( i, t)        = (par.ESS.sigma);
    A_eq_A3_sub.P_ESScrg( i, t)          = (1-par.ESS.eta_c) * par.delta_T/60;
    A_eq_A3_sub.P_ESSdcrg( i, t)         = (1/par.ESS.eta_d-1) * par.delta_T/60;    
    i=i+1;
end

A_eq_A3=concatenateMatrix(x_var,A_eq_A3_sub);
clearvars A_eq_A3_sub

%% A37 A38 Energy Storage constraints
[A_ineq_A37_sub, b_ineq_A37]=createMatrix(6*par.T, x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
i=1;

for t=1:par.T
    A_ineq_A37_sub.P_ESScrg( i, t)     = 1;
    A_ineq_A37_sub.S_ESScrg( i, t)     = -par.ESS.P_c_max;
    i=i+1;
end
for t=1:par.T
    A_ineq_A37_sub.P_ESScrg( i, t)     = -1;
    A_ineq_A37_sub.S_ESScrg( i, t)     = par.ESS.P_c_min;
    i=i+1;
end

for t=1:par.T
    A_ineq_A37_sub.P_ESSdcrg( i, t)        = 1;
    A_ineq_A37_sub.S_ESSdcrg( i, t)        = -par.ESS.P_d_max;
    i=i+1;
end

for t=1:par.T
    A_ineq_A37_sub.P_ESSdcrg( i, t)        = -1;
    A_ineq_A37_sub.S_ESSdcrg( i, t)        = par.ESS.P_d_min;
    i=i+1;
end

for t=1:par.T
    A_ineq_A37_sub.P_ESSdcrg( i, t)        = 1;
    A_ineq_A37_sub.S_ESSidle( i, t)        = par.ESS.P_d_max;
    b_ineq_A37( i, 1)                      = par.ESS.P_d_max;
    i=i+1;
end

for t=1:par.T
    A_ineq_A37_sub.P_ESScrg( i, t)        = 1;
    A_ineq_A37_sub.S_ESSidle( i, t)        = par.ESS.P_c_max;
    b_ineq_A37( i, 1)                      = par.ESS.P_c_max;
    i=i+1;
end

A_ineq_A37=concatenateMatrix(x_var,A_ineq_A37_sub);
clearvars A_ineq_A37_sub


%% A39 Energy Discharge
[A_eq_A39_sub, b_eq_A39]=createMatrix( par.T , x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
i=1;
for t=1:par.T
    A_eq_A39_sub.S_ESScrg( i, t)        = 1;
    A_eq_A39_sub.S_ESSdcrg( i, t)        = 1;
    A_eq_A39_sub.S_ESSidle( i, t)        = 1;
    b_eq_A39(i,1)=1;
    i=i+1;
end
A_eq_A39=concatenateMatrix(x_var,A_eq_A39_sub);
clearvars A_eq_A39_sub

%% A4 Heat balance equation HT, Aufteilung und Zusammensetzung HT Str�me
% HTR max von 300 auf 3000 gesetzt, damit das hier gel�st werden kann
[A_eq_A4_sub, b_eq_A4]=createMatrix(8*par.T, x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
i=1;
for t=1:par.T
    A_eq_A4_sub.Qd_HT_HTR( i, t)           = +1;
    A_eq_A4_sub.Qd_HT_TESS_out( i, t)          = +1;
    A_eq_A4_sub.Qd_HT_TESS_in( i, t)          = -1;
    A_eq_A4_sub.Qd_HT_WT(i, t)             = -1;
    %A_eq_A4_sub.Qd_delta( i, t)         = -1;
    %A_eq_A4_sub.Qd_HT_CHP( i, t)           = +1;
    for al=2:par.n_CHPmodes
        A_eq_A4_sub.S_CHPP_approx(i, (al-1) * par.T + t)     = par.CHP.alpha(al)*par.CHP.P*par.CHPmodes(al);
    end
    b_eq_A4(i,1) = I.QDemand_HT_t(t) + 0.15 * I.QDemand_HT_t(t);
    i=i+1;
end

%Heater Output
for t=1:par.T
    A_eq_A4_sub.Qd_HT_HTR (i, t)        = 1;
    A_eq_A4_sub.Qd_HT_HTR_s (i,t)       = -1;
    A_eq_A4_sub.Qd_HT_HTR_wt (i,t)       = -1;
    A_eq_A4_sub.Qd_HT_HTR_d (i,t)       = -1;
    A_eq_A4_sub.Qd_HT_HTR_h (i,t)       = -1;
    i=i+1;
end

% HT Speicher Output
for t=1:par.T
    A_eq_A4_sub.Qd_HT_TESS_out (i, t)        = 1;
    A_eq_A4_sub.Qd_HT_TESS_wt (i,t)       = -1;
    A_eq_A4_sub.Qd_HT_TESS_d (i,t)       = -1;
    A_eq_A4_sub.Qd_HT_TESS_h (i,t)       = -1;
    i=i+1;
end

% CHP Output, funktioniert das so?
for t=1:par.T
    for al=2:par.n_CHPmodes
        A_eq_A4_sub.S_CHPP_approx(i, (al-1) * par.T + t)     = par.CHP.alpha(al)*par.CHP.P*par.CHPmodes(al);
    end
    A_eq_A4_sub.Qd_HT_CHP_s (i,t)       = -1;
    A_eq_A4_sub.Qd_HT_CHP_wt (i,t)       = -1;
    A_eq_A4_sub.Qd_HT_CHP_d (i,t)       = -1;
    A_eq_A4_sub.Qd_HT_CHP_h (i,t)       = -1;
    i=i+1;
end

% Input WT
for t=1:par.T
    A_eq_A4_sub.Qd_HT_WT (i,t)          = 1;
    A_eq_A4_sub.Qd_HT_CHP_wt (i,t)       = -1;
    A_eq_A4_sub.Qd_HT_TESS_wt (i,t)       = -1;
    A_eq_A4_sub.Qd_HT_HTR_wt (i,t)       = -1;
    i=i+1;
end

% Input Demand
for t=1:par.T
    A_eq_A4_sub.Qd_HT_HTR_d (i,t)       = 1;
    A_eq_A4_sub.Qd_HT_TESS_d (i,t)       = 1;
    A_eq_A4_sub.Qd_HT_CHP_d (i,t)       = 1;
    b_eq_A4(i,1) = I.QDemand_HT_t(t);
    i=i+1;
end

% Input Hilfsenergie (15% gesamter HT Demand)
for t=1:par.T
    A_eq_A4_sub.Qd_HT_HTR_h (i,t)       = 1;
    A_eq_A4_sub.Qd_HT_TESS_h (i,t)       = 1;
    A_eq_A4_sub.Qd_HT_CHP_h (i,t)       = 1;
    b_eq_A4(i,1) = 0.15 * I.QDemand_HT_t(t);
    i=i+1;
end

%HT Speicher Input
for t=1:par.T
    A_eq_A4_sub.Qd_HT_TESS_in (i, t)        = -1;
    A_eq_A4_sub.Qd_HT_CHP_s (i,t)       = 1;
    A_eq_A4_sub.Qd_HT_HTR_s (i,t)       = 1;
    i=i+1;
end

A_eq_A4=concatenateMatrix(x_var,A_eq_A4_sub);
clearvars A_eq_A4_sub

%% A01 Heat balance equation NT 33

[A_eq_A01_sub, b_eq_A01]=createMatrix(par.T, x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
i=1;
for t=1:par.T
    A_eq_A01_sub.Qd_THP( i, t)           = +1;
    b_eq_A01(i,1) = I.QDemand_NT_t(t);
    i=i+1;
end

A_eq_A01=concatenateMatrix(x_var,A_eq_A01_sub);
clearvars A_eq_A01_sub




%% A4a Curtailment Maximum
% [A_ineq_A4a_sub, b_ineq_A4a]=createMatrix(par.T, x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
% i=1;
% for t=1:par.T
%     A_ineq_A4a_sub.Qd_delta( i, t)         = 1;
%     b_ineq_A4a(i,1) = par.CHP.Curt_max;
%     i=i+1;
% end
% 
% A_ineq_A4a=concatenateMatrix(x_var,A_ineq_A4a_sub);
% clearvars A_ineq_A4a_sub

%% A0002 Hilfsenergie

[A_eq_A0002_sub, b_eq_A0002]=createMatrix(2 * par.T, x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
i=1;

for t=1:par.T
    A_eq_A0002_sub.P_help( i, t)           = +1;
    b_eq_A0002(i,1) = 0.04 * I.QDemand_HT_t(t) + 0.04 * I.QDemand_NT_t(t);
    i=i+1;
end

for t=1:par.T
    A_eq_A0002_sub.P_help( i, t)           = -1;
    A_eq_A0002_sub.P_CHP_h(i,t)        = 1;
    A_eq_A0002_sub.P_PV_h(i,t)         = 1;
    A_eq_A0002_sub.P_Grid_d_h(i,t)     = 1;
    A_eq_A0002_sub.P_ESSdcrg_PV_h(i,t)    = 1;
    A_eq_A0002_sub.P_ESSdcrg_CHP_h(i,t)    = 1;
    i=i+1;
end

A_eq_A0002    = concatenateMatrix(x_var,A_eq_A0002_sub);
clearvars A_eq_THP_sub

%% THP1 THP equation

[A_eq_THP_sub, b_eq_THP]=createMatrix(3*par.T, x_var); 
i=1;
%43
for t=1:par.T
    A_eq_THP_sub.Qd_THP( i, t)          = +1;
    A_eq_THP_sub.P_THP( i, t)           = -par.THP.COP;
    i=i+1;
end
%44
for t=1:par.T
    A_eq_THP_sub.Qd_THP_in(i, t)        = 1;
    A_eq_THP_sub.Qd_NT_TESS_out(i, t)       = -1;
    A_eq_THP_sub.Qd_NT_WT (i, t)        = -1;
    i=i+1;
end
%103 eta ist zurzeit =1
for t=1:par.T
    A_eq_THP_sub.Qd_THP(i, t)           = 1;
    A_eq_THP_sub.Qd_THP_in(i, t)        = -par.THP.eta;
    i=i+1;
end
%hier muss noch was f�r abh�ngigkeit von COP von T NT hin, aber ich wei� noch nicht, wie das funktioniert    
    
A_eq_THP    = concatenateMatrix(x_var,A_eq_THP_sub);
clearvars A_eq_THP_sub

%% A5 HT TESS equation

[A_eq_A5_sub, b_eq_A5]=createMatrix(2*par.T+2, x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
i=1;
for t=1:par.T
    A_eq_A5_sub.Q_HT_TESS( i, t+1)          = +1;
    A_eq_A5_sub.Q_HT_TESS( i, t)           = -(1-par.TESS.sigma);
    A_eq_A5_sub.Qd_HT_TESS_out( i, t)          = - (-par.delta_T/60);
    A_eq_A5_sub.Qd_HT_TESS_in( i, t)          = - (par.delta_T/60);
    i=i+1;
end

%101
for t=1:par.T
    A_eq_A5_sub.Qd_HT_TESS_V(i,t+1)     = 1;
    A_eq_A5_sub.Q_HT_TESS(i,t)          = -((1-par.TESS.sigma)/(par.delta_T/60));
    i=i+1;
end

A_eq_A5_sub.Q_HT_TESS(i, 1)        = 1; %Anfangsbedingung
b_eq_A5(i,1)                       = I.start.Q_HT_TESS_T0;
i=i+1;

A_eq_A5_sub.Q_HT_TESS(i,par.T + 1) = 1; %Endedingung
b_eq_A5(i,1)                       = I.end.Q_HT_TESS_T0;
i=i+1;

A_eq_A5=concatenateMatrix(x_var,A_eq_A5_sub);
clearvars A_eq_A5_sub

%% A05 HT TESS unequation (107)

[A_ineq_A05_sub, b_ineq_A05]=createMatrix(par.T, x_var);
i=1;

for t=1:par.T
    A_ineq_A05_sub.Qd_HT_TESS_out( i, t+1)          = (par.delta_T/60);
    A_ineq_A05_sub.Q_HT_TESS(i, 1)        = -1;
    i=i+1;
end

A_ineq_A05=concatenateMatrix(x_var,A_ineq_A05_sub);
clearvars A_ineq_A05_sub    


%% A02 NT TESS equation
% hat noch selbes sigma wie HT

[A_eq_A02_sub, b_eq_A02]=createMatrix(4*par.T+2, x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
i=1;
for t=1:par.T
    A_eq_A02_sub.Q_NT_TESS( i, t+1)          = +1;
    A_eq_A02_sub.Q_NT_TESS( i, t)           = -(1-par.TESS.sigma);
    A_eq_A02_sub.Qd_NT_TESS_out( i, t)          = - (-par.delta_T/60);
    A_eq_A02_sub.Qd_NT_TESS_in( i, t)          = - (par.delta_T/60);
    i=i+1;
end
%23 Das ist jetzt nicht mehr so! weil TESS in jetzt was anderes ist
for t=1:par.T
    A_eq_A02_sub.Qd_NT_TESS_in( i, t)          = 1;
    A_eq_A02_sub.Qd_SH_s(i, t)              = -1;
    A_eq_A02_sub.Qd_NT_TESS_V(i,t)           = -1;
    i=i+1;
end

A_eq_A02_sub.Q_NT_TESS(i, 1)        = 1; %Anfangsbedingung
b_eq_A02 (i,1)                       = I.start.Q_NT_TESS_T0;
i=i+1;

A_eq_A02_sub.Q_NT_TESS(i,par.T + 1) = 1; %Endedingung
b_eq_A02 (i,1)                       = I.end.Q_NT_TESS_T0;
i=i+1;

%105 eta_kopplung ist geraten
for t=1:par.T
    A_eq_A02_sub.Qd_NT_TESS_V(i,t)     = 1;
    A_eq_A02_sub.Qd_HT_TESS_V(i,t)     = - par.TESS.eta_kopplung ;
    i=i+1;
end

%102 hat noch NT Max Speicher Parameter drin
for t=1:par.T
    A_eq_A02_sub.T_NT_TESS(i,1)         = 1;
    A_eq_A02_sub.Q_NT_TESS(i,1)         = - (1/par.TESS.Q_max);
    i=i+1;
end

A_eq_A02=concatenateMatrix(x_var,A_eq_A02_sub);
clearvars A_eq_A02_sub

%% A06 HT TESS unequation (106)

[A_ineq_A06_sub, b_ineq_A06]=createMatrix(par.T, x_var);
i=1;

for t=1:par.T
    A_ineq_A06_sub.Qd_NT_TESS_out( i, t+1)          = (par.delta_T/60);
    A_ineq_A06_sub.Q_NT_TESS(i, 1)        = -1;
    i=i+1;
end

A_ineq_A06=concatenateMatrix(x_var,A_ineq_A06_sub);
clearvars A_ineq_A06_sub    

%% A03 W�rmetauscher

[A_eq_A03_sub, b_eq_A03]=createMatrix(par.T, x_var); 
i=1;

for t=1:par.T
    A_eq_A03_sub.Qd_NT_WT(i, t)         = 1;
    A_eq_A03_sub.Qd_HT_WT(i, t)         = -par.WT.eta;
    i=i+1;
end

A_eq_A03=concatenateMatrix(x_var,A_eq_A03_sub);
clearvars A_eq_A03_sub

%% A6 CHP Q and P equation
[A_eq_A6_sub, b_eq_A6]=createMatrix(2*par.T, x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
i=1;
for t=1:par.T
    A_eq_A6_sub.Qd_HT_CHP( i, t)     = +1;
    %A_eq_A6_sub.P_CHP( i, t)      = - par.CHP.alpha;
    for al=1:par.n_CHPmodes
        A_eq_A6_sub.S_CHPP_approx(i, (al-1) * par.T + t)     = -par.CHP.alpha(al)*par.CHP.P*par.CHPmodes(al);
    end
    i=i+1;
end

for t=1:par.T %A21
    A_eq_A6_sub.S_off_CHP( i, t)      = +1;
    A_eq_A6_sub.S_run_CHP( i, t)      = +1;
    b_eq_A6( i, 1)                    = +1;
    i=i+1;
end



A_eq_A6=concatenateMatrix(x_var,A_eq_A6_sub);
clearvars A_eq_A6_sub

%% CHP2 CHP P equation
% [A_ineq_CHP2_sub, b_ineq_CHP2]=createMatrix(2*par.T, x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
% i=1;
% for t=1:par.T
%     %A_ineq_CHP2_sub.P_CHP( i, t)        = +1;
%     for al=2:par.n_CHPmodes
%         A_ineq_CHP2_sub.S_CHPP_approx(i, (al-1) * par.T + t)     = par.CHPmodes(al)*par.CHP.P ;
%     end
%     A_ineq_CHP2_sub.S_run_CHP( i, t)    = - par.CHP.P_max;
%     i=i+1;
% end
% for t=1:par.T
%     %A_ineq_CHP2_sub.P_CHP( i, t)        = -1;
%     for al=2:par.n_CHPmodes
%         A_ineq_CHP2_sub.S_CHPP_approx(i, (al-1) * par.T + t)     = -par.CHPmodes(al)*par.CHP.P ;
%     end
%     A_ineq_CHP2_sub.S_run_CHP( i, t)    = par.CHP.P_min;
%     i=i+1;
% end
% 
% A_ineq_CHP2=concatenateMatrix(x_var,A_ineq_CHP2_sub);
% clearvars A_ineq_CHP2_sub

%% CHP3 CHP P equation
n2=1;
for n3=1:par.CHP.t_min    
	n2=n2+n3;
end
[A_ineq_CHP3_sub, b_ineq_CHP3]=createMatrix((par.T-par.CHP.t_min)*par.CHP.t_min+n2, x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
i=1;
for t=par.CHP.t_min+1:par.T
    for tau=1:par.CHP.t_min
        A_ineq_CHP3_sub.S_off_CHP( i, t)            = 1;
        A_ineq_CHP3_sub.S_off_CHP( i, t-1)          = -1;
        A_ineq_CHP3_sub.S_run_CHP( i, t-tau)        = -1;
        i=i+1;
    end
end


for t=1:par.CHP.t_min
    if I.start.S_run_CHP_T0==1
        delta_t_run=par.CHP.t_min-I.start.S_run_CHP_T0_num; %noch notwendige Laufzeit bevor Abschaltung erfolgen kann. BHKW ist zu diesem Zeitpunkt noch angeschaltet
    else 
        delta_t_run=0;
    end
    if t>1
        for tau=1:t-1
            A_ineq_CHP3_sub.S_off_CHP( i, t)        = 1;
            A_ineq_CHP3_sub.S_off_CHP( i, t-1)                  = -1;
            if delta_t_run-t>=0
               A_ineq_CHP3_sub.S_run_CHP( i, t-tau)        = -1;
            else
                b_ineq_CHP3(i,1)    =   0;
            end
            i=i+1;
        end
    else
        A_ineq_CHP3_sub.S_off_CHP( i, t)        = 1;
        if delta_t_run-t>=0
           b_ineq_CHP3(i,1)        = 1;
        else
            b_ineq_CHP3(i,1)    =   1;
        end
        i=i+1;
    end
end

A_ineq_CHP3=concatenateMatrix(x_var,A_ineq_CHP3_sub);
clearvars A_ineq_CHP3_sub

%% CHP4 Linearization of alpha constraints

[A_eq_CHP4_sub, b_eq_CHP4]=createMatrix(par.T, x_var);
i=1;
for t=1:par.T 
    for al=1:par.n_CHPmodes
        A_eq_CHP4_sub.S_CHPP_approx(i, (al-1) * par.T + t)     = par.CHPmodes(al)*par.CHP.P ;
    end
    A_eq_CHP4_sub.P_CHP(i,t)                         = -1;
    i=i+1;
    
end

A_eq_CHP4=concatenateMatrix(x_var,A_eq_CHP4_sub);
clearvars A_eq_CHP4_sub

%% CHP5 Linearization of LCC Constraints weight factors

[A_eq_CHP5_sub, b_eq_CHP5]=createMatrix(5*par.T, x_var);
i=1;
for t=1:par.T 
    for al=1:par.n_CHPmodes
        A_eq_CHP5_sub.S_CHPP_approx(i, (al-1) * par.T + t)     = 1;
    end
    b_eq_CHP5(i,1)                         = 1;
    i=i+1;    
end

for t=1:par.T 
    for al=2:par.n_CHPmodes
        A_eq_CHP5_sub.S_CHPP_approx(i, (al-1) * par.T + t)     = 1;
    end
    A_eq_CHP5_sub.S_run_CHP( i, t)        = -1;
    i=i+1; 
end
for t=1:par.T 
    for al=2:par.n_CHPmodes
        A_eq_CHP5_sub.S_CHPP_approx(i, (al-1) * par.T + t)     = 1;
    end
    A_eq_CHP5_sub.S_off_CHP( i, t)        = 1;
    b_eq_CHP5( i, 1)        = 1;
    i=i+1; 
end

for t=1:par.T
    %A_ineq_CHP2_sub.P_CHP( i, t)        = -1;
    al=1;
    A_eq_CHP5_sub.S_CHPP_approx(i, (al-1) * par.T + t)     = 1 ;
    A_eq_CHP5_sub.S_off_CHP( i, t)    = -1;
    i=i+1;
end

for t=1:par.T
    %A_ineq_CHP2_sub.P_CHP( i, t)        = -1;
    al=1;
    A_eq_CHP5_sub.S_CHPP_approx(i, (al-1) * par.T + t)     = +1 ;
    A_eq_CHP5_sub.S_run_CHP( i, t)    = 1;
    b_eq_CHP5( i,1)    = 1;
    i=i+1;
end

A_eq_CHP5=concatenateMatrix(x_var,A_eq_CHP5_sub);
clearvars A_eq_CHP5_sub


%% A20-A22 CHP startup constraints
[A_ineq_A20_sub, b_ineq_A20]=createMatrix( 3*(par.T), x_var); %zus?tzliche Bedingung f?r Anfangs und Endedingungen
i=1;
for t=1:par.T %A20
    A_ineq_A20_sub.S_sup_CHP( i, t)      = +1;
    if t==1       
        b_ineq_A20( i, 1)      = +1-I.start.S_run_CHP_T0;
    else
        A_ineq_A20_sub.S_run_CHP( i, t-1)      = +1;
        b_ineq_A20( i, 1)      = +1;
    end
    i=i+1;
end
for t=1:par.T %A21
    A_ineq_A20_sub.S_sup_CHP( i, t)      = +1;
    A_ineq_A20_sub.S_run_CHP( i, t)      = -1;
    b_ineq_A20( i, 1)      = 0;
    i=i+1;
end
for t=1:par.T %A22
    A_ineq_A20_sub.S_sup_CHP( i, t)      = -1;

    A_ineq_A20_sub.S_run_CHP( i, t)      = +1;
    
    if t==1       
        b_ineq_A20( i, 1)      = +0+I.start.S_run_CHP_T0;
    else
        A_ineq_A20_sub.S_run_CHP( i, t-1)    = -1;
        b_ineq_A20( i, 1)      = 0;
    end
    i=i+1;
end

A_ineq_A20=concatenateMatrix(x_var,A_ineq_A20_sub);
clearvars A_ineq_A20_sub


%% A12 PCC constraints with selector
[A_ineq_A12_sub, b_ineq_A12]=createMatrix(2*par.T , x_var);
i=1;
for t=1:par.T 
    A_ineq_A12_sub.P_Grid_d(i,t)   = 1;
    A_ineq_A12_sub.P_Grid_s(i,t)   = 0;
    A_ineq_A12_sub.S_Grid(i,t)     = -par.Grid.P_d_max;
    b_ineq_A12(i,1) =0;
    i=i+1;
end
for t=1:par.T 
    A_ineq_A12_sub.P_Grid_d(i,t)   = 0;
    A_ineq_A12_sub.P_Grid_s(i,t)   = 1;
    A_ineq_A12_sub.S_Grid(i,t)     = +par.Grid.P_s_max;
    b_ineq_A12(i,1) = par.Grid.P_s_max;
    i=i+1;
end
A_ineq_A12=concatenateMatrix(x_var,A_ineq_A12_sub);
clearvars A_ineq_A12_sub

%% A47 PV Curtail constraint
[A_ineq_A47_sub, b_ineq_A47]=createMatrix(par.T , x_var);
i=1;
for t=1:par.T 
    A_ineq_A47_sub.P_Curt_PV(i,t)   = 1;
    b_ineq_A47(i,1)                 =I.PV_t(t);
    i=i+1;
end

A_ineq_A47=concatenateMatrix(x_var,A_ineq_A47_sub);
clearvars A_ineq_A47_sub

%% A04 SH Curtail constraint 40
%braucht noch Daten
[A_ineq_A04_sub, b_ineq_A04]=createMatrix(par.T , x_var);
i=1;
for t=1:par.T 
    A_ineq_A04_sub.P_Curt_SH(i,t)   = 1;
    b_ineq_A04(i,1)                 =I.PV_t(t);
    i=i+1;
end

A_ineq_A04=concatenateMatrix(x_var,A_ineq_A04_sub);
clearvars A_ineq_A04_sub

%% LCC1 Linearization of LCC
[A_eq_LCC1_sub, b_eq_LCC1]=createMatrix(1 , x_var);
i=1;
    A_eq_LCC1_sub.D_DODabssum(i,1)     =   -1;       
    for ahdap=1:par.AhDODApprox 
        for dap=1:par.DODApprox
            A_eq_LCC1_sub.S_AhDODapprox(i, (ahdap-1)* par.DODApprox + dap)  = par.AhDODvector(ahdap);
        end
    end

A_eq_LCC1=concatenateMatrix(x_var,A_eq_LCC1_sub);
clearvars A_eq_LCC1_sub

%% LCC1a Linearization of LCC
[A_ineq_LCC1a_sub, b_ineq_LCC1a]=createMatrix(  2 * par.DODApprox  , x_var);
i=1;
for dap=1:par.DODApprox
    A_ineq_LCC1a_sub.S_DODapprox(i, dap)     = +big_M;
    A_ineq_LCC1a_sub.D_DODper(i,1)           = 1;
    b_ineq_LCC1a(i,1)                        = +big_M+par.DODvector(dap+1);
    i=i+1;
end
%11.2
for dap=1:par.DODApprox 
    A_ineq_LCC1a_sub.S_DODapprox(i, dap)     = +par.DODvector(dap);
    A_ineq_LCC1a_sub.D_DODper(i,1)           = -1;
    b_ineq_LCC1a(i,1)                        = -0.0001;
    i=i+1; 
end  

A_ineq_LCC1a=concatenateMatrix(x_var,A_ineq_LCC1a_sub);
clearvars A_ineq_LCC1a_sub

%% LCC2 Linearization of LCC Constraints weight factors

[A_eq_LCC2_sub, b_eq_LCC2]=createMatrix(par.T, x_var);
i=1;
for t=1:par.T 
    for sap=1:par.SOCApprox
        A_eq_LCC2_sub.S_SOCapprox(i, (sap-1) * par.T + t)     = par.SOCvector(sap);
    end
    %A_eq_LCC2_sub.P_ESScrg(i,t)                           = -par.ESS.eta_c * 1/par.ESS.P_c_max * par.delta_T/60;
    A_eq_LCC2_sub.E_ESSstore(i,t)                         = -1/par.ESS.E_max ;
    i=i+1;
    
end

A_eq_LCC2=concatenateMatrix(x_var,A_eq_LCC2_sub);
clearvars A_eq_LCC2_sub

%% LCC2a Linearization of LCC Constraints weight factors

[A_eq_LCC2a_sub, b_eq_LCC2a]=createMatrix(par.T, x_var);
i=1;
for t=1:par.T 
    for sap=1:par.SOCApprox
        A_eq_LCC2a_sub.S_SOCapprox(i, (sap-1) * par.T + t)     = 1;
    end
    b_eq_LCC2a(i,1)                         = 1;
    i=i+1;
    
end

A_eq_LCC2a=concatenateMatrix(x_var,A_eq_LCC2a_sub);
clearvars A_eq_LCC2a_sub

%% LCC3 DOD Calculations

[A_eq_LCC3_sub, b_eq_LCC3]=createMatrix( 2*par.T_C+1, x_var);
i=1;
for t=1:par.T_C 
    A_eq_LCC3_sub.D_DOD(i, t)       = -1;
    A_eq_LCC3_sub.P_ESScrg(i,t)  	= par.ESS.eta_c     *   1/par.ESS.E_max * par.delta_T/60;
    A_eq_LCC3_sub.P_ESSdcrg(i,t) 	= -1/par.ESS.eta_d  *   1/par.ESS.E_max * par.delta_T/60;
    i=i+1;  
end
for t=1:par.T_C 
    A_eq_LCC3_sub.D_DODabs(i, t)       = -1;
    A_eq_LCC3_sub.P_ESScrg(i,t)     = par.ESS.eta_c     *   1/par.ESS.E_max * par.delta_T/60;
    A_eq_LCC3_sub.P_ESSdcrg(i,t)	= 1/par.ESS.eta_d   *   1/par.ESS.E_max * par.delta_T/60;
    i=i+1;  
end
for t=1:par.T_C 
    A_eq_LCC3_sub.D_DODabs(i, t)       = -1;
    A_eq_LCC3_sub.D_DODabssum(i, 1)    =1;
end


A_eq_LCC3=concatenateMatrix(x_var,A_eq_LCC3_sub);
clearvars A_eq_LCC3_sub


%% LCC5 Linearization of LCC
[A_eq_LCC5_sub, b_eq_LCC5]=createMatrix( par.DODApprox , x_var);
i=1;

for dap=1:par.DODApprox 
    for ahdap=1:par.AhDODApprox
        A_eq_LCC5_sub.S_AhDODapprox(i, (ahdap-1)* par.DODApprox + dap) = 1;
    end
    A_eq_LCC5_sub.S_DODapprox(i, dap) = -1;
    i=i+1;
end
    

A_eq_LCC5=concatenateMatrix(x_var,A_eq_LCC5_sub);
clearvars A_eq_LCC5_sub

%% LCC4 Linearization of LCC Binary constraints linearization
[A_eq_LCC4_sub, b_eq_LCC4]=createMatrix( 1, x_var);
i=1;

for dap=1:par.DODApprox
    A_eq_LCC4_sub.S_DODapprox(i, dap) = 1;
end
b_eq_LCC4(i,1)=1;

A_eq_LCC4=concatenateMatrix(x_var,A_eq_LCC4_sub);
clearvars A_eq_LCC4_sub



%% LCC20 SOC Min Max identification equations per cycle
[A_ineq_LCC20_sub, b_ineq_LCC20]=createMatrix(4*par.T_C, x_var);
i=1;
for t=1:par.T_C
    A_ineq_LCC20_sub.E_ESSstore(i,t)=-1/par.ESS.E;
    A_ineq_LCC20_sub.D_SOCMIN(i,1)=1;
    i=i+1;
end
for t=1:par.T_C
    A_ineq_LCC20_sub.E_ESSstore(i,t)=1/par.ESS.E;
    A_ineq_LCC20_sub.D_SOCMAX(i,1)=-1;
    i=i+1;
end

for t=1:par.T_C
    A_ineq_LCC20_sub.E_ESSstore(i,t)=-1/par.ESS.E;
    A_ineq_LCC20_sub.D_SOCMAX(i,1)=1;
    A_ineq_LCC20_sub.S_SOCMAX(i,t)=big_M;
    b_ineq_LCC20(i,1)=big_M;
    i=i+1;
end

for t=1:par.T_C
    A_ineq_LCC20_sub.E_ESSstore(i,t)=1/par.ESS.E;
    A_ineq_LCC20_sub.D_SOCMIN(i,1)=-1;
    A_ineq_LCC20_sub.S_SOCMIN(i,t)=big_M;
    b_ineq_LCC20(i,1)=big_M;
    i=i+1;
end

A_ineq_LCC20=concatenateMatrix(x_var,A_ineq_LCC20_sub);
clearvars A_ineq_LCC20_sub

%% LCC21 SOC Min Max identification equations per cycle
[A_eq_LCC21_sub, b_eq_LCC21]=createMatrix(3, x_var);
i=1;
for t=1:par.T_C
    A_eq_LCC21_sub.S_SOCMIN(i,t)=1;
    b_eq_LCC21(i,1)=1;
end

i=i+1;
for t=1:par.T_C
    A_eq_LCC21_sub.S_SOCMAX(i,t)=1;
    b_eq_LCC21(i,1)=1;
end

i=i+1;
A_eq_LCC21_sub.D_DODper(i,1)   =1;
A_eq_LCC21_sub.D_SOCMAX(i,1)=-1;
A_eq_LCC21_sub.D_SOCMIN(i,1)=+1;

A_eq_LCC21=concatenateMatrix(x_var,A_eq_LCC21_sub);
clearvars A_eq_LCC21_sub

%% Zusammenfuehrung Matrizen f?r Optimierer

A_eq_sys=[A_eq_A0001; A_eq_A0002; A_eq_A2; A_eq_A3; A_eq_A4; A_eq_A01; A_eq_A5; A_eq_A02; A_eq_A03; A_eq_A6;  A_eq_A39; A_eq_THP; A_eq_AOF; A_eq_CHP4; A_eq_CHP5];
b_eq_sys=[b_eq_A0001; b_eq_A0002; b_eq_A2; b_eq_A3; b_eq_A4; b_eq_A01; b_eq_A5; b_eq_A02; b_eq_A03; b_eq_A6;  b_eq_A39; b_eq_THP; b_eq_AOF; b_eq_CHP4; b_eq_CHP5];
A_ineq_sys=[A_ineq_A12; A_ineq_A05; A_ineq_A06; A_ineq_A20; A_ineq_A37;  A_ineq_A47; A_ineq_A04;    A_ineq_CHP3];%A_ineq_A4a; A_ineq_CHP2;
b_ineq_sys=[b_ineq_A12; b_ineq_A05; b_ineq_A06; b_ineq_A20; b_ineq_A37;  b_ineq_A47; b_ineq_A04;    b_ineq_CHP3];%b_ineq_A4a; b_ineq_CHP2;    

if opt.INCL_LCC==1
    A_eq_sys=   [A_eq_sys; A_eq_LCC1; A_eq_LCC2; A_eq_LCC2a;  A_eq_LCC3;    A_eq_LCC4;  A_eq_LCC5;  A_eq_LCC21];
    b_eq_sys=   [b_eq_sys; b_eq_LCC1; b_eq_LCC2; b_eq_LCC2a;  b_eq_LCC3;    b_eq_LCC4;  b_eq_LCC5;  b_eq_LCC21];
    A_ineq_sys= [A_ineq_sys; A_ineq_LCC1a; A_ineq_LCC20];
    b_ineq_sys= [b_ineq_sys; b_ineq_LCC1a; b_ineq_LCC20];   
end

if opt.INCL_epsconstraint == 1
   A_ineq_sys= [A_ineq_sys; A_ineq_OF2];
   b_ineq_sys= [b_ineq_sys; b_ineq_OF2]; 
end


clearvars A_eq_A2 A_eq_A3 A_eq_A4 A_eq_A01 A_eq_A5 A_eq_A02 A_eq_A6 A_eq_LCC1 A_eq_LCC2 A_eq_LCC3 A_eq_LCC4 A_eq_LCC11 A_ineq_LCC12 A_eq_LCC13 A_eq_THP
clearvars b_eq_A2 b_eq_A3 b_eq_A4 b_eq_A01 b_eq_A5 b_eq_A02 b_eq_A6 b_eq_LCC1 b_eq_LCC2 b_eq_LCC3 b_eq_LCC4 b_eq_LCC11 b_ineq_LCC12 b_eq_LCC13 b_eq_THP
clearvars A_ineq_A12 A_ineq_A20 A_ineq_A37 A_ineq_A47 A_ineq_LCC14 A_ineq_LCC15
clearvars b_ineq_A12 b_ineq_A20 b_ineq_A37 b_ineq_A47 b_ineq_LCC14 b_ineq_LCC15
clearvars f_sub

%% Optimierung

RMESS.f=f;
RMESS.ub=x_var.ub;
RMESS.lb=x_var.lb;
RMESS.Aineq=A_ineq_sys;
RMESS.bineq=b_ineq_sys;
RMESS.Aeq=A_eq_sys;
RMESS.beq=b_eq_sys;
RMESS.ctype=x_var.type;

clearvars A_ineq_sys b_ineq_sys A_eq_sys b_eq_sys f

cplex = Cplex(RMESS);

% Add Special order Set for AhDOD approx
SOSnAHDOD=((x_var.details.S_AhDODapprox.vect_end+1)-x_var.details.S_AhDODapprox.vect_start)/par.AhDODApprox;
i=1;
for dap=1:par.DODApprox
    id_vect=zeros(par.AhDODApprox,1);
    wt_vect=zeros(par.AhDODApprox,1);
    for ahdap=1:par.AhDODApprox
            id_vect(ahdap)=x_var.details.S_AhDODapprox.vect_start-1 + (ahdap-1) * par.DODApprox + dap;
            wt_vect(ahdap)=ahdap;        
    end
    cplex.addSOSs('2', id_vect, wt_vect, {'sos2ahdod'}); 
    i=i+1;
end

%Add Special order set for SOC approx
SOSnDOD=((x_var.details.S_DODapprox.vect_end+1)-x_var.details.S_DODapprox.vect_start)/par.DODApprox;
i=1;
    id_vect=zeros(par.DODApprox,1);
    wt_vect=zeros(par.DODApprox,1);
    for dap=1:par.DODApprox
            id_vect(dap)=x_var.details.S_DODapprox.vect_start-1 + dap;
            wt_vect(dap)=dap;        
    end
    cplex.addSOSs('1', id_vect, wt_vect, {'sos1DOD'}); 

%Add Special order set for SOC approx
SOSnSOC=((x_var.details.S_SOCapprox.vect_end+1)-x_var.details.S_SOCapprox.vect_start)/par.SOCApprox;
i=1;
for t=1:par.T
    id_vect=zeros(par.SOCApprox,1);
    wt_vect=zeros(par.SOCApprox,1);
    for sap=1:par.SOCApprox
            id_vect(sap)=x_var.details.S_SOCapprox.vect_start-1 + (sap-1)*par.T +t;
            wt_vect(sap)=sap;        
    end
    cplex.addSOSs('2', id_vect, wt_vect, {'sos2soc'}); 
    i=i+1;
end

Alphaap=((x_var.details.S_CHPP_approx.vect_end+1)-x_var.details.S_CHPP_approx.vect_start)/par.n_CHPmodes;
i=1;
for t=1:par.T
    id_vect=zeros(par.n_CHPmodes,1);
    wt_vect=zeros(par.n_CHPmodes,1);
    for j=1:par.n_CHPmodes
            id_vect(j)=x_var.details.S_CHPP_approx.vect_start-1 + (j-1)*par.T +t;
            wt_vect(j)=j;        
    end
    cplex.addSOSs('2', id_vect, wt_vect, {'Alphaap2'}); 
    i=i+1;
end

if opt.cplex.DisplayIter==0
    cplex.DisplayFunc=[];
end
if opt.cplex.TimeCap>0
    cplex.Param.timelimit.Cur=opt.cplex.TimeCap;
end
if opt.cplex.SolTolerance>0
    cplex.Param.mip.tolerances.mipgap.Cur=opt.cplex.SolTolerance;
end

sol_found=0;
cplex.solve;

% while sol_found==0 %If no solution is found restrictions are changed and timecap increased.
%     cplex.solve;
%     if isfield( cplex.Solution, 'x')
%         if (cplex.Solution.miprelgap*100)<=15
%             sol_found=1;
%         end
%     end
%     if sol_found==0
%         if opt.cplex.TimeCap>0
%             opt.cplex.TimeCap=opt.cplex.TimeCap+2*60;
%             cplex.Param.timelimit.Cur=opt.cplex.TimeCap;
%         end
%         if opt.cplex.SolTolerance>0
%             opt.cplex.SolTolerance=opt.cplex.SolTolerance*10;
%             cplex.Param.mip.tolerances.mipgap.Cur=opt.cplex.SolTolerance;
%         end
%     end
% end

%% output back to other format

x           =cplex.Solution.x;
[res ,start] =divide_xvar_RM(x, x_var.dim.vect, x_var.names, x_var.subdim ,  par);

res.time    =cplex.Solution.time;
res.mipgap  =cplex.Solution.miprelgap;
%clearvars a b x 

end