%% Voreinstellungen

%hat die M�glichkeit THP nicht zu nehmen und vorher auf 50 Grad zu heizen
%und macht das, egal wie billig Strom ist

%au�erdem l�uft es nie f�r alle Outputs und Inputs, bei manchen Gr��en gibt
%es einfach kein Ergebnis - das d�rfte eigentlich nicht sein

x_var.subdim={};
x_var.type=[];
x_var.dim.vect=[];
x_var.dim.vect_start=[];
x_var.dim.vect_end=[];
x_var.dim.total=0;
x_var.names=[];
x_var.ub=[];
x_var.lb=[];
big_M=1000000;

par.delta_T     =  60; %min
par.T_O_Master  =  2; %Optimierungszeitraum in Tagen
par.T_O         =  par.T_O_Master;
par.T           =  24/(par.delta_T/60)*par.T_O_Master; %Optimierungszeitraum in Zeiteinheiten delta_T
par.T_1         =  par.T + 1;
par.T_C         =  24/(par.delta_T/60);
par.T_total     =  4; %kompletter Betrachtungszeitraum in TAgen


C_E = 0.02; %Preis elektrisch
C_G = 0.04; %Preis Gas

% COP = 4;

T_HT_delta_WT_in = 10;
eta_WT = 0.95;

m = 2.05; %kg/s
c = 4.182; %(kW*s)/(kg*K)

I.QDemand_NT_t = 180* ones (1,48); % in kW 180 * ones(1,48);
I.Qd_SH_prod = 120 * ones(1,48);%120 * rand (1,48);
I.Qd_TESS_V_prod = 30 * ones(1,48);

par.COP_stuetz_vector = [4.4; 5.6; 6.7];
par.T_THP_in_stuetz_vector = [15;30;45]; %gilt wenn DemandTemperatur 50 Grad ist
par.lamda_COP_Approx = length(par.COP_stuetz_vector);

par.NT_TESS.T_min = 5;%15; % Eigenschaften Speicher m�ssen in Namen noch nach NT und HT differenziert werden, sind im Code zurzeit noch �berall dieselben
par.NT_TESS.T_max = 90;%35;  %Bezeichnung mit par.NT.usw. einheitlich machen nach Umbennenung NT und HT und nicht mehr par.T_NT_usw.
par.NT_TESS.sigma = 0.0062;
par.NT_TESS.Volume = 100 ;% in l

Q_max_NT_TESS = par.NT_TESS.Volume * par.NT_TESS.T_max * c / 60; %244;%244; %244 in kWh bei 100 l bei 35Grad mit c von Wasser durch 60 von Sekunden auf Stunden zu kommen
Q_min_NT_TESS = par.NT_TESS.Volume * par.NT_TESS.T_min * c / 60;%104.55;%104.55; %104.55 bei 100l bei 15 Grad
gamma_NT_TESS = (Q_max_NT_TESS - Q_min_NT_TESS) / (par.NT_TESS.T_max - par.NT_TESS.T_min);

I.start.Q_NT_TESS_T0 = Q_min_NT_TESS; %Speicher am Anfang leer
I.end.Q_NT_TESS_T0 = (Q_max_NT_TESS - Q_min_NT_TESS)/2 + Q_min_NT_TESS;      % Speicher am Ende halb voll




x_var   =   createVariable(x_var, 'P_THP',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);

x_var   =   createVariable(x_var, 'Q_NT_TESS',       par.T_1,      {'1' 'T_1' '' '' ''},      'C',    Q_min_NT_TESS,  Q_max_NT_TESS);

x_var   =   createVariable(x_var, 'Qd_NT_WT_in',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M); %hei�t im richtigen Programm glaube ich etwas anders
x_var   =   createVariable(x_var, 'Qd_THP',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M); %in Qd_THP_out umbenennen
x_var   =   createVariable(x_var, 'Qd_THP_zu',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'Qd_THP_in',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'Qd_NT_WT_out',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'Qd_NT_TESS_in',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'Qd_NT_TESS_out',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'Qd_NT_Demand_Zusatz',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);

x_var   =   createVariable(x_var, 'Qd_NT_SH_Curt',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'Qd_NT_TESS_V_Curt',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);

x_var   =   createVariable(x_var, 'Qd_SH_s',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'Qd_NT_TESS_V',       par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);

x_var   =   createVariable(x_var, 'T_THP_in',         par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);
x_var   =   createVariable(x_var, 'T_NT_TESS_out',         par.T,      {'1' 'T' '' '' ''},      'C',    par.NT_TESS.T_min,  par.NT_TESS.T_max);
x_var   =   createVariable(x_var, 'T_NT_TESS_in',         par.T,      {'1' 'T' '' '' ''},      'C',    par.NT_TESS.T_min,  par.NT_TESS.T_max);
x_var   =   createVariable(x_var, 'T_NT_delta_WT_out',         par.T,      {'1' 'T' '' '' ''},      'C',    0,  big_M);

x_var   =   createVariable(x_var, 'S_T_THP_1',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_T_THP_2',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_T_THP_3',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_T_THP_4',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_T_THP_5',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);

x_var   =   createVariable(x_var, 'S_COP_1',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_COP_2',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_COP_3',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_COP_4',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_COP_5',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_COP_6',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_COP_7',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_COP_8',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_COP_9',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);
x_var   =   createVariable(x_var, 'S_COP_10',         par.T,      {'1' 'T' '' '' ''},      'B',    0,  1);

x_var   =   createVariable(x_var, 'beta_WT',         par.T,      {'1' 'T' '' '' ''},      'C',    -big_M,  big_M);


%% Zielfunktion

[f_sub]=createMatrix( 1 , x_var);
for t=1:par.T
    f_sub.P_THP(1, t)   = C_E; 
    f_sub.Qd_NT_WT_in(1, t)   = C_G;
end

f = concatenateMatrix(x_var,f_sub);

%% A001 THP intern

[A_eq_A001_sub, b_eq_A001]=createMatrix(3*par.T, x_var); 
i=1;

% %(1)
% for t=1:par.T
%     A_eq_A001_sub.P_THP(i, t)         = COP;
%     A_eq_A001_sub.Qd_THP(i, t)         = -1;
%     i=i+1;
% end

%(2)
for t=1:par.T
    A_eq_A001_sub.P_THP(i, t)         = 1;
    A_eq_A001_sub.Qd_THP_zu(i, t)         = -1;
    i=i+1;
end

%(3) mit zus�tzlicher Nachfrage, damit es bei zu
%kleiner Nachfrage trotzdem l�uft
for t=1:par.T
    A_eq_A001_sub.Qd_THP(i, t)         = 1;
%     A_eq_A001_sub.Qd_NT_Demand_Zusatz(i, t)         = -1;
    b_eq_A001(i,1) = I.QDemand_NT_t(t);
    i=i+1;
end

%(4)
for t=1:par.T
    A_eq_A001_sub.Qd_THP_in(i, t)         = 1;
    A_eq_A001_sub.Qd_THP(i, t)         = -1;
    A_eq_A001_sub.P_THP(i, t)         = 1;
    i=i+1;
end

A_eq_A001=concatenateMatrix(x_var,A_eq_A001_sub);

%% A002 Berechnung Eingangstemperatur THP

[A_eq_A002_sub, b_eq_A002]=createMatrix(4*par.T, x_var); 
i=1;

%(9)
for t=1:par.T
    A_eq_A002_sub.T_THP_in(i, t)         = 1;
    A_eq_A002_sub.T_NT_TESS_out(i, t)         = -1;
    A_eq_A002_sub.T_NT_delta_WT_out(i, t)         = -1;
    i=i+1;
end

%(10)
for t=1:par.T
    A_eq_A002_sub.T_NT_delta_WT_out(i, t)         = 1;
    A_eq_A002_sub.beta_WT(i, t)         = -eta_WT * T_HT_delta_WT_in ; 
    i=i+1;
end

for t=1:par.T
    A_eq_A002_sub.S_T_THP_1(i, t)         = 1;
    A_eq_A002_sub.S_T_THP_2(i, t)         = 1;
    A_eq_A002_sub.S_T_THP_3(i, t)         = 1;
    A_eq_A002_sub.S_T_THP_4(i, t)         = 1;
    A_eq_A002_sub.S_T_THP_5(i, t)         = 1;
    b_eq_A002(i,1) = 1;
    i=i+1;
end

for t=1:par.T
    A_eq_A002_sub.S_T_THP_1(i, t)         = -1 * 0;
    A_eq_A002_sub.S_T_THP_2(i, t)         = -1 * 0.25;
    A_eq_A002_sub.S_T_THP_3(i, t)         = -1 * 0.75;
    A_eq_A002_sub.S_T_THP_4(i, t)         = -1 * 1.25;
    A_eq_A002_sub.S_T_THP_5(i, t)         = -1 * 1.75;
    A_eq_A002_sub.beta_WT(i, t)         = 1;
    i=i+1;
end

A_eq_A002=concatenateMatrix(x_var,A_eq_A002_sub);

%% A005 Berechnung Eingangstemperatur THP inequal

[A_ineq_A005_sub, b_ineq_A005]=createMatrix(8*par.T, x_var); 
i=1;

%(11a 1)
for t=1:par.T
    A_ineq_A005_sub.Qd_NT_WT_out(i, t)         = 1;
    A_ineq_A005_sub.S_T_THP_1(i, t)         = big_M;
    b_ineq_A005(i,1) = big_M;
    i=i+1;
end

%(11b 1)
for t=1:par.T
    A_ineq_A005_sub.Qd_NT_WT_out(i, t)         = -1;
    A_ineq_A005_sub.Qd_NT_TESS_out(i, t)         = 0.0;
    A_ineq_A005_sub.S_T_THP_2(i, t)         = big_M;
    b_ineq_A005(i,1) = big_M;
    i=i+1;
end

%(11b 2)
for t=1:par.T
    A_ineq_A005_sub.Qd_NT_WT_out(i, t)         = 1;
    A_ineq_A005_sub.Qd_NT_TESS_out(i, t)         = -0.5;
    A_ineq_A005_sub.S_T_THP_2(i, t)         = big_M;
    b_ineq_A005(i,1) = big_M;
    i=i+1;
end

%(11c 1)
for t=1:par.T
    A_ineq_A005_sub.Qd_NT_WT_out(i, t)         = -1;
    A_ineq_A005_sub.Qd_NT_TESS_out(i, t)         = 0.5;
    A_ineq_A005_sub.S_T_THP_3(i, t)         = big_M;
    b_ineq_A005(i,1) = big_M;
    i=i+1;
end

%(11c 2)
for t=1:par.T
    A_ineq_A005_sub.Qd_NT_WT_out(i, t)         = 1;
    A_ineq_A005_sub.Qd_NT_TESS_out(i, t)         = -1.0;
    A_ineq_A005_sub.S_T_THP_3(i, t)         = big_M;
    b_ineq_A005(i,1) = big_M;
    i=i+1;
end

%(11d 1)
for t=1:par.T
    A_ineq_A005_sub.Qd_NT_WT_out(i, t)         = -1;
    A_ineq_A005_sub.Qd_NT_TESS_out(i, t)         = 1.0;
    A_ineq_A005_sub.S_T_THP_4(i, t)         = big_M;
    b_ineq_A005(i,1) = big_M;
    i=i+1;
end

%(11d 2)
for t=1:par.T
    A_ineq_A005_sub.Qd_NT_WT_out(i, t)         = 1;
    A_ineq_A005_sub.Qd_NT_TESS_out(i, t)         = -1.5;
    A_ineq_A005_sub.S_T_THP_4(i, t)         = big_M;
    b_ineq_A005(i,1) = big_M;
    i=i+1;
end

%(11e 1)
for t=1:par.T
    A_ineq_A005_sub.Qd_NT_WT_out(i, t)         = -1;
    A_ineq_A005_sub.Qd_NT_TESS_out(i, t)         = 1.5;
    A_ineq_A005_sub.S_T_THP_5(i, t)         = big_M;
    b_ineq_A005(i,1) = big_M;
    i=i+1;
end

A_ineq_A005=concatenateMatrix(x_var,A_ineq_A005_sub);

%% A003 W�rmeflussgleichungen

[A_eq_A003_sub, b_eq_A003]=createMatrix(6*par.T, x_var);
i=1;

%(12)
for t=1:par.T
    A_eq_A003_sub.Qd_THP_in(i, t)         = 1;
    A_eq_A003_sub.Qd_NT_WT_out(i, t)         = -1;
    A_eq_A003_sub.Qd_NT_TESS_in(i, t)         = 1;
    i=i+1;
end

%(13)
for t=1:par.T
    A_eq_A003_sub.Qd_NT_WT_out(i, t)         = 1;
    A_eq_A003_sub.Qd_NT_TESS_out(i, t)         = -1;
    A_eq_A003_sub.Qd_NT_WT_in(i, t)         = -1;
    i=i+1;
end

%(14)
for t=1:par.T
    A_eq_A003_sub.Qd_NT_WT_out(i, t)         = 1;
    A_eq_A003_sub.T_THP_in(i, t)         = -1 * m * c;
    i=i+1;
end

%(15)
for t=1:par.T
    A_eq_A003_sub.Qd_NT_TESS_out(i, t)         = 1;
    A_eq_A003_sub.T_NT_TESS_out(i, t)         = -1 * m * c;
    i=i+1;
end

%(16)
for t=1:par.T
    A_eq_A003_sub.Qd_NT_TESS_in(i, t)         = 1;
    A_eq_A003_sub.T_NT_TESS_in(i, t)         = -1 * m * c;
    i=i+1;
end

%(17)
for t=1:par.T
    A_eq_A003_sub.Qd_THP_in(i, t)         = 1;
    A_eq_A003_sub.T_THP_in(i, t)         = -1 * m * c;
    A_eq_A003_sub.T_NT_TESS_in(i, t)         = 1 * m * c;
    i=i+1;
end

A_eq_A003=concatenateMatrix(x_var,A_eq_A003_sub);

%% A004 Speicherinhalt

[A_eq_A004_sub, b_eq_A004]=createMatrix(1*par.T + 2, x_var);
i=1;

%(18) Hier ist Speicherverlust + SH als fester Input bei b drin, muss
%normal auf A Seite und als Variable gemacht werden
for t=1:par.T
    A_eq_A004_sub.Q_NT_TESS( i, t+1)          = +1;
    A_eq_A004_sub.Q_NT_TESS( i, t)           = -(1-par.NT_TESS.sigma);
    A_eq_A004_sub.Qd_NT_TESS_out( i, t)          = - (-par.delta_T/60);
    %A_eq_A004_sub.Qd_NT_TESS_Curt( i, t)          = - (-par.delta_T/60);
    A_eq_A004_sub.Qd_NT_TESS_in( i, t)          = - (par.delta_T/60);
    A_eq_A004_sub.Qd_SH_s( i, t)          = - (par.delta_T/60);
    A_eq_A004_sub.Qd_NT_TESS_V( i, t)          = - (par.delta_T/60);
    %b_eq_A004(i, 1) = I.Qd_SH_und_V(t);
    i=i+1;
end

A_eq_A004_sub.Q_NT_TESS(i, 1)        = 1; %Anfangsbedingung
b_eq_A004 (i,1)                       = I.start.Q_NT_TESS_T0;
i=i+1;

A_eq_A004_sub.Q_NT_TESS(i,par.T + 1) = 1; %Endedingung
b_eq_A004 (i,1)                       = I.end.Q_NT_TESS_T0;
i=i+1;

A_eq_A004=concatenateMatrix(x_var,A_eq_A004_sub);

%% A006 Berechnung Ausgangstemperatur Speicher

[A_eq_A006_sub, b_eq_A006]=createMatrix(1*par.T, x_var);
i=1;

for t=1:par.T
    A_eq_A006_sub.T_NT_TESS_out(i, t)         = 1;
    A_eq_A006_sub.Q_NT_TESS(i, t)         = -1 * 1/gamma_NT_TESS;
    b_eq_A006 (i,1) = par.NT_TESS.T_min - (Q_min_NT_TESS / gamma_NT_TESS);
    i=i+1;
end

A_eq_A006=concatenateMatrix(x_var,A_eq_A006_sub);

%% A007 SH Curtailment und Speicherkopplung Curtailment (die k�nnen so nicht in anderen Code �bernommen werden, da wird vorher curtailt

[A_eq_A007_sub, b_eq_A007]=createMatrix(2*par.T, x_var);
i=1;

for t=1:par.T
    A_eq_A007_sub.Qd_NT_TESS_V_Curt(i, t)         = 1;
    A_eq_A007_sub.Qd_NT_TESS_V(i, t)         = 1;
    b_eq_A007 (i,1) = I.Qd_TESS_V_prod(t);
    i=i+1;
end

for t=1:par.T
    A_eq_A007_sub.Qd_NT_SH_Curt(i, t)         = 1;
    A_eq_A007_sub.Qd_SH_s(i, t)         = 1;
    b_eq_A007 (i,1) = I.Qd_SH_prod(t);
    i=i+1;
end

A_eq_A007=concatenateMatrix(x_var,A_eq_A007_sub);

%% A008 COP Abh�ngigkeit

[A_ineq_A008_sub, b_ineq_A008]=createMatrix(11*par.T, x_var);
i=1;

% Berechnung der passenden Selectvariable aus T_THP_in

%S_COP_1 untere Grenze
for t=1:par.T
    A_ineq_A008_sub.T_THP_in(i, t)         = -1;
    A_ineq_A008_sub.S_COP_1(i, t)         = big_M;
    b_ineq_A008 (i,1) = -0 + big_M;
    i=i+1;
end
% S_COP_1 obere Grenze hier stimmt Wert nicht, ist im Normalfall 7.5
for t=1:par.T
    A_ineq_A008_sub.T_THP_in(i, t)         = 1;
    A_ineq_A008_sub.S_COP_1(i, t)         = big_M;
    b_ineq_A008 (i,1) = 7.5 + big_M;
    i=i+1;
end

% S_COP_2 untere Grenze
for t=1:par.T
    A_ineq_A008_sub.T_THP_in(i, t)         = -1;
    A_ineq_A008_sub.S_COP_2(i, t)         = big_M;
    b_ineq_A008 (i,1) = -7.5 + big_M;
    i=i+1;
end
% S_COP_2 obere Grenze
for t=1:par.T
    A_ineq_A008_sub.T_THP_in(i, t)         = 1;
    A_ineq_A008_sub.S_COP_2(i, t)         = big_M;
    b_ineq_A008 (i,1) = 12.5 + big_M;
    i=i+1;
end

% S_COP_3 untere Grenze
for t=1:par.T
    A_ineq_A008_sub.T_THP_in(i, t)         = -1;
    A_ineq_A008_sub.S_COP_3(i, t)         = big_M;
    b_ineq_A008 (i,1) = -12.5 + big_M;
    i=i+1;
end
% S_COP_3 obere Grenze
for t=1:par.T
    A_ineq_A008_sub.T_THP_in(i, t)         = 1;
    A_ineq_A008_sub.S_COP_3(i, t)         = big_M;
    b_ineq_A008 (i,1) = 17.5 + big_M;
    i=i+1;
end

%S_COP_4 untere Grenze
for t=1:par.T
    A_ineq_A008_sub.T_THP_in(i, t)         = -1;
    A_ineq_A008_sub.S_COP_4(i, t)         = big_M;
    b_ineq_A008 (i,1) = -17.5 + big_M;
    i=i+1;
end
%S_COP_4 obere Grenze
for t=1:par.T
    A_ineq_A008_sub.T_THP_in(i, t)         = 1;
    A_ineq_A008_sub.S_COP_4(i, t)         = big_M;
    b_ineq_A008 (i,1) = 42.5 + big_M;
    i=i+1;
end

% %S_COP_5 untere Grenze
% for t=1:par.T
%     A_ineq_A008_sub.T_THP_in(i, t)         = -1;
%     A_ineq_A008_sub.S_COP_5(i, t)         = big_M;
%     b_ineq_A008 (i,1) = -22.5 + big_M;
%     i=i+1;
% end
% %S_COP_5 obere Grenze
% for t=1:par.T
%     A_ineq_A008_sub.T_THP_in(i, t)         = 1;
%     A_ineq_A008_sub.S_COP_5(i, t)         = big_M;
%     b_ineq_A008 (i,1) = 27.5 + big_M;
%     i=i+1;
% end
% % 
% % % S_COP_6 untere Grenze
% % for t=1:par.T
% %     A_ineq_A008_sub.T_THP_in(i, t)         = -1;
% %     A_ineq_A008_sub.S_COP_6(i, t)         = big_M;
% %     b_ineq_A008 (i,1) = -27.5 + big_M;
% %     i=i+1;
% % end
% % % S_COP_6 obere Grenze
% % for t=1:par.T
% %     A_ineq_A008_sub.T_THP_in(i, t)         = 1;
% %     A_ineq_A008_sub.S_COP_6(i, t)         = big_M;
% %     b_ineq_A008 (i,1) = 32.5 + big_M;
% %     i=i+1;
% % end
% % 
% % % S_COP_7 untere Grenze
% % for t=1:par.T
% %     A_ineq_A008_sub.T_THP_in(i, t)         = -1;
% %     A_ineq_A008_sub.S_COP_7(i, t)         = big_M;
% %     b_ineq_A008 (i,1) = -32.5 + big_M;
% %     i=i+1;
% % end
% % % S_COP_7 obere Grenze
% % for t=1:par.T
% %     A_ineq_A008_sub.T_THP_in(i, t)         = 1;
% %     A_ineq_A008_sub.S_COP_7(i, t)         = big_M;
% %     b_ineq_A008 (i,1) = 37.5 + big_M;
% %     i=i+1;
% % end
% % 
% % % S_COP_8 untere Grenze
% % for t=1:par.T
% %     A_ineq_A008_sub.T_THP_in(i, t)         = -1;
% %     A_ineq_A008_sub.S_COP_8(i, t)         = big_M;
% %     b_ineq_A008 (i,1) = -37.5 + big_M;
% %     i=i+1;
% % end
% % % S_COP_8 obere Grenze
% % for t=1:par.T
% %     A_ineq_A008_sub.T_THP_in(i, t)         = 1;
% %     A_ineq_A008_sub.S_COP_8(i, t)         = big_M;
% %     b_ineq_A008 (i,1) = 42.5 + big_M;
% %     i=i+1;
% % end

% S_COP_9 untere Grenze
for t=1:par.T
    A_ineq_A008_sub.T_THP_in(i, t)         = -1;
    A_ineq_A008_sub.S_COP_9(i, t)         = big_M;
    b_ineq_A008 (i,1) = -42.5 + big_M;
    i=i+1;
end
% S_COP_9 obere Grenze
for t=1:par.T
    A_ineq_A008_sub.T_THP_in(i, t)         = 1;
    A_ineq_A008_sub.S_COP_9(i, t)         = big_M;
    b_ineq_A008 (i,1) = 50.0 + big_M;
    i=i+1;
end

% S_COP_10 untere Grenze - hier braucht THP keinen Strom, sondern schiebt
% einfach durch
for t=1:par.T
    A_ineq_A008_sub.T_THP_in(i, t)         = -1;
    A_ineq_A008_sub.S_COP_10(i, t)         = big_M;
    b_ineq_A008 (i,1) = -50 + big_M;
    i=i+1;
end

A_ineq_A008=concatenateMatrix(x_var,A_ineq_A008_sub);

%% Summe der COP Selectvariablen

[A_eq_A009_sub, b_eq_A009]=createMatrix(par.T, x_var);
i=1;

%Summe der Selectvariablen = 1
for t=1:par.T
    A_eq_A009_sub.S_COP_1(i, t)         = 1;
    A_eq_A009_sub.S_COP_2(i, t)         = 1;
    A_eq_A009_sub.S_COP_3(i, t)         = 1;
    A_eq_A009_sub.S_COP_4(i, t)         = 1;
%     A_eq_A009_sub.S_COP_5(i, t)         = 1;
%     A_eq_A009_sub.S_COP_6(i, t)         = 1;
%     A_eq_A009_sub.S_COP_7(i, t)         = 1;
%     A_eq_A009_sub.S_COP_8(i, t)         = 1;
    A_eq_A009_sub.S_COP_9(i, t)         = 1;
    A_eq_A009_sub.S_COP_10(i, t)         = 1;
    b_eq_A009 (i,1) = 1;
    i=i+1;
end

A_eq_A009=concatenateMatrix(x_var,A_eq_A009_sub);

%% Berechnung des W�rmeoutputs der THP �ber COP und elektrischen Input der THP - Gleichung (1)

[A_ineq_A0010_sub, b_ineq_A0010]=createMatrix(12*par.T, x_var);
i=1;

%S_COP_1a
for t=1:par.T
    A_ineq_A0010_sub.P_THP(i, t)         = 3.66;
    A_ineq_A0010_sub.Qd_THP(i, t)         = -1;
    A_ineq_A0010_sub.S_COP_1(i, t)         = big_M;
    b_ineq_A0010 (i,1) = big_M;
    i=i+1;
end
%S_COP_1b
for t=1:par.T
    A_ineq_A0010_sub.P_THP(i, t)         = -3.66;
    A_ineq_A0010_sub.Qd_THP(i, t)         = 1;
    A_ineq_A0010_sub.S_COP_1(i, t)         = big_M;
    b_ineq_A0010 (i,1) = big_M;
    i=i+1;
end

%S_COP_2a
for t=1:par.T
    A_ineq_A0010_sub.P_THP(i, t)         = 4.05;
    A_ineq_A0010_sub.Qd_THP(i, t)         = -1;
    A_ineq_A0010_sub.S_COP_2(i, t)         = big_M;
    b_ineq_A0010 (i,1) = big_M;
    i=i+1;
end
%S_COP_2b
for t=1:par.T
    A_ineq_A0010_sub.P_THP(i, t)         = -4.05;
    A_ineq_A0010_sub.Qd_THP(i, t)         = 1;
    A_ineq_A0010_sub.S_COP_2(i, t)         = big_M;
    b_ineq_A0010 (i,1) = big_M;
    i=i+1;
end

%S_COP_3a
for t=1:par.T
    A_ineq_A0010_sub.P_THP(i, t)         = 4.44;
    A_ineq_A0010_sub.Qd_THP(i, t)         = -1;
    A_ineq_A0010_sub.S_COP_3(i, t)         = big_M;
    b_ineq_A0010 (i,1) = big_M;
    i=i+1;
end
%S_COP_3b
for t=1:par.T
    A_ineq_A0010_sub.P_THP(i, t)         = -4.44;
    A_ineq_A0010_sub.Qd_THP(i, t)         = 1;
    A_ineq_A0010_sub.S_COP_3(i, t)         = big_M;
    b_ineq_A0010 (i,1) = big_M;
    i=i+1;
end

%S_COP_4a
for t=1:par.T
    A_ineq_A0010_sub.P_THP(i, t)         = 4.4; %4.82
    A_ineq_A0010_sub.Qd_THP(i, t)         = -1;
    A_ineq_A0010_sub.S_COP_4(i, t)         = big_M;
    b_ineq_A0010 (i,1) = big_M;
    i=i+1;
end
%S_COP_4b
for t=1:par.T
    A_ineq_A0010_sub.P_THP(i, t)         = -4.4; %4.82
    A_ineq_A0010_sub.Qd_THP(i, t)         = 1;
    A_ineq_A0010_sub.S_COP_4(i, t)         = big_M;
    b_ineq_A0010 (i,1) = big_M;
    i=i+1;
end

% %S_COP_5a
% for t=1:par.T
%     A_ineq_A0010_sub.P_THP(i, t)         = 5.21;
%     A_ineq_A0010_sub.Qd_THP(i, t)         = -1;
%     A_ineq_A0010_sub.S_COP_5(i, t)         = big_M;
%     b_ineq_A0010 (i,1) = big_M;
%     i=i+1;
% end
% %S_COP_5b
% for t=1:par.T
%     A_ineq_A0010_sub.P_THP(i, t)         = -5.21;
%     A_ineq_A0010_sub.Qd_THP(i, t)         = 1;
%     A_ineq_A0010_sub.S_COP_5(i, t)         = big_M;
%     b_ineq_A0010 (i,1) = big_M;
%     i=i+1;
% end
% 
% %S_COP_6a
% for t=1:par.T
%     A_ineq_A0010_sub.P_THP(i, t)         = 5.60;
%     A_ineq_A0010_sub.Qd_THP(i, t)         = -1;
%     A_ineq_A0010_sub.S_COP_6(i, t)         = big_M;
%     b_ineq_A0010 (i,1) = big_M;
%     i=i+1;
% end
% %S_COP_6b
% for t=1:par.T
%     A_ineq_A0010_sub.P_THP(i, t)         = -5.60;
%     A_ineq_A0010_sub.Qd_THP(i, t)         = 1;
%     A_ineq_A0010_sub.S_COP_6(i, t)         = big_M;
%     b_ineq_A0010 (i,1) = big_M;
%     i=i+1;
% end
% 
% %S_COP_7a
% for t=1:par.T
%     A_ineq_A0010_sub.P_THP(i, t)         = 5.99;
%     A_ineq_A0010_sub.Qd_THP(i, t)         = -1;
%     A_ineq_A0010_sub.S_COP_7(i, t)         = big_M;
%     b_ineq_A0010 (i,1) = big_M;
%     i=i+1;
% end
% %S_COP_7b
% for t=1:par.T
%     A_ineq_A0010_sub.P_THP(i, t)         = -5.99;
%     A_ineq_A0010_sub.Qd_THP(i, t)         = 1;
%     A_ineq_A0010_sub.S_COP_7(i, t)         = big_M;
%     b_ineq_A0010 (i,1) = big_M;
%     i=i+1;
% end
% 
% %S_COP_8a
% for t=1:par.T
%     A_ineq_A0010_sub.P_THP(i, t)         = 6.38;
%     A_ineq_A0010_sub.Qd_THP(i, t)         = -1;
%     A_ineq_A0010_sub.S_COP_8(i, t)         = big_M;
%     b_ineq_A0010 (i,1) = big_M;
%     i=i+1;
% end
% %S_COP_8b
% for t=1:par.T
%     A_ineq_A0010_sub.P_THP(i, t)         = -6.38;
%     A_ineq_A0010_sub.Qd_THP(i, t)         = 1;
%     A_ineq_A0010_sub.S_COP_8(i, t)         = big_M;
%     b_ineq_A0010 (i,1) = big_M;
%     i=i+1;
% end

%S_COP_9a
for t=1:par.T
    A_ineq_A0010_sub.P_THP(i, t)         = 6.77;
    A_ineq_A0010_sub.Qd_THP(i, t)         = -1;
    A_ineq_A0010_sub.S_COP_9(i, t)         = big_M;
    b_ineq_A0010 (i,1) = big_M;
    i=i+1;
end
%S_COP_9b
for t=1:par.T
    A_ineq_A0010_sub.P_THP(i, t)         = -6.77;
    A_ineq_A0010_sub.Qd_THP(i, t)         = 1;
    A_ineq_A0010_sub.S_COP_9(i, t)         = big_M;
    b_ineq_A0010 (i,1) = big_M;
    i=i+1;
end

%S_COP_10a - THP braucht keinen Strom, sondern schiebt einfach durch
for t=1:par.T
    A_ineq_A0010_sub.P_THP(i, t)         = 1;
    A_ineq_A0010_sub.S_COP_10(i, t)         = big_M;
    b_ineq_A0010 (i,1) = big_M;
    i=i+1;
end
%S_COP_10b - THP braucht keinen Strom, sondern schiebt einfach durch
for t=1:par.T
    A_ineq_A0010_sub.P_THP(i, t)         = -1;
    A_ineq_A0010_sub.S_COP_10(i, t)         = big_M;
    b_ineq_A0010 (i,1) = big_M;
    i=i+1;
end

A_ineq_A0010=concatenateMatrix(x_var,A_ineq_A0010_sub);

%% Berechnung des W�rmeinputs (aus dem W�rmekreis, nicht zugeschossene W�rme!) der THP aus COP und W�rmeoutput der COP - Gleichung (4)

% [A_ineq_A0011_sub, b_ineq_A0011]=createMatrix(4*par.T, x_var);
% i=1;

% %S_COP_1a
% for t=1:par.T
%     A_ineq_A0011_sub.Qd_THP_in(i, t)         = 1;
%     A_ineq_A0011_sub.Qd_THP(i, t)         = -(1 - 1/3.66);
%     A_ineq_A0011_sub.S_COP_1(i, t)         = big_M;
%     b_ineq_A0011 (i,1) = big_M;
%     i=i+1;
% end
% %S_COP_1b
% for t=1:par.T
%     A_ineq_A0011_sub.Qd_THP_in(i, t)         = -1;
%     A_ineq_A0011_sub.Qd_THP(i, t)         = +(1 - 1/3.66);
%     A_ineq_A0011_sub.S_COP_1(i, t)         = big_M;
%     b_ineq_A0011 (i,1) = big_M;
%     i=i+1;
% end

% %S_COP_2a
% for t=1:par.T
%     A_ineq_A0011_sub.Qd_THP_in(i, t)         = 1;
%     A_ineq_A0011_sub.Qd_THP(i, t)         = -(1 - 1/4.05);
%     A_ineq_A0011_sub.S_COP_2(i, t)         = big_M;
%     b_ineq_A0011 (i,1) = big_M;
%     i=i+1;
% end
% %S_COP_2b
% for t=1:par.T
%     A_ineq_A0011_sub.Qd_THP_in(i, t)         = -1;
%     A_ineq_A0011_sub.Qd_THP(i, t)         = +(1 - 1/4.05);
%     A_ineq_A0011_sub.S_COP_2(i, t)         = big_M;
%     b_ineq_A0011 (i,1) = big_M;
%     i=i+1;
% end
% 
% %S_COP_3a
% for t=1:par.T
%     A_ineq_A0011_sub.Qd_THP_in(i, t)         = 1;
%     A_ineq_A0011_sub.Qd_THP(i, t)         = -(1 - 1/4.44);
%     A_ineq_A0011_sub.S_COP_3(i, t)         = big_M;
%     b_ineq_A0011 (i,1) = big_M;
%     i=i+1;
% end
% %S_COP_3b
% for t=1:par.T
%     A_ineq_A0011_sub.Qd_THP_in(i, t)         = -1;
%     A_ineq_A0011_sub.Qd_THP(i, t)         = +(1 - 1/4.44);
%     A_ineq_A0011_sub.S_COP_3(i, t)         = big_M;
%     b_ineq_A0011 (i,1) = big_M;
%     i=i+1;
% end
% 
% %S_COP_4a
% for t=1:par.T
%     A_ineq_A0011_sub.Qd_THP_in(i, t)         = 1;
%     A_ineq_A0011_sub.Qd_THP(i, t)         = -(1 - 1/4.82);
%     A_ineq_A0011_sub.S_COP_4(i, t)         = big_M;
%     b_ineq_A0011 (i,1) = big_M;
%     i=i+1;
% end
% %S_COP_4b
% for t=1:par.T
%     A_ineq_A0011_sub.Qd_THP_in(i, t)         = -1;
%     A_ineq_A0011_sub.Qd_THP(i, t)         = +(1 - 1/4.82);
%     A_ineq_A0011_sub.S_COP_4(i, t)         = big_M;
%     b_ineq_A0011 (i,1) = big_M;
%     i=i+1;
% end
% 
% %S_COP_5a
% for t=1:par.T
%     A_ineq_A0011_sub.Qd_THP_in(i, t)         = 1;
%     A_ineq_A0011_sub.Qd_THP(i, t)         = -(1 - 1/5.21);
%     A_ineq_A0011_sub.S_COP_5(i, t)         = big_M;
%     b_ineq_A0011 (i,1) = big_M;
%     i=i+1;
% end
% %S_COP_5b
% for t=1:par.T
%     A_ineq_A0011_sub.Qd_THP_in(i, t)         = -1;
%     A_ineq_A0011_sub.Qd_THP(i, t)         = +(1 - 1/5.21);
%     A_ineq_A0011_sub.S_COP_5(i, t)         = big_M;
%     b_ineq_A0011 (i,1) = big_M;
%     i=i+1;
% end
% 
% %S_COP_6a
% for t=1:par.T
%     A_ineq_A0011_sub.Qd_THP_in(i, t)         = 1;
%     A_ineq_A0011_sub.Qd_THP(i, t)         = -(1 - 1/5.60);
%     A_ineq_A0011_sub.S_COP_6(i, t)         = big_M;
%     b_ineq_A0011 (i,1) = big_M;
%     i=i+1;
% end
% %S_COP_6b
% for t=1:par.T
%     A_ineq_A0011_sub.Qd_THP_in(i, t)         = -1;
%     A_ineq_A0011_sub.Qd_THP(i, t)         = +(1 - 1/5.60);
%     A_ineq_A0011_sub.S_COP_6(i, t)         = big_M;
%     b_ineq_A0011 (i,1) = big_M;
%     i=i+1;
% end
% 
% %S_COP_7a
% for t=1:par.T
%     A_ineq_A0011_sub.Qd_THP_in(i, t)         = 1;
%     A_ineq_A0011_sub.Qd_THP(i, t)         = -(1 - 1/5.99);
%     A_ineq_A0011_sub.S_COP_7(i, t)         = big_M;
%     b_ineq_A0011 (i,1) = big_M;
%     i=i+1;
% end
% %S_COP_7b
% for t=1:par.T
%     A_ineq_A0011_sub.Qd_THP_in(i, t)         = -1;
%     A_ineq_A0011_sub.Qd_THP(i, t)         = +(1 - 1/5.99);
%     A_ineq_A0011_sub.S_COP_7(i, t)         = big_M;
%     b_ineq_A0011 (i,1) = big_M;
%     i=i+1;
% end
% 
% %S_COP_8a
% for t=1:par.T
%     A_ineq_A0011_sub.Qd_THP_in(i, t)         = 1;
%     A_ineq_A0011_sub.Qd_THP(i, t)         = -(1 - 1/6.38);
%     A_ineq_A0011_sub.S_COP_8(i, t)         = big_M;
%     b_ineq_A0011 (i,1) = big_M;
%     i=i+1;
% end
% %S_COP_8b
% for t=1:par.T
%     A_ineq_A0011_sub.Qd_THP_in(i, t)         = -1;
%     A_ineq_A0011_sub.Qd_THP(i, t)         = +(1 - 1/6.38);
%     A_ineq_A0011_sub.S_COP_8(i, t)         = big_M;
%     b_ineq_A0011 (i,1) = big_M;
%     i=i+1;
% end

% %S_COP_9a
% for t=1:par.T
%     A_ineq_A0011_sub.Qd_THP_in(i, t)         = 1;
%     A_ineq_A0011_sub.Qd_THP(i, t)         = -(1 - 1/6.77);
%     A_ineq_A0011_sub.S_COP_9(i, t)         = big_M;
%     b_ineq_A0011 (i,1) = big_M;
%     i=i+1;
% end
% %S_COP_9b
% for t=1:par.T
%     A_ineq_A0011_sub.Qd_THP_in(i, t)         = -1;
%     A_ineq_A0011_sub.Qd_THP(i, t)         = +(1 - 1/6.77);
%     A_ineq_A0011_sub.S_COP_9(i, t)         = big_M;
%     b_ineq_A0011 (i,1) = big_M;
%     i=i+1;
% end

% A_ineq_A0011=concatenateMatrix(x_var,A_ineq_A0011_sub);

%% Zusammenf�hren Matrizen

A_eq_sys=[A_eq_A001; A_eq_A002; A_eq_A003; A_eq_A004; A_eq_A006; A_eq_A007; A_eq_A009];
b_eq_sys=[b_eq_A001; b_eq_A002; b_eq_A003; b_eq_A004; b_eq_A006; b_eq_A007; b_eq_A009];
A_ineq_sys=[A_ineq_A005; A_ineq_A008; A_ineq_A0010]; %A_ineq_A0011];
b_ineq_sys=[b_ineq_A005; b_ineq_A008; b_ineq_A0010]; %b_ineq_A0011];  


%% L�sung

RMESS.f=f;
RMESS.ub=x_var.ub;
RMESS.lb=x_var.lb;
RMESS.Aineq=A_ineq_sys;
RMESS.bineq=b_ineq_sys;
RMESS.Aeq=A_eq_sys;
RMESS.beq=b_eq_sys;
RMESS.ctype=x_var.type;

cplex = Cplex(RMESS);

%cplex.Param.mip.tolerances.mipgap.Cur = 1;
%cplex.Param.timelimit.Cur=20;
%cplex.Param.simplex.tolerances.Cur = 0.1;
 
cplex.solve;

x           =cplex.Solution.x;
[res ,start] =divide_xvar_RM(x, x_var.dim.vect, x_var.names, x_var.subdim ,  par);

filename = strcat('Test_NT_Parameter_',datestr(now,'yy-mm-dd_HH-MM-SS'),'.xlsx');

% xlswrite (filename,x_var.names(12),1,'A1');
% xlswrite (filename,x_var.names(11),1,'B1');
% xlswrite (filename,x_var.names(10),1,'C1');
% xlswrite (filename,res.Qd_NT_TESS_V_Curt_T,1,'A2');
% xlswrite (filename,res.Qd_NT_SH_Curt_T,1,'B2');
% xlswrite (filename,res.Qd_NT_Demand_Zusatz_T, 1,'C2');
% 
% xlswrite (filename,par.NT_TESS.T_min,1,'E2');
% xlswrite (filename,par.NT_TESS.T_max,1,'F2');
% xlswrite (filename,par.NT_TESS.Volume,1,'G2');